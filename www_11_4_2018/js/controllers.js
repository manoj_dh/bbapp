angular.module('starter.controllers', [])
    .controller('DashCtrl', function ($scope, $rootScope) {
        $scope.userType = localStorage.getItem('type');
        console.log($rootScope);
    })

    .controller("showgraphCtrl", function ($scope, $stateParams, $location, $ionicHistory, $state) { //,$ChartJsProvider
        var urlParams = $location.search();
        //alert(ChartJsProvider);
        //ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
        /*  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
          $scope.series = ['Series A', 'Series B'];
          $scope.data = [
              [65, 59, 80, 81, 56, 55, 40],
              [28, 48, 40, 19, 86, 27, 90]
          ];*/
        //alert(urlParams.date);
        console.log(urlParams);
        $scope.check = {};
        $scope.check.type = 'bar';
        $scope.projectid = urlParams.projectid;
        $scope.mostviewed = urlParams.mostviewed;
        var a = urlParams.date;
        $scope.labels = [];
        var dates = "";
        for (var i = 0; i < urlParams.date.length; i++) {
            if (i == 0) {
                $scope.labels.push(urlParams.date[i]);
            } else {
                $scope.labels.push(dates + urlParams.date[i]);
            }
        }

        $scope.organic = [];
        for (var i = 0; i < urlParams.organic.length; i++) {

            $scope.organic.push(urlParams.organic[i]);
        }
        $scope.direct = [];
        for (var i = 0; i < urlParams.direct.length; i++) {

            $scope.direct.push(urlParams.direct[i]);
        }
        $scope.social = [];
        for (var i = 0; i < urlParams.social.length; i++) {

            $scope.social.push(urlParams.social[i]);
        }
        $scope.ads = [];
        for (var i = 0; i < urlParams.ads.length; i++) {

            $scope.ads.push(urlParams.ads[i]);
        }

        $scope.series = ['Organic', 'Dirrect', 'Social', 'Ads'];
        $scope.data = [
            $scope.organic,
            $scope.direct,
            $scope.social,
            $scope.ads

        ];


        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.goBack = function () {
            console.log('here');
            //$ionicHistory.goBack(-1);
            $state.go('forpage', {
                projectid: $scope.projectid
            });
        }

    })
    .controller("showgoogle", function ($scope, $stateParams, $location, Chart, $ionicHistory, $state, $ionicPopup, $ionicLoading, $interval) {
        //,$ChartJsProvider
        var urlParams = $location.search();

        $scope.projectid = urlParams.projectid;
        $scope.mostviewed = urlParams.mostviewed;
        $scope.selectedpage = urlParams.selectedpage
        $scope.pages_data = urlParams.pages_data;
        $scope.dates = urlParams.dates;
        $scope.pageSelectedValue = "";
        $scope.chart = {};
        $scope.check = {};
        $scope.id = urlParams.id;
        //alert($scope.id);
        $scope.selectedtime = urlParams.time1;
        $scope.selecteduser = 'Users';
        $scope.Users2 = 'Users';
        $scope.check.type = 'Users';
        //$interval($scope.update, 100);



        $scope.update = function (selectedItem) {
            $scope.chart.page = selectedItem;
            $(".page_select").parent().removeClass("selector");
            $("#RemoveSpan").find("span").remove();
            // $('"#RemoveSpan"').next().remove();
            //alert('adfasdf');
        }
        var ids = $scope.id;
        $scope.chart.page = ids;
        setTimeout(function () {
            //alert(ids);
            $scope.update(ids);
            $scope.update1($scope.selectedtime);


        }, 2000);
        $scope.update1 = function (selectedItem) {
            $scope.selectedtime = selectedItem;
        }
        $scope.update2 = function (selectedItem) {
            $scope.selecteduser = selectedItem;
        }

        $scope.pages = [];

        $scope.againstpage = urlParams.againstpage;
        if ($scope.againstpage == '') {
            var myPopup = $ionicPopup.show({
                template: 'There is no data',
                buttons: [{
                    text: 'OK',
                    onTap: function (e) {

                        $state.go('forpage', {
                            projectid: $scope.projectid
                        });

                    }
                }]
            });

        };
        $scope.datasetOverride = {
            backgroundColor: "rgba(151,187,205,0.5)"
        };
        $scope.labels = [];
        var dates = "";
        angular.forEach($scope.againstpage, function (value, key) {
            //console.log(key + ': ' + value['pageTitle']);
            $scope.labels.push(value['date']);
        });
        $scope.series = ['page', 'vv'];

        $scope.data = [];




        angular.forEach($scope.againstpage, function (value, key) {
            //console.log(key + ': ' + value['pageTitle']);
            $scope.data.push(value['pageviews']);

        });

        $scope.getchart = function () {
            $scope.selectedpage1 = [];
            $scope.userType = localStorage.getItem('type');
            for (var i = 0; i < $scope.pages.length; i++) {
                console.log($scope.pages[i]);
                if ($scope.pages[i]['id'] == $scope.chart.page) {
                    $scope.selectedpage1.push($scope.pages[i]['url']);
                };
            }

            if ($scope.selectedpage1 == '') {
                $scope.selectedpage1.push('Overview');
            };

            if ($scope.chart.page == undefined) {
                var myPopup = $ionicPopup.show({
                    template: "Please Select Page",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                return false;
            };
            if ($scope.chart.time == undefined) {
                var myPopup = $ionicPopup.show({
                    template: "Please Select Time Durration",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                return false;
            };

            $ionicLoading.show({
                content: 'loading'
            })
            var chart = {
                token: localStorage.getItem('token'),
                projectid: $scope.projectid,
                start_date: $scope.chart.time,
                page: $scope.chart.page,
                title: $scope.selectedpage1
            };
            Chart.getchart(chart).then(function (responce) {
                $scope.labels = [];
                if (responce.data.success) {
                    //alert(JSON.stringify(responce.data.forpage));
                    /* angular.forEach(responce.data.forpage, function(value, key) {
                                  //console.log(key + ': ' + value['pageTitle']);
                                  $scope.data.push(value['pageviews']);
                                });*/

                    angular.forEach(responce.data.forpage, function (value, key) {
                        //console.log(key + ': ' + value['pageTitle']);
                        $scope.labels.push(value['date']);
                    });
                    if ($scope.check.type == 'Users') {
                        $scope.Users = 'Users';
                        $scope.Users2 = '';
                        $scope.Bounce = '';
                        $scope.Sessions = '';
                        $scope.Durration = '';
                        angular.forEach(responce.data.forpage, function (value, key) {
                            //console.log(key + ': ' + value['pageTitle']);
                            $scope.data.push(value['pageviews']);
                        });

                    };

                    if ($scope.check.type == 'Sessions') {
                        $scope.Sessions = 'Sessions';
                        $scope.Users2 = '';
                        $scope.Bounce = '';
                        $scope.Users = '';
                        $scope.Durration = '';
                        angular.forEach(responce.data.forpage, function (value, key) {
                            //console.log(key + ': ' + value['pageTitle']);
                            $scope.data.push(value['sessions']);
                        });

                    };
                    if ($scope.check.type == 'Bounce') {

                        $scope.Bounce = 'Bounce';
                        $scope.Users2 = '';
                        $scope.Durration = '';
                        $scope.Users = '';
                        $scope.Sessions = '';
                        angular.forEach(responce.data.forpage, function (value, key) {
                            //console.log(key + ': ' + value['pageTitle']);
                            $scope.data.push(value['bounces']);
                        });

                    };
                    if ($scope.check.type == 'Durration') {
                        $scope.Durration = 'Durration';
                        $scope.Users2 = '';
                        $scope.Bounce = '';
                        $scope.Users = '';
                        $scope.Sessions = '';
                        angular.forEach(responce.data.forpage, function (value, key) {

                            //console.log(key + ': ' + value['pageTitle']);
                            $scope.data.push(value['sessionDuration']);
                        });
                    };


                    //$scope.date = responce.data.from;					
                    $scope.series = ['Organic'];


                    $ionicLoading.hide();
                    console.log(responce);

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }),
                function (err) {
                    $ionicLoading.hide();
                };
        }


        $scope.getgraphs = function () {
            $scope.data = [];
            if ($scope.chart.page == 'Overview') {
                $scope.getchart();
                /* if ($scope.check.type == 'Users') {
						 	
					    angular.forEach($scope.againstpage, function(value, key) {
									  //console.log(key + ': ' + value['pageTitle']);
									  $scope.data.push(value['pageviews']);
									});
					};
							
							if ($scope.check.type == 'Sessions') {			
					    angular.forEach($scope.againstpage, function(value, key) {
									  //console.log(key + ': ' + value['pageTitle']);
									  $scope.data.push(value['sessions']);
									});
					};
					if ($scope.check.type == 'Bounce') {
					    angular.forEach($scope.againstpage, function(value, key) {
									  //console.log(key + ': ' + value['pageTitle']);
									  $scope.data.push(value['bounces']);
									});
					};
					if ($scope.check.type == 'Durration') {
					    angular.forEach($scope.againstpage, function(value, key) {
									  //console.log(key + ': ' + value['pageTitle']);
									  $scope.data.push(value['sessionDuration']);
									});
					};*/
            }
            if ($scope.chart.page != 'Overview') {
                $scope.getchart();

            }
        }

        $scope.getpages = function () {

            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var chart = {
                token: localStorage.getItem('token'),
                projectid: $scope.projectid
            };
            Chart.getpages(chart).then(function (responce) {

                if (responce.data.success) {
                    $ionicLoading.hide();
                    console.log(responce);
                    //alert(JSON.stringify(responce.data.requirements));
                    for (var i = 0; i < responce.data.pages.length; i++) {
                        console.log(responce.data.pages[i]);
                        $scope.pages.push(responce.data.pages[i]);
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getpages();

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.goBack = function () {
            console.log('here');
            $state.go('forpage', {
                projectid: $scope.projectid
            });
        }

    })
    .controller("webPerformance", function ($scope, $stateParams, $location, $state) {
        var urlParams = $location.search();

        console.log(urlParams.data);
        $scope.alldata = [];
        $scope.data = urlParams.data;

        $scope.projectid = urlParams.project_id;
        //alert($scope.projectid);
        $scope.goBack = function () {
            console.log('here');
            //$ionicHistory.goBack(-1);
            $state.go('forpage', {
                projectid: $scope.projectid
            });
        }

    })
    .controller("heatmap", function ($scope, $stateParams, $location, $state) {
        var urlParams = $location.search();

        console.log(urlParams.data);
        $scope.alldata = [];
        $scope.data = urlParams.data;
        $scope.projectid = urlParams.project_id;
        // alert(JSON.stringify($scope.data));
        $scope.goBack = function () {
            console.log('here');
            //$ionicHistory.goBack(-1);
            $state.go('forpage', {
                projectid: $scope.projectid
            });
        }

    })
    .controller("forpage", function ($scope, $location, $ionicPopover, $state, $http, $ionicModal, $ionicLoading, Chart, $ionicPopup, ionicDatePicker) { //,$ChartJsProvider

        var url = $location.path().split('/');

        $scope.projectid = url[2];
        $scope.pages = [];
        $scope.views = [];
        $scope.chart = {};

        $scope.getpages = function () {

            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var chart = {
                token: localStorage.getItem('token'),
                projectid: $scope.projectid
            };
            Chart.getpages(chart).then(function (responce) {

                if (responce.data.success) {
                    $ionicLoading.hide();
                    console.log(responce);
                    //alert(JSON.stringify(responce.data.requirements));
                    for (var i = 0; i < responce.data.pages.length; i++) {
                        console.log(responce.data.pages[i]);
                        $scope.pages.push(responce.data.pages[i]);
                    }

                    /*angular.forEach(responce.data.pages, function(value, key) {
				  //console.log(key + ': ' + value['pageTitle']);
				  $scope.pages.push(value['pageTitle']);
				});*/
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getpages();
        $scope.chart.assesment = 'heatmap';
        $scope.getviews = function () {
            $scope.userType = localStorage.getItem('type');
            if ($scope.chart.page == undefined) {
                var myPopup = $ionicPopup.show({
                    template: "Please Select Page",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                return false;
            };

            $ionicLoading.show({
                content: 'loading'
            })
            var chart = {
                token: localStorage.getItem('token'),
                view: $scope.chart.assesment,
                projectid: $scope.projectid,
                page: $scope.chart.page
            };
            Chart.getviews(chart).then(function (responce) {

                if (responce.data.success) {
                    $ionicLoading.hide();

                    $scope.views = responce.data.workflow;
                    if (responce.data.view == 'heatmap') {
                        $location.path('/heatmap').search({
                            data: $scope.views,
                            project_id: $scope.projectid
                        });
                    };
                    if (responce.data.view == 'web') {
                        $location.path('/webPerformance').search({
                            data: $scope.views,
                            project_id: $scope.projectid
                        });
                    };

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        /*$scope.endDate = new Date();
        $scope.openDatePickerTwo = function (val) {
        	
            	var ipObj1 = {
                	callback: function (val) {  //Mandatory
        				console.log(val);
                  		console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                  		$scope.endDate = new Date(val);
                	},
                	from: new Date(),
                	to: new Date(2022, 10, 30),
                	inputDate: new Date(),
                	mondayFirst: true,
                	disableWeekdays: [],
                	closeOnSelect: false,
               		templateType: 'popup'
              	};
              	ionicDatePicker.openDatePicker(ipObj1);
            };
            $scope.startDate = new Date();
         	$scope.openDatePickerOne = function (val) {
         		
            	var ipObj1 = {
                	callback: function (val) {  //Mandatory
                		
        				console.log(val);
                  		console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                  		$scope.startDate = new Date(val);
                  	
                	},
                	from: new Date(),
                	to: new Date(2022, 10, 30),
                	inputDate: new Date(),
                	mondayFirst: true,
                	disableWeekdays: [],
                	closeOnSelect: false,
               		templateType: 'popup'
              	};
              	ionicDatePicker.openDatePicker(ipObj1);
        	};*/
        $scope.dates = [];
        $scope.showgooglechart = function () {
            $scope.selectedpage = [];
            $scope.userType = localStorage.getItem('type');
            for (var i = 0; i < $scope.pages.length; i++) {
                console.log($scope.pages[i]);
                if ($scope.pages[i]['id'] == $scope.chart.page) {
                    $scope.selectedpage.push($scope.pages[i]['url']);
                };
            }
            //alert(JSON.stringify($scope.selectedpage));
            if ($scope.dates.val == undefined) {
                var myPopup = $ionicPopup.show({
                    template: "Please Select Time Interval",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                return false;
            };
            $scope.selectedpage = [];
            for (var i = 0; i < $scope.pages.length; i++) {
                console.log($scope.pages[i]);
                if ($scope.pages[i]['id'] == $scope.chart.page) {
                    $scope.selectedpage.push($scope.pages[i]['url']);
                };
            }

            if ($scope.selectedpage == '') {
                $scope.selectedpage.push('Overview');
            };
            /*$scope.showgooglechart1 = function() {
        var xhr = new XMLHttpRequest();
        var file = "http://bleedingbulbapp.com/webservices/";
        var randomNum = Math.round(Math.random() * 10000);
    
        xhr.open('HEAD', file + "?rand=" + randomNum, true);
        xhr.send();
        
        xhr.addEventListener("readystatechange", processRequest, false);
    
        function processRequest(e) {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 304) {
            alert("connection exists!");
            } else {
            alert("connection doesn't exist!");
            }
        }
        }
        }
        $scope.showgooglechart1(); */
            $ionicLoading.show({
                content: 'loading'
            });
            $scope.time1 = $scope.dates.val;
            var chart = {
                token: localStorage.getItem('token'),
                projectid: $scope.projectid,
                start_date: $scope.dates.val,
                end_date: $scope.endDate,
                page: $scope.chart.page,
                title: $scope.selectedpage
            };

            Chart.showchart(chart).then(function (responce) {
                $scope.date = [];

                if (responce.data.success) {

                    // alert(JSON.stringify(responce.data.mostViewedPages));


                    //$scope.date = responce.data.from;
                    $scope.mostviewed = responce.data.mostViewedPages;
                    $scope.pages_data = responce.data.pages_data;
                    $scope.aginstpage = responce.data.againstpage;
                    $scope.dates = responce.data.dates;
                    $ionicLoading.hide();

                    $location.path('/showgoogle').search({
                        mostviewed: $scope.mostviewed,
                        projectid: $scope.projectid,
                        pages_data: $scope.pages_data,
                        dates: $scope.dates,
                        againstpage: $scope.aginstpage,
                        selectedpage: $scope.selectedpage,
                        id: $scope.chart.page,
                        time1: $scope.time1
                    });
                    console.log(responce);

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.showchart = function () {
            $scope.userType = localStorage.getItem('type');
            if ($scope.chart.page == undefined) {
                var myPopup = $ionicPopup.show({
                    template: "Please Select Page",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                return false;
            };
            if ($scope.chart.page == 'Overview') {
                $scope.showgooglechart();
                return false;

            };

            $ionicLoading.show({
                content: 'loading'
            })
            var chart = {
                token: localStorage.getItem('token'),
                projectid: $scope.projectid,
                start_date: $scope.dates.val,
                end_date: $scope.endDate,
                page: $scope.chart.page
            };
            Chart.showchart(chart).then(function (responce) {
                $scope.date = [];
                if (responce.data.success) {

                    // alert(JSON.stringify(responce.data.mostViewedPages));
                    for (var i = 0; i < responce.data.from.length; i++) {
                        $scope.date.push(responce.data.from[i]);
                    }

                    //$scope.date = responce.data.from;
                    $scope.mostviewed = responce.data.mostViewedPages;
                    $scope.organic_status = responce.data.organic_status;
                    $scope.direct_status = responce.data.direct_status;
                    $scope.social_status = responce.data.social_status;
                    $scope.ads = responce.data.ads_status;
                    $ionicLoading.hide();
                    $location.path('/showgraph').search({
                        date: $scope.date,
                        mostviewed: $scope.mostviewed,
                        organic: $scope.organic_status,
                        direct: $scope.direct_status,
                        social: $scope.social_status,
                        ads: $scope.ads,
                        projectid: $scope.projectid
                    });
                    console.log(responce);

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }




    })
    .controller('ActivityCtrl', function ($scope, $cordovaDevice, $ionicPopover, $state, $http, $ionicModal, $ionicLoading, $ionicPopup, Activity, $timeout) {
        $scope.taskList = [];
        $scope.messagesList = [];
        $scope.invoicesList = [];
        $scope.documentList = [];
        $scope.meetingListing = [];
        $scope.now = new Date();
        //$scope.now.toISOString();

        $scope.getActivity = function () {
            //alert('con');
            $scope.taskList = [];
            $scope.messagesList = [];
            $scope.invoicesList = [];
            $scope.documentList = [];
            $scope.meetingListing = [];
            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var activity = {
                token: localStorage.getItem('token')
            };
            //alert('FCMPlugin sy bhr');
            if (typeof FCMPlugin != 'undefined') {
                //alert('FCMPlugin ma enter');
                FCMPlugin.getToken(
                    function (token) {
                        //alert(token);
                        var user = {
                            "token": localStorage.getItem('token'),
                            "device_id": token,
                            "type": $scope.userType,
                            "user_id": localStorage.getItem('user_id')
                        };
                        //	alert('con');
                        Activity.registerDevice(user).then(function (responce) {
                            //	alert(responce);
                            console.log(responce);

                        }, function (err) {
                            $ionicLoading.hide();
                        });
                        //alert('Tokenxxxxxxxxxxxx: ' + token);
                        console.log('Token: ' + token);
                    },
                    function (err) {
                        alert('error retrieving token: ' + token);
                        console.log('error retrieving token: ' + err);
                    });
            }
            Activity.getActivity(activity).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.invoices.length; i++) {
                        console.log(responce.data.invoices[i]);
                        $scope.invoicesList.push(responce.data.invoices[i]);
                    }
                    for (var i = 0; i < responce.data.all_tasks.length; i++) {
                        //console.log(responce.data.all_tasks[i]);
                        $scope.taskList.push(responce.data.all_tasks[i]);
                    }
                    for (var i = 0; i < responce.data.inbox_messages.length; i++) {
                        console.log(responce.data.inbox_messages[i]);
                        $scope.messagesList.push(responce.data.inbox_messages[i]);
                    }
                    for (var i = 0; i < responce.data.documents.length; i++) {
                        console.log(responce.data.documents[i]);
                        $scope.documentList.push(responce.data.documents[i]);
                    }
                    for (var i = 0; i < responce.data.appointments.length; i++) {
                        console.log(responce.data.appointments[i]);
                        $scope.meetingListing.push(responce.data.appointments[i]);
                    }
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getActivity();
        //getUnreadMessage($scope);
        $scope.getDocumentDetail = function ($document_id) {
            $state.go('documentDetail', {
                "documentid": $document_id
            })
        }
        $scope.getTaskDetail = function ($task_id) {
            $state.go('taskDetail', {
                "taskid": $task_id
            })
        }
        $scope.getInoviceDetail = function ($invoice_id) {
            $state.go('invoiceDetail', {
                "invoiceid": $invoice_id
            })
        }

    })
    .controller('ProjectsCtrl', function ($scope, $ionicPopover, $state, $http, $ionicModal, $ionicLoading, $ionicPopup, Projects, $timeout, $interval) {
        $scope.projectsList = [];
        $scope.task = {};
        $scope.btn1 = {};
        $scope.btn2 = {};
        $scope.companies = [];
        $scope.userType = localStorage.getItem('type');
        localStorage.setItem('project_id', 10);
        $scope.IsVisible = false;
        $scope.IsVisible2 = false;
        $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.
            updatelink($scope, "pending_tab");
            $scope.IsVisible = $scope.IsVisible ? false : true;
            $scope.IsVisible2 = false;
            $scope.getProjects();
        }
        $scope.ShowHide2 = function () {
            //If DIV is visible it will be hidden and vice versa.
            updatelink($scope, "completed_tab");
            $scope.IsVisible2 = $scope.IsVisible2 ? false : true;
            $scope.IsVisible = false;
            $scope.getCompleteProjects();
        }


        $scope.getProjects = function () {
            updatelink($scope, "pending_tab"); // select green color on tab
            $scope.projectsList = [];
            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token'),
                status: 2,
                company_id: $scope.task.compnay
            };
            Projects.getProjects(project).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.projectsList.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompleteProjects = function () {
            $scope.userType = localStorage.getItem('type');
            updatelink($scope, "completed_tab"); // select green color on tab
            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token'),
                status: 4,
                company_id: $scope.task.compnay
            };
            Projects.getProjects(project).then(function (responce) {
                if (responce.data.success) {
                    $scope.projectsList = [];
                    $ionicLoading.hide();
                    console.log(responce);
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        console.log(responce.data.projects[i])
                        $scope.projectsList.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        //$scope.getProjects();
        $scope.getCompanies = function () {
            // /updatelink($scope,"pending_tab"); // select green color on tab//mn ny ki hai

            $scope.ShowHide();
            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token')

            };
            Projects.getCompanies(project).then(function (responce) {
                if (responce.data.success) {
                    //$scope.projectsList = [];
                    $ionicLoading.hide();
                    console.log(responce);
                    for (var i = 0; i < responce.data.companies.length; i++) {
                        console.log(responce.data.companies[i])
                        $scope.companies.push(responce.data.companies[i]);
                        //$scope.projects = responce.data.projects;
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompanies();
        $scope.getProjectDetail = function ($project_id) {
            $state.go('projectDetail', {
                "projectid": $project_id
            })
        }
    })
    .controller('quotes', function ($scope, $location, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Quotes, $ionicHistory) {

        $scope.userType = localStorage.getItem('type');
        $scope.getquotes = function () {

            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $scope.quotes = [];
            $ionicLoading.show({
                content: 'loading'
            })
            var quotes = {
                token: localStorage.getItem('token')

            };
            
            Quotes.getquotes(quotes).then(function (responce) {
                
                $ionicLoading.hide();
                if (responce.data.success) {
                    $scope.data = responce.data.data;

                    $location.path('/quotesDetail').search({
                        data: $scope.data
                    });
                   // console.log(responce.data.data);
                     //console.log('sssss');
                     //console.log($scope.data);    
                   // alert(JSON.stringify($scope.data));
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.result3 = [];


        $scope.user = {};
        
        $scope.listing = function () {
            $scope.user.status = 'Pending';
            updatelink($scope, "pending_tab");
            $scope.test = [];
            $scope.userType = localStorage.getItem('type');
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var quotes = {
                status: $scope.user.status,
                token: localStorage.getItem('token')

            };
            Quotes.getdetails(quotes).then(function (responce) {
                $ionicLoading.hide();
                if (responce.data.success) {
                    $scope.listing = [];
                    /*alert(JSON.stringify(responce.data.data));*/
                    for (var i = 0; i < responce.data.data.length; i++) {
                        console.log(responce.data.data[i]);
                        $scope.listing.push(responce.data.data[i]);
                    }

                    /*$location.path('/quotesDetail').search({
                        data: $scope.test
                    });*/


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });



        }
        $scope.listing();
        
        $scope.getdetails = function (val) {
           // alert();

           if (val == 'Pending') {
            $scope.user.status = 'Pending';
            updatelink($scope, "pending_tab");
           };
          
           if (val == 'Rejected') {
            $scope.user.status = 'Rejected';
            updatelink($scope, "rejected_tab");
           };
             if (val == 'accepted') {
              $scope.user.status = 'accepted';
            updatelink($scope, "completed_tab");
           };
             
            $scope.userType = localStorage.getItem('type');
            $scope.listing = [];
            $ionicLoading.show({
                content: 'loading'
            })

            var quotes = {
               // status: $scope.user.status,val
                status: val,
                token: localStorage.getItem('token')

            };
            Quotes.getdetails(quotes).then(function (responce) {

                if (responce.data.success) {
                    for (var i = 0; i < responce.data.data.length; i++) {
                        console.log(responce.data.data[i]);
                        $scope.listing.push(responce.data.data[i]);
                    }
                    $ionicLoading.hide();
                    console.log(responce.data.data);
                 

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        //$scope.userType = localStorage.getItem('type');
        
        $scope.fordetail = function (id) {
            var id = id;

            $scope.detail = [];
            $scope.rate_item='';
            $scope.name_item='';
          //  alert(JSON.stringify($scope.listing));
            //alert(JSON.stringify($scope.listing));
            if ($scope.userType != 'client') {
                angular.forEach($scope.listing, function (value, key) {

                    if (value['id'] == id) {
                       // alert(value['details']);
                        var res = value['details'].split("<br>");
                        for (var i = 0; i < res.length; i++) {
                           var xp=res[i].trim()
                           var res2 = xp.split("$");
                           var category = res2[0];
                           $scope.name_item =$scope.name_item+'<br>'+category;
                           //$scope.name_item.push("<br>");
                         if (res[i] != ' ') { 

                           var rate = res2[1].split(":");
                           var rate = rate[0]; 
                            var a = Number(rate || 0);
                            var b = Number($scope.rate_item || 0);
                           $scope.rate_item = a+b;

                         //  console.log(category+"==="+rate);
                       }
                           //alert(res2[0]);
                        };
                       // $scope.detail.push(value['details']);
                       
                    }
                    key++;
                });

            //alert(JSON.stringify($scope.detail));
            };
            if ($scope.userType == 'client') {
                angular.forEach($scope.listing, function (value, key) {

                    if (value['id'] == id) {

                         var res = value['details'].split("<br>");
                        for (var i = 0; i < res.length; i++) {
                           var xp=res[i].trim()
                           var res2 = xp.split("$");
                           var category = res2[0];
                           $scope.name_item =$scope.name_item+'<br>'+category;
                           //$scope.name_item.push("<br>");
                         if (res[i] != ' ') { 

                           var rate = res2[1].split(":");
                           var rate = rate[0]; 
                            var a = Number(rate || 0);
                            var b = Number($scope.rate_item || 0);
                           $scope.rate_item = a+b;

                         //  console.log(category+"==="+rate);
                       }
                           //alert(res2[0]);
                        };

                       // $scope.detail.push(value['details']);
                    }
                    key++;
                });


            };

            if ($scope.detail == '') {
                $scope.detail = "Empty";
            };
            console.log($scope.name_item);
            var re =$scope.name_item;
           // $scope.name_item= re.replace(',',' ');
          
            var myPopup = $ionicPopup.show({
                template:$scope.name_item,
                title: 'Total:'+$scope.rate_item,
                buttons: [{
                    text: 'OK'
                },]
            });
        }
        $scope.check = {};
        $scope.sendback = function () {

            var quotes = {
                id: $scope.row_id,
                email: $scope.row_email,
                price: $scope.datas.price
            };
            if ($scope.datas.price == undefined) {
               var myPopup = $ionicPopup.show({
                        template: "Please add price first",
                        buttons: [{
                            text: 'OK'
                        },]
                    });   
               return false;
            };
            Quotes.sendback(quotes).then(function (responce) {

                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: "Quotes send back successfully",
                        buttons: [{
                            text: 'OK'
                        },]
                    });


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.accept = function (id) {
            var quotes = {
                id: id,
                token: localStorage.getItem('token'),

            };
            Quotes.accept(quotes).then(function (responce) {

                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: "Quotes accepted successfully",
                        buttons: [{
                            text: 'OK'
                        },]
                    });


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Allready Accepted",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.Reject = function (id) {


            var quotes = {
                id: id,
                token: localStorage.getItem('token'),
                coment: $scope.rej.coment

            };
            Quotes.Reject(quotes).then(function (responce) {

                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: "Quotes rejected successfully",
                        buttons: [{
                            text: 'OK'
                        },]
                    });


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Allready rejected",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.rej = {};
        $scope.send_reject = function (id) {

            var myPopup = $ionicPopup.show({
                template: '<input type = "text" ng-model ="rej.coment" placeholder="Comment">',
                title: 'Add Comment',
                subTitle: 'Enter your comment',
                cssClass: 'for-check',
                scope: $scope,
                buttons: [{

                    text: 'Cancel',
                    onTap: function (e) {

                        return false;
                    }
                },
                {
                    text: 'OK',
                    onTap: function (e) {

                        $scope.Reject(id);
                    }

                },]
            });

        }









        $scope.datas = {};
        $scope.send_qote = '0';
        $scope.nda_flag = '0';
        $scope.sale_flag = '0';
        $scope.send = function (id, email) {
            $scope.send_qote = '1';
            $scope.row_id = id;
            $scope.row_email = email;
            var myPopup = $ionicPopup.show({
                template: '<input type = "text" ng-model ="datas.price" placeholder="Price">',
                title: 'Enter Amount',
                subTitle: 'Enter your amount',
               // cssClass: 'for-check',
                scope: $scope,
                buttons: [{
                    text: 'OK',
                    onTap: function (e) {

                        $scope.sendback();
                    }

                },]
            });

        }
         $scope.NDA_view = function (id) {
            $ionicLoading.show();
            $scope.ndaDetails =[];
            $scope.for_id = id;
            var quotes = {
                id: $scope.for_id

            }
            Quotes.NDA_view(quotes).then(function (responce) {
          //    alert(JSON.stringify(responce.data.document));
                if (responce.data.success) {

                    $scope.ndaDetails.push(responce.data.document);
                     $location.path('/ndaDetails').search({
                        data: $scope.ndaDetails,id:$scope.for_id
                    });
                 /*   var myPopup = $ionicPopup.show({
                        template: 'Client Name:'+responce.data.document.client_name+'<br>'+
                        'Current Date:'+responce.data.document.current_date+'<br>'+
                        'Title:'+responce.data.document.title+'<br>'+
                         'Admin Sign:'+responce.data.document.admin_sign+'<br>'+
                         'Type:'+responce.data.document.type+'<br>',
                        title: 'NDA Document',
                        buttons: [{
                            text: 'Cancel'
                        },
                            {
                        text: 'Send',
                        type: 'button-positive',
                        onTap: function (e) {
                            $scope.NDA(id);
                        }
                      }]
                    });*/


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Proposal Is Not Accepted By Client",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
         }
        
        $scope.saleagreement_view = function (id) {
            $ionicLoading.show();
           $scope.salDetails=[];
            $scope.for_id = id;
            var quotes = {
                id: $scope.for_id
            }
            Quotes.saleagreement_view(quotes).then(function (responce) {
           //  alert(JSON.stringify(responce.data.document));
                if (responce.data.success) {
                    $scope.salDetails.push(responce.data.document)
                     $location.path('/saleAgreementDetail').search({
                        data: $scope.salDetails,id:$scope.for_id
                    });

                     /* var myPopup = $ionicPopup.show({
                        template: 
                        'Client Name:'+responce.data.document.client_name+'<br>'+
                        'Current Date:'+responce.data.document.admin_sign_datetime+'<br>'+
                        'Title:'+responce.data.document.title+'<br>'+
                         'Admin Sign:'+responce.data.document.admin_sign+'<br>'+
                         'Type:'+responce.data.document.type+'<br>'+
                         'Description:  <br>'+responce.data.document.description+'<br>',
                          title: 'Saleagreement Document',
                          buttons: [{
                            text: 'Cancel'
                        },
                            {
                        text: 'Send',
                        type: 'button-positive',
                        onTap: function (e) {
                            $scope.saleagreement(id);
                        }
                      }]
                    });
*/

                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Proposal Is Not Accepted By Client",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
       
        $scope.goBack = function () {

            $state.go('tab.activity');
        }


    })
.controller('saleAgreementDetail', function ($scope, $filter, $interval, $location, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Quotes, $ionicHistory) {
   $ionicLoading.hide();
    var urlParams = $location.search();
    $scope.data = urlParams.data;
    $scope.id = urlParams.id;

     $scope.saleagreement = function () {
            $scope.sale_flag='1';
            var quotes = {
                id: $scope.id
            };
            Quotes.saleagreement(quotes).then(function (responce) {

                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: "Saleagreement Document Added Successfully",
                        buttons: [{
                            text: 'OK'
                        },]
                    });


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Proposal Is Not Accepted By Client",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
    })
.controller('ndaDetails', function ($scope, $filter, $interval, $location, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Quotes, $ionicHistory) {
    $ionicLoading.hide();
    var urlParams = $location.search();
    $scope.data = urlParams.data;
    $scope.id = urlParams.id;

    $scope.NDA = function () {
          
            var quotes = {
                id: $scope.id
            };
            Quotes.NDA(quotes).then(function (responce) {

                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: "Document Added Successfully",
                        buttons: [{
                            text: 'OK'
                        },]
                    });


                    return false;
                    $ionicLoading.hide();


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Proposal Is Not Accepted By Client",
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
})
    .controller('quotesDetail', function ($scope, $filter, $interval, $location, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Quotes, $ionicHistory) {

        var urlParams = $location.search();
        $scope.data = urlParams.data;
        $scope.selectedItem=[];
       // alert(JSON.stringify(urlParams.data));
       console.log(urlParams.data);
          angular.forEach(urlParams.data, function (value, key) {
            angular.forEach(value['group'], function (value2, key2) {
               
                       //console.log(key + ': ' + value['pageTitle']);
                      $scope.selectedItem.push({ 'g_id': value2.id,'val': 0});
                      });
                  });
       // console.log('fffffffffffff');
        //console.log($scope.selectedItem);
        $scope.also='';
        $scope.showchange =  false;
        $interval(function () {

            if ($scope.also != 0) {
                $scope.showchange =  true;
            };
            //$scope.check();
        }, 1000);
        //$scope.dependent =0;
        $scope.toggleGroup = function (group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };
        $scope.isGroupShown = function (group) {
            return $scope.shownGroup === group;
        };
        $scope.request_quote = function () {
            $scope.user.name = localStorage.getItem('name');
            $scope.user.email =localStorage.getItem('email');
            $scope.user.phone =localStorage.getItem('phone');
        
            var myPopup = $ionicPopup.show({
                template: ' <input type = "text" ng-model ="user.name"  placeholder="User name" />' +
                    '<input type = "text" ng-model ="user.email" placeholder="Email" />' +
                    '<input type = "text" ng-model ="user.phone" placeholder="Phone">',
                templateUrl: '',
                title: 'Enter Details',
                subTitle: 'Enter your Details',
                cssClass: 'for-check',
                scope: $scope,
                buttons: [{

                    text: 'Cancel',
                    onTap: function (e) {

                        return false;
                    }
                },

                {
                    text: 'OK',
                    onTap: function (e) {

                        return false;
                    }

                },]
            });
        }
        $scope.result3 = [];
        $scope.tex = '';
        $scope.rad = '';
        $scope.check1 = '';
        $scope.it = [];
        /*$scope.textbox = function(rate, name, hours) {
        	$scope.tex++;
            var value = $("#check").val();
            var rate = rate;
            var name = name;
            var hours = hours;
            var total = value * rate;
            $scope.result3.push(total);
            alert($scope.it);
            return false;
            document.getElementById('calculations').appendChild(rate);
            var myEl = angular.element(document.querySelector('#calculations'));
            myEl.append(name + '  $' + total + '-' + hours + ' hours<br>');


        }*/
        $scope.result = [];
        $scope.its = {};
        $scope.data123 = [];
        $scope.isChecked2 = function (id) {
            var match = true;
            return match;
        }
        $scope.isChecked2();
        $scope.isChecked = function (id) {
            var match = false;
            for (var i = 0; i < $scope.data123.length; i++) {
                if ($scope.data123[i].id == id) {

                    match = true;
                }
            }

            return match;
        };
        $scope.textbox = "";

        $scope.three_month = function (period) {
           // alert(JSON.stringify($scope.data123));
          
           
    angular.forEach($scope.data123, function (val1, key) {

        angular.forEach(val1['duration'], function (val2, key2) {
            if (val1['id'] == val2['itemid'] && val2['period'] == period) {
              if (val1['typeid'] !=3 ) {
              if (val1['id'] ==$scope.itemselected.id) {
                $scope.data123[key]['rate'] = val2['rate'];
              };
             }
               
               if (val1['typeid'] ==3 ) {
              if (val1['id'] ==$scope.itemselected.id) {
              var tot=  $scope.textbox*val2['rate'];
                $scope.data123[key]['rate'] = tot;
              };
             }
             // console.log($scope.itemselected.id)
              // console.log($scope.data123);
           
               };  
           })
        })     
      }
   
      $scope.alcopunter =0;
      
        $scope.sync = function (bool, item, check,group_ids) {
   
            $scope.arraycheck=[];
$scope.itemselected= item;
            $scope.alli = [];
            if (check == 'true') {
                if (item != '') {
                    $scope.data123.push(item);
                };
                console.log($scope.data123);
                return false;
            };
            if (bool) {
                $scope.ccccc(group_ids);
                $scope.check1++;
                $scope.check_groupid = item.group_id;
                // add item
                $scope.data123.push(item);

                 if (item.dependent >0) {
                   angular.forEach(item.duration, function (val, key) {
                    if (val['period'] =='3 month') {  

                         $scope.three_month(val['period']);
                    };

                   }) 
                };

                   /* angular.forEach($scope.data123, function (val, key) {*/
                //   $scope.arraycheck.push('group id '+'='+item.group_id+ 'counter'+'='+$scope.alcopunter++);

              /*     }) */
console.log($scope.arraycheck);
            } else {
                 // remove item
                $scope.reverse(group_ids);               
                $scope.check1--;
                $scope.check_groupid = item.group_id;
                for (var i = 0; i < $scope.data123.length; i++) {
                    if ($scope.data123[i].id == item.id) {
                        $scope.data123.splice(i, 1);
                    }
                }
            }
        };
$scope.fffff =0;
//$scope.selectedItem=[];
/*$scope.fff2[21] = 0;
$scope.fff2[22] = 0;
$scope.fff2[23] = 0;
$scope.fff2[24] = 0;*/
 $scope.ccccc = function (a) {
 $scope.fffff++;
    angular.forEach($scope.selectedItem, function (val, key) {
                   
                    if (val['g_id'] ==a) {  
                        val['val']= parseFloat(val['val'])+parseFloat(1);
                    };

                   }) 
 
  //$scope.selectedItem[a]= parseFloat($scope.selectedItem[a])+parseFloat(1);
 
  //$scope.fffff++;
   console.log('ddddddddd');
  console.log($scope.selectedItem);
 }
  $scope.reverse = function (a) {

    //console.log($scope.selectedItem);
  //  return false;
   //$scope.fff2.push(a+','+$scope.fffff);
 $scope.fffff++;

    angular.forEach($scope.selectedItem, function (val, key) {
                   
                    if (val['g_id'] ==a) {  
                        val['val']= parseFloat(val['val'])-parseFloat(1);
                    };

                   }) 
 
  //$scope.selectedItem[a]= parseFloat($scope.selectedItem[a])+parseFloat(1);
 
  //$scope.fffff++;
  console.log($scope.selectedItem);
 }
        $scope.sync2 = function (bool, item,group_ids) {

            $scope.itemselected= item;
            var x = angular.element(document.getElementById("text_" + item.id));
            $scope.textbox = x.val();
            //alert(JSON.stringify(item));
           // console.log(item);
           // return false;
            if ($scope.textbox != '') {
                var a = $scope.textbox;
                var b=  a.length;
                if (b == 1) {
                
                $scope.check_groupid = item.group_id;
                $scope.tex++;
                var rate = item.rate;
                var name = item.name;
                var hours = item.hours;
                var total = $scope.textbox * rate;
                /*console.log('gggggg');
                console.log($scope.data123);*/
                
                
               
               if ($scope.data123 !=' ') {
                angular.forEach($scope.data123, function (a) {
                    if ($scope.data123.indexOf(item) === -1) {
                         $scope.data123.push(item);
                          $scope.result3.push(total);
                          $scope.ccccc(group_ids);
                             // alert('asad');
                            }

                    });
   /*
                    for (var i = 0; i < $scope.data123.length; i++) {
                      
                    if ($scope.data123[i].id !=  item.id) { 
                       
                        // alert(das);
                          $scope.ccccc(group_ids);
                          $scope.data123.push(item);
                          $scope.result3.push(total);
                       //  $scope.three_month(val['period']);
                  
                    }
                 }*/
                 console.log('ffffffff');
                 console.log($scope.data123);
                 };
                 if ($scope.data123 == '') {
                 $scope.ccccc(group_ids);
               $scope.data123.push(item);
                $scope.result3.push(total);

                 };               
                if (item.dependent >0) {
                   angular.forEach(item.duration, function (val, key) {
                    if (val['period'] =='3 month') {     
                         $scope.three_month(val['period']);
                    };

                   }) 
               }
                };
                if (b != 1) {
                    $scope.reverse(group_ids);
                $scope.tex_groupid = item.group_id;
                $scope.tex--;
                for (var i = 0; i < $scope.data123.length; i++) {
                    if ($scope.data123[i].id == item.id) {
                        $scope.data123.splice(i, 1);
                    }
                }
              // again add in array
                $scope.ccccc(group_ids);
                $scope.check_groupid = item.group_id;
                $scope.tex++;
                var rate = item.rate;
                var name = item.name;
                var hours = item.hours;
                var total = $scope.textbox * rate;
                $scope.data123.push(item);
                $scope.result3.push(total);
                if (item.dependent >0) {
                   angular.forEach(item.duration, function (val, key) {
                    if (val['period'] =='3 month') {  
                         $scope.three_month(val['period']);
                    };

                   }) 
            };
            console.log($scope.data123);
               };
                /*document.getElementById('calculations').appendChild(rate);*/
                /*alert(JSON.stringify($scope.data123));*/
                /*var myEl = angular.element(document.querySelector('#calculations'));
                myEl.append(name + '  $' + total + '-' + hours + ' hours<br>');*/
            };
            if ($scope.textbox == '') {
               
                $scope.reverse(group_ids);
                $scope.tex_groupid = item.group_id;
                $scope.tex--;
                for (var i = 0; i < $scope.data123.length; i++) {
                    if ($scope.data123[i] == item) {
                           
                        $scope.data123.splice(i, 1);
                        
                    }
                }

            };
        };

        $scope.sync3 = function (bool, item) {
            $scope.itemselected= item;
            var x = angular.element(document.getElementById("radio_" + item.id));
            $scope.radio_value = x.val();
            $scope.data123 = [];
            if ($scope.radio_value == 'on') {
                $scope.R_groupid = item.group_id;
                $scope.rad = 1;
                $scope.data123.push(item);

            } else {
                $scope.rad--;
                for (var i = 0; i < $scope.data123.length; i++) {
                    if ($scope.data123[i].id == item.id) {
                        $scope.data123.splice(i, 1);
                    }
                }
            }
        };


        /*$scope.checkbox = function(rate, name, hours,id) {
        	console.log($scope.its.adult);
        	angular.forEach($scope.its, function(value, index) {
        		alert(value);
        		alert(value.id);
		    	if(value.val)
		       		console.log('value.id');//here you can get selected value
		   });
        	$scope.check1++;
            var rate = rate;
            var name = name;
            var hours = hours;
            //$scope.result.push(rate);
           	//alert(angular.element('#'+id).val());
            /*document.getElementById('calculations').appendChild(rate);*\/
            var myEl = angular.element(document.querySelector('#calculations'));
            myEl.append(name + '  $' + rate + '-' + hours + ' hours<br>');

        }*/

        $scope.itemsaa = 0;
        $scope.result1 = [];
        $scope.radio = function (rate, name, hours) {
            $scope.rad++;
            var rate = rate;
            var name = name;
            var hours = hours;
            $scope.result1.push(rate);

            /*document.getElementById('calculations').appendChild(rate);*/
            var myEl = angular.element(document.querySelector('#calculations'));
            myEl.append(name + '  $' + rate + '-' + hours + ' hours<br>');
        }
        $scope.user = {};
        $scope.check = function () {

            $scope.alltotals = [];

            function getSum2(total, num) {
                return total + num;
            }
            if ($scope.result != '') {
                $scope.totalcheckbox = $scope.result.reduce(getSum2);

            };
            if ($scope.result1 != '') {
                $scope.totalradio = $scope.result1.reduce(getSum2);
            }
            if ($scope.result3 != '') {
                $scope.totaltextbox = $scope.result3.reduce(getSum2);
            }
            if ($scope.result3 == '') {
                $scope.totaltextbox = '';
            }
            if ($scope.result == '') {
                $scope.totalcheckbox = '';
            }
            if ($scope.result1 == '') {
                $scope.totalradio = '';
            }
            var total = $scope.totalcheckbox + $scope.totalradio + $scope.totaltextbox;
            $scope.alltotals = total;
            var num = total / 100;
            $scope.change = num * 30;
            if ($scope.change == '') {
                $scope.change = 0;
            }
            if ($scope.alltotals == '') {
                $scope.alltotals = 0;
            }
            if ($scope.change != '') {
                $scope.change = parseFloat($scope.change);
            };
            if ($scope.alltotals != '') {
                $scope.alltotals = parseFloat($scope.alltotals);
            };


            $scope.to = $scope.change + $scope.alltotals;



        }

        $interval(function () {
            $scope.check();
        }, 1000);
        $scope.savedata = function () {
            $scope.alltotal = [];

            function getSum(total, num) {
                return total + num;
            }
            if ($scope.result != '') {
                $scope.totalcheckbox = $scope.result.reduce(getSum);

            };
            if ($scope.result1 != '') {
                $scope.totalradio = $scope.result1.reduce(getSum);
            }
            if ($scope.result3 != '') {
                $scope.totaltextbox = $scope.result3.reduce(getSum);
            }
            if ($scope.result3 == '') {
                $scope.totaltextbox = '';
            }
            if ($scope.result == '') {
                $scope.totalcheckbox = '';
            }
            if ($scope.result1 == '') {
                $scope.totalradio = '';
            }
            var total = $scope.totalcheckbox + $scope.totalradio + $scope.totaltextbox;
            /*myEl.append(name + '  $' + rate + '-' + hours + ' hours<br>');*/
            $scope.details12 = [];
            angular.forEach($scope.data123, function (value, key) {
                console.log(value['name'] + '$' + value['rate'] + '' + value['hours'] + 'hours');
                $scope.details12.push(value['name'] + ' ' + '$' + value['rate'] + '-' + value['hours'] + ' ' + 'hours');
            });
            /* alert(JSON.stringify($scope.gggggg));
             return false;*/
            //var detail = document.getElementById("calculations").innerHTML
            // var detail = ;
            $scope.totals = total;
            /*var myEl = angular.element( document.querySelector( '#calculations' ) );
            myEl.append('<br><br><br>Total are '+total+'<br>'); */


            var quotes = {
                name: $scope.user.name,
                email: $scope.user.email,
                phone: $scope.user.phone,
                detail: $scope.details12,
                totalprice: total
            };
            Quotes.saverequest(quotes).then(function (responce) {

                //alert(JSON.stringify(responce.data));
                if (responce.data.success) {

                    var myPopup = $ionicPopup.show({
                        template: "Request send successfully",
                        buttons: [{
                            text: 'OK',
                            onTap: function (e) {
                                $scope.goBack();
                            }

                        },]
                    });

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    /*checkExpiry($scope,responce,$state);*/
                }
            }, function (err) {
                $ionicLoading.hide();
            });


        }
        $scope.get_services = function () {

            $scope.services = [];
            Quotes.get_services().then(function (responce) {

                //alert(JSON.stringify(responce.data));
                if (responce.data.success) {
                    $scope.services = responce.data.data;
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    /*checkExpiry($scope,responce,$state);*/
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.get_services();

        $scope.get_packages_item = function () {

            $scope.items_idsss = [];
            $scope.selectedPackage = [];
            //$scope.items_idsss.push($scope.user.packages);
            var a = $scope.user.packages;
            $scope.items_idsss = a.split(',');
            //console.log($scope.items_idsss);

            //console.log($scope.data);
            angular.forEach($scope.data, function (val1, key) {
       
                angular.forEach(val1['group'], function (val2, key) {
                    // console.log(value['items']);
                    
                    angular.forEach(val2['items'], function (val, key) {

                        angular.forEach($scope.items_idsss, function (value, key) {
                            //        alert('r');
                            if (value == val['id']) {
                                $scope.ccccc(val['group_id']);
                                if (val2['type'] == 'checkbox') {

                                    $scope.check1++;
                                    $scope.check_groupid = val2['id'];
                                    
                                };
                                if (val2['type'] == 'radio') {
                                    $scope.rad = 1;
                                    $scope.R_groupid = val2['id'];

                                };
                                if (val2['type'] == 'textbox') {
                                    $scope.tex++;
                                    $scope.tex_groupid = val2['id'];
                                    $scope.ccccc(val2['group_id']);
                                };
                                //alert(JSON.stringify(val2['type']));
                                $scope.selectedPackage.push(val);
                            };

                        });

                    });
                });
            });


            angular.forEach($scope.selectedPackage, function (value, key) {

                $scope.sync('true', value, 'true');


            });
        }

        $scope.get_all_packages = function () {

            var quotes = {
                id: $scope.user.services,

            };
            $scope.packages = [];
            Quotes.get_all_packages(quotes).then(function (responce) {

                //alert(JSON.stringify(responce.data));
                if (responce.data.success) {
                    $scope.packages = responce.data.data;
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    /*checkExpiry($scope,responce,$state);*/
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        //$scope.get_all_packages();
        $scope.isaradio = function (item) {
         $scope.dependent11 =[];
         $scope.dependent11 =item;
            return $scope.dependent11;
        }
        
        $scope.get_against_services = function () {
            $scope.get_all_packages();
            var quotes = {
                id: $scope.user.services,

            };

            Quotes.get_against_services(quotes).then(function (responce) {

                //alert(JSON.stringify(responce.data));
                if (responce.data.success) {
                    $scope.data = responce.data.data;
                    console.log($scope.data);
                   /*   angular.forEach(responce.data.data[0].group, function (value, key) {
                       //console.log(key + ': ' + value['pageTitle']);
                      $scope.selectedItem.push({ 'g_id': value.id,'val': 0});
                  });*/
                  //  $scope.selectedItem = responce.data.data[0].group;

                    //alert(JSON.stringify(responce.data.data[0].group));
                    console.log('sss');
                    console.log($scope.data);

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    /*checkExpiry($scope,responce,$state);*/
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.goBack = function () {

            $state.go('tab.quotes');
        }



    })
    .controller('ProjectsDetailCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Projects, $ionicHistory) {
        $scope.projectDetail = [];
        $scope.total_meetings = [];
        $scope.team_member = [];
        $scope.total_requirements = [];
        $scope.total_pending = [];
        $scope.total_documents = [];
        $scope.total_tasks = [];
        $scope.late = [];
        $scope.total_invoices = [];
        $scope.total_complete = [];
        $scope.invoiceId = 0;
        $scope.showDetail = 0;
        $scope.userType = localStorage.getItem('type');
        $scope.getProject = function (project_id) {

            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $scope.projectsList = [];
            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token'),
                project_id: project_id
            };
            Projects.getProjectDetail(project).then(function (responce) {

                //alert(JSON.stringify(responce.success));
                if (responce.data.success) {
                    // alert(responce.data.project_detail.nextPaymentDue);
                    $scope.showDetail = 1;
                    console.log(responce.data.project_detail);
                    $ionicLoading.hide();
                    $scope.projectDetail = responce.data.project_detail;
                    $scope.total_meetings = responce.data.total_meetings;
                    $scope.late = responce.data.late_task;
                    $scope.total_pending = responce.data.total_pending;
                    $scope.total_invoices = responce.data.total_invoices;
                    $scope.total_documents = responce.data.total_documents;
                    $scope.total_requirements = responce.data.total_req;
                    $scope.total_complete = responce.data.total_complete;
                    $scope.team_member = responce.data.team_member;
                    if (responce.data.project_detail.nextPaymentDue == '' || responce.data.project_detail.nextPaymentDue == null) {
                        $scope.invoiceId = 'not';
                    };
                    if (responce.data.project_detail.nextPaymentDue != '' || responce.data.project_detail.nextPaymentDue != null) {
                        $scope.invoiceId = responce.data.project_detail.nextPaymentDue;
                    };

                    console.log($scope.projectDetail);
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "database is false",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    /*checkExpiry($scope,responce,$state);*/
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getProject($stateParams.projectid);
        $scope.goBack = function () {
            console.log('here');
            //$ionicHistory.goBack(-1);
            $state.go('tab/projects');
        }
        $scope.projectTasks = function (pid) {
            console.log(pid);
            $state.go('projectTasks', {
                projectid: pid
            });
        }
        $scope.messages = function ($project_id) {
            $state.go('tab/projects');
        }
        $scope.payInvoice = function () {
            $state.go('payInvoice', {
                "invoiceid": $scope.projectDetail.nextPaymentDue.id
            })
        }
        $scope.newMessage = function () {
            $state.go('addMessage', {
                "projectid": $stateParams.projectid
            })
        }

    })
    .controller('TasksCtrl', function ($ionicHistory, $scope, $ionicPopover, $location, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Tasks, $timeout) {
        //$scope.projectid = 0;
        $scope.companies = [];
        $scope.task = {};
        $scope.project_selected12 = [];
        $scope.projectsList = [];
        $scope.userso = [];

        var url = $location.path().split('/');
        $scope.firstParameter = url[1];
        $scope.secondParameter = url[2];
        // alert($scope.secondParameter);

        if ($scope.firstParameter == 'projectTasks') {
            $scope.projectid = $scope.secondParameter = url[2];
        }
        if ($scope.firstParameter == 'tab') {
            $scope.projectid = 0;
        }
        $scope.currentUser = localStorage.getItem('user_id');
        $scope.userType = localStorage.getItem('type');
        if (localStorage.getItem('token') == null) {
            $state.go('/login');
        }
        $scope.tasksList = [];
        $scope.user_id = localStorage.getItem('user_id');
        $scope.IsVisible = false;

        $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.

            $scope.IsVisible = true;


        }
         $scope.allprojects = function () {

             Tasks.allprojects().then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                       // console.log(responce.data.data.listings[i]);
                       $scope.projectsList.push(responce.data.projects[i]);
                    }
                    console.log($scope.tasksList);
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
         }
         $scope.allprojects();
        $scope.allTasks = function () {
      updatelink($scope, "pending_tab");
            //$scope.pending1 = false; // select green color on tab
            $scope.tasksList = [];
           
            var task = {
                token: localStorage.getItem('token')          
            };
            Tasks.allTasks(task).then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.data.listings.length; i++) {
                       // console.log(responce.data.data.listings[i]);
                        $scope.tasksList.push(responce.data.data.listings[i]);
                    }
                    console.log($scope.tasksList);
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
$scope.allTasks();

        $scope.getTasks = function () {
            //updatelink($scope,"completed_tab");
            updatelink($scope, "pending_tab"); // select green color on tab
            $scope.tasksList = [];
            var project_id = $scope.task.projects;
            $scope.pending1 = true;


            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                status: 'pending',
                projectid: $scope.task.projects
            };
            Tasks.getTasks(task).then(function (responce) {
                if (responce.data.success == false) {
                    var myPopup1 = $ionicPopup.show({
                        template: 'Please Select Project First',
                        buttons: [{
                            text: 'OK',
                            type: 'btn btn-pink'
                        },]
                    });
                };
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.tasks.received.length; i++) {
                        console.log(responce.data.tasks.received[i]);
                        $scope.tasksList.push(responce.data.tasks.received[i]);
                    }
                    if ($scope.tasksList != '') {
                        $scope.ShowHide();
                    };
                    for (var i = 0; i < responce.data.tasks.assigned.length; i++) {
                        console.log(responce.data.tasks.assigned[i]);
                        $scope.tasksList.push(responce.data.tasks.assigned[i]);
                    }
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK',
                            type: 'btn btn-pink'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        if ($scope.secondParameter != 'tasks') {

            $scope.task.projects = $scope.secondParameter;
            $scope.getTasks();

        }
        $scope.deleteTask = function (id) {
            var task = {
                token: localStorage.getItem('token')
            }
            Tasks.deleteTask(id, task).then(function (responce) {
                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    console.log(id);
                    $scope.getTasks();
                }

            }, function (err) {
                $ionicLoading.hide();
            });
        };
        $scope.getCompleteTasks = function () {
            updatelink($scope, "completed_tab");
            $scope.pending1 = false; // select green color on tab
            $scope.tasksList = [];
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                status: 'completed',
                projectid: $scope.task.projects
            };
            Tasks.getTasks(task).then(function (responce) {
                if (responce.data.success == false) {
                    var myPopup1 = $ionicPopup.show({
                        template: 'Please Select Project First',
                        buttons: [{
                            text: 'OK',
                            type: 'btn btn-pink'
                        },]
                    });
                };
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.tasks.length; i++) {
                        console.log(responce.data.tasks[i]);
                        $scope.tasksList.push(responce.data.tasks[i]);
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.projectid = $stateParams.projectid
        if (angular.isUndefined($scope.projectid)) {
            $scope.projectid = 0;
        }
        $scope.gettasks_user = function () {

            /* if ($scope.task.category !='all') {
             	alert('ggg');
             };*/
            $scope.tasksList = [];
            if ($scope.pending1 == true) {

                $scope.statusoftasks = 'pending';
            }
            if ($scope.pending1 == false) {

                $scope.statusoftasks = 'completed';
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                statusoftasks: $scope.statusoftasks,
                status: $scope.task.category,
                projectid: $scope.task.projects

            };
            Tasks.gettasks_user(task).then(function (responce) {

                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.tasks.length; i++) {
                        console.log(responce.data.tasks[i]);
                        $scope.tasksList.push(responce.data.tasks[i]);
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.projectid = $stateParams.projectid
        if (angular.isUndefined($scope.projectid)) {
            $scope.projectid = 0;
        }
        //$scope.getTasks();
        $scope.getCompanies = function () {
            // /updatelink($scope,"pending_tab"); // select green color on tab//mn ny ki hai

            $scope.ShowHide();
            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token')

            };
            Tasks.getCompanies(project).then(function (responce) {
                if (responce.data.success) {
                    //$scope.projectsList = [];
                    $ionicLoading.hide();
                    console.log(responce);
                    for (var i = 0; i < responce.data.companies.length; i++) {
                        console.log(responce.data.companies[i])
                        $scope.companies.push(responce.data.companies[i]);
                        //$scope.projects = responce.data.projects;
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompanies(); 
        $scope.getProjects = function () {
            //updatelink($scope,"pending_tab"); // select green color on tab
              $scope.projectsList = [];
            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                status: 2,
                company_id: $scope.task.compnay
            };
            Tasks.getProjects(task).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                   $scope.tasksList =[];
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.projectsList.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getallusers_associate_to_project = function () {
            //updatelink($scope,"pending_tab"); // select green color on tab

            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                project_id: $scope.task.projects
            };
            Tasks.getusers(task).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {

                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.users.length; i++) {
                        $scope.userso.push(responce.data.users[i]);
                        //$scope.projects = responce.data.projects;
                    }


                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getallProjectsfor = function () {
            //updatelink($scope,"pending_tab"); // select green color on tab

            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token')
            };
            Tasks.getallProjectsfor(task).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.project_selected12.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

     
        $scope.getallProjectsfor();
        $scope.getTaskDetail = function ($task_id) {
            $state.go('taskDetail', {
                "taskid": $task_id
            })
        }
        $scope.editTask = function (taskid) {
            $state.go('editTask', {
                "id": taskid
            });
        }
        $scope.addTask = function () {

            $state.go('addTask', {
                "projectid": $stateParams.projectid
            });
        }
    })
    .controller('TasksDetailCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Tasks, $ionicHistory, $ionicViewService) {

        $scope.taskDetail = [];
        $scope.taskTodo = [];
        $scope.showDetail = 0;
        $scope.currentUser = localStorage.getItem('user_id');
        $scope.getTaskDetail = function (taskid) {
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                task_id: taskid
            };
            Tasks.getTaskDetail(task).then(function (responce) {
                console.log(responce);
                $scope.taskTodo = [];
                if (responce.data.success) {
                    $scope.showDetail = 1;
                    $ionicLoading.hide();
                    $scope.taskDetail = responce.data.taskDetail;
                    for (var i = 0; i < responce.data.taskDetailTodoList.length; i++) {
                        $scope.taskTodo.push(responce.data.taskDetailTodoList[i]);
                    }
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.deleteToDoTask = function (id) {
            var task = {
                token: localStorage.getItem('token')
            }
            Tasks.deleteToDoTask(id, task).then(function (responce) {
                if (responce.data.success) {
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $scope.getTaskDetail($stateParams.taskid);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        };
        $scope.getTaskDetail($stateParams.taskid);
        $scope.taskCompleted = function () {
            Tasks.taskComplete($stateParams.taskid).then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $state.go('tab.tasks');
                }
            })
        }
        $scope.goBack = function () {

            $state.go('tab.tasks');
        }
        $scope.completeTask = function (id, task_id, type) {
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                todo_id: id,
                task_id: task_id,
                type: type
            };
            Tasks.updateStatus(task).then(function (responce) {
                if (responce.data.success) {
                    $scope.taskTodo = [];
                    $ionicLoading.hide();
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.taskDetailTodoList.length; i++) {
                        $scope.taskTodo.push(responce.data.taskDetailTodoList[i]);
                    }
                    /*var myPopup = $ionicPopup.show({
                    		template: responce.data.data.message,
                    		buttons: [
                    		{ text: 'OK' },
                    		]
                    	});*/
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
    })
    .controller('AddTaskCtrl', function ($scope, $ionicPopover, $state, $stateParams, $location, $filter, $http, $ionicModal, $ionicLoading, $ionicPopup, Tasks, $ionicHistory, ionicDatePicker) {
        var url = $location.path().split('/');
        $scope.firstParameter = url[1];
        $scope.showhide = false;
        $scope.secondParameter = url[2];
        $scope.requirements = [];
        // alert($scope.secondParameter);
        if ($scope.secondParameter != '') {
            $scope.showhide = true;
            // alert('jjj');
            $scope.projectsrecipts = function () {
                //alert('ggg');
                $ionicLoading.show({
                    content: 'loading'
                })
                var task = {
                    //token: localStorage.getItem('token')
                    project_id: $scope.secondParameter

                };
                Tasks.getReciptsByproject(task).then(function (responce) {
                    console.log(responce.data.users);
                    //$scope.items = [];
                    for (var i = 0; i < responce.data.users.length; i++) {
                        $scope.users_project.push(responce.data.users[i]);
                    }
                    $ionicLoading.hide();
                    //	alert('asad');
                    $scope.getprojectsByCompnay();
                }, function (err) {
                    $ionicLoading.hide();
                });

            }
   
            $scope.getRequirementsfor = function () {


                $ionicLoading.show({
                    content: 'loading'
                })
                var task12 = {
                    project_id: $scope.secondParameter
                };

                Tasks.getRequirements(task12).then(function (responce) {
                    //alert(JSON.stringify(responce.data.requirements));
                    console.log(responce.data.requirements);

                    for (var i = 0; i < responce.data.requirements.length; i++) {
                        $scope.requirements.push(responce.data.requirements[i]);
                    }
                    $ionicLoading.hide();
                }, function (err) {
                    $ionicLoading.hide();
                });
            }
            $scope.projectsrecipts();
            $scope.getRequirementsfor();
        }

        $scope.addTask = [];
        $scope.items = [];
        $scope.projects = [];
        $scope.project = [];

        $scope.users_project = [];
        $scope.task = {};
        $scope.task_id = $stateParams.projectid;
        $scope.companies = [];
        $scope.toDoListItems = [];
        $scope.userType = localStorage.getItem('type');
        $scope.currentDate;
        $scope.addTask = function (taskid) {
            if ($scope.secondParameter != '') {
                $scope.task.projects = $scope.secondParameter;
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                users: $scope.task.users,
                title: $scope.task.taskName,
                due_date: $filter('date')($scope.currentDate, "yyyy-MM-dd"),
                description: $scope.task.description,
                notes: $scope.task.notes,
                company_id: $scope.task.compnay,
                audience: $scope.task.audience,
                priority: $scope.task.priority,
                todo: $scope.toDoListItems,
                project_id: $scope.task.projects,
                requirement_id: $scope.task.requirements,
                description_checklist: $scope.toDoListItems
            };

            //alert(JSON.stringify(task));
            Tasks.addTask(task).then(function (responce) {
                //alert(responce);
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $scope.clearFields();
                    $state.go('tab.tasks');
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'Ok'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.clearFields = function () {
            $scope.task.taskName = "";
            $scope.task.priority = "";
            $scope.task.description = "";
            $scope.task.notes = "";
        }
        $scope.currentDate = new Date();

        $scope.datePickerCallback = function (val) {
            if (!val) {
                console.log('Date not selected');
            } else {
                console.log('Selected date is : ', val);
            }
        };

        $scope.getRecipts = function () {

            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
            };
            Tasks.getRecipts(task).then(function (responce) {
                //alert(responce);
                console.log(responce);

                if (responce.data.success) {
                    //alert();
                    $ionicLoading.hide();
                    //alert(JSON.stringify(responce.data.data.project));
                    for (var i = 0; i < responce.data.data.users.length; i++) {
                        $scope.items.push(responce.data.data.users[i]);
                    }
                    for (var i = 0; i < responce.data.data.companies.length; i++) {
                        $scope.companies.push(responce.data.data.companies[i]);
                    }

                    /*for(var i=0;i<responce.data.data.project.length;i++){
                    	$scope.project.push(responce.data.data.project[i]);
                    }*/

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        /*$scope.getProjects= function(){
		
		$ionicLoading.show({content:'loading'})
		var task = {
			company_id: $scope.task.compnay,
		};
		alert(JSON.stringify(task));
		Tasks.getProjects(task).then(function(responce){
			//alert(responce);
			console.log(responce);
			
			if(responce.data.success){
			
				$ionicLoading.hide();
				for(var i=0;i<responce.data.data.projects.length;i++){
					$scope.items.push(responce.data.data.projects[i]);
				}
				
			}else{
				$ionicLoading.hide();
				var myPopup = $ionicPopup.show({
						template: responce.data.data.message,
						buttons: [
						{ text: 'OK' },
						]
					});
				checkExpiry($scope,responce,$state);
			}
		},function(err){
			$ionicLoading.hide();
		});
        }*/
        $scope.getRecipts();
        //$scope.getProjects();
        $scope.onValueChanged = function (val) {
            //	alert(val);
            console.log(val);
            $scope.task.users = val;
        }
        $scope.checkusers = function () {
            if ($scope.userType == 'admin' || $scope.userType == 'super_admin') {
                if ($scope.users_project == "") {
                    //alert('kal');
                    var myPopup = $ionicPopup.show({
                        template: 'First select company and project',
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }
            if ($scope.userType == 'client') {
                if ($scope.users_project == "") {
                    //alert('kal');
                    var myPopup = $ionicPopup.show({
                        template: 'First select project',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }
        }

        $scope.onValueChangedforprojects = function (val) {
            //	alert('bbb');
            console.log(val);
            $scope.task.projects = val;
        }
        $scope.getCustomText = function (val) {
            console.log(val);
            $scope.task.users = val;
        }

        $scope.getReciptsByCompnay = function () {

            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                company_id: $scope.task.compnay

            };
            Tasks.getReciptsByCompnay(task).then(function (responce) {
                console.log(responce.data.users);
                //$scope.items = [];
                for (var i = 0; i < responce.data.users.length; i++) {
                    $scope.items.push(responce.data.users[i]);
                }
                $ionicLoading.hide();
                //	alert('asad');
                $scope.getprojectsByCompnay();
            }, function (err) {
                $ionicLoading.hide();
            });

        }

        $scope.getReciptsByproject = function () {

            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                //token: localStorage.getItem('token')
                project_id: $scope.task.projects

            };
            Tasks.getReciptsByproject(task).then(function (responce) {
                console.log(responce.data.users);
                //$scope.items = [];
                for (var i = 0; i < responce.data.users.length; i++) {
                    $scope.users_project.push(responce.data.users[i]);
                }
                $ionicLoading.hide();
                //	alert('asad');
                $scope.getprojectsByCompnay();
            }, function (err) {
                $ionicLoading.hide();
            });

        }
        $scope.getprojectsByCompnay = function () {

            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                company_id: $scope.task.compnay
            };

            Tasks.getprojectsByCompnay(task).then(function (responce) {
                //alert(JSON.stringify(responce.data.projects));
                console.log(responce.data.projects);
                $scope.projects = [];
                for (var i = 0; i < responce.data.projects.length; i++) {
                    $scope.projects.push(responce.data.projects[i]);
                }
                $ionicLoading.hide();
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        //alert('fff');
$scope.allprojects = function () {
             Tasks.allprojects().then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                       // console.log(responce.data.data.listings[i]);
                       $scope.projects.push(responce.data.projects[i]);
                    }
                    console.log($scope.tasksList);
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
         }
         $scope.allprojects();
        $scope.getRequirements = function () {
            if ($scope.secondParameter != '') {
                $scope.task.projects = $scope.secondParameter;
            }

            $ionicLoading.show({
                content: 'loading'
            })
            var task12 = {
                project_id: $scope.task.projects
            };

            Tasks.getRequirements(task12).then(function (responce) {
                //alert(JSON.stringify(responce.data.requirements));
                console.log(responce.data.requirements);

                for (var i = 0; i < responce.data.requirements.length; i++) {
                    $scope.requirements.push(responce.data.requirements[i]);
                }
                $ionicLoading.hide();
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.openDatePickerOne = function (val) {
            var ipObj1 = {
                callback: function (val) { //Mandatory
                    console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    $scope.currentDate = new Date(val);
                },
                from: new Date(),
                to: new Date(2022, 10, 30),
                inputDate: new Date(),
                mondayFirst: true,
                disableWeekdays: [],
                closeOnSelect: false,
                templateType: 'popup'
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $ionicModal.fromTemplateUrl('modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.AddItem = function (data) {
            //alert(data.newItem);

            if (data.newItem != "") {
                //alert(data.newItem);
                $scope.toDoListItems.push(data.newItem);
                //alert($scope.toDoListItems);
                data.newItem = ' ';
            }
        };
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
        $scope.removeItem = function (index) {

            $scope.toDoListItems.splice(index, 1);

        }
    })
    .controller('EditTaskCtrl', function ($filter, Tasks, $cordovaFileTransfer, $stateParams, $cordovaFile, $state, $scope, $ionicPopover, $http, $ionicModal, $ionicLoading, $ionicPopup, $cordovaImagePicker, Profile, $cordovaCamera, $cordovaDevice, $cordovaActionSheet, $ionicViewService, ionicDatePicker) {
        $scope.getTask = function (taskid) {
            Tasks.getTaskById(taskid).then(function (task) {
                $scope.task = task.data;
                console.log($scope.task);
                var editTask = {
                    id: $scope.task.id,
                    taskName: $scope.task.title,
                    description: $scope.task.description,
                    dueDate: $scope.task.niceDate
                }
                $scope.task = editTask;
            })
        }
        $scope.getTask($stateParams.id);
        $scope.openDatePickerOne = function (val) {
            var ipObj1 = {
                callback: function (val) { //Mandatory
                    console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    $scope.task.dueDate = new Date(val);
                },
                from: new Date(),
                to: new Date(2022, 10, 30),
                inputDate: new Date(),
                mondayFirst: true,
                disableWeekdays: [],
                closeOnSelect: false,
                templateType: 'popup'
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };
        $scope.editTask = function (taskid) {
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
                title: $scope.task.taskName,
                due_date: $scope.task.dueDate, //due_date: $filter('date')($scope.task.dueDate, "MM/dd/yyyy"),
                description: $scope.task.description,
            };
            console.log(task);
            Tasks.editTask($stateParams.id, task).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    //checkExpiry($scope,responce,$state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
    })
    .controller('DocumentCtrl', function ($scope, $ionicPopover, $location, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Documents, $timeout) {
        var url = $location.path().split('/');
        $scope.firstParameter = url[1];
        $scope.secondParameter = url[2];
        $scope.userType = localStorage.getItem('type');
        updatelink($scope, "pending_tab");
        //$scope.projectid = 0;
        $scope.companies = [];
        $scope.projectsList = [];
        $scope.project_selected12 = [];
        $scope.task = {};
        if (localStorage.getItem('token') == null) {
            $state.go('/login');
        }
        $scope.documentsList = [];
        $scope.getCompanies = function () {
            // /updatelink($scope,"pending_tab"); // select green color on tab//mn ny ki hai

            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token')

            };
            Documents.getCompanies(project).then(function (responce) {
                if (responce.data.success) {
                    //$scope.projectsList = [];
                    $ionicLoading.hide();
                    console.log(responce);
                    for (var i = 0; i < responce.data.companies.length; i++) {
                        console.log(responce.data.companies[i])
                        $scope.companies.push(responce.data.companies[i]);
                        //$scope.projects = responce.data.projects;
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompanies();
        $scope.alldocs = function () {
            //$scope.pending1 = false; // select green color on tab
            var task = {
                token: localStorage.getItem('token')          
            };
            Documents.alldocs(task).then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.data.listings.length; i++) {
                        console.log(responce.data.data.listings[i]);
                        $scope.documentsList.push(responce.data.data.listings[i]);
                    }
                    
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
$scope.alldocs();
        $scope.getallProjectsfor = function () {
            //updatelink($scope,"pending_tab"); // select green color on tab

            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token')
            };
            Documents.getallProjectsfor(task).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.project_selected12.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getallProjectsfor();
        $scope.getDocuments = function () {
            updatelink($scope, "pending_tab"); // select green color on tab
            $scope.documentsList = [];
            if ($scope.firstParameter == 'tab') {
                $scope.projectid = $scope.task.projects;
            }
            if ($scope.firstParameter == 'projectDocuments') {
                $scope.projectid = $scope.secondParameter;

            };
            $ionicLoading.show({
                content: 'loading'
            })
            var document = {
                token: localStorage.getItem('token'),
                status: 'pending',
                projectid: $scope.projectid
            };
            Documents.getDocuments(document).then(function (responce) {
                console.log(responce);
                $ionicLoading.hide();
                if (responce.data.success) {
                    for (var i = 0; i < responce.data.documents.length; i++) {
                        console.log(responce.data.documents[i]);
                        $scope.documentsList.push(responce.data.documents[i]);
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Please select project first",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    //checkExpiry($scope,responce,$state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getProjects = function () {
            //updatelink($scope,"pending_tab"); // select green color on tab
             $scope.projectsList = [];
            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token'),
                status: 2,
                company_id: $scope.task.compnay
            };
            Documents.getProjects(project).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $scope.documentsList=[];
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.projectsList.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
          $scope.allprojects = function () {

             Documents.allprojects().then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                       // console.log(responce.data.data.listings[i]);
                       $scope.projectsList.push(responce.data.projects[i]);
                    }
                    console.log($scope.tasksList);
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
         }
         $scope.allprojects();
        $scope.getCompleteDocuments = function () {
            updatelink($scope, "completed_tab"); // select green color on tab
            $scope.documentsList = [];
            if ($scope.firstParameter == 'tab') {
                $scope.projectid = $scope.task.projects;
            }
            if ($scope.firstParameter == 'projectDocuments') {
                $scope.projectid = $scope.secondParameter;

            };
            $ionicLoading.show({
                content: 'loading'
            })
            var document = {
                token: localStorage.getItem('token'),
                status: 'completed',
                projectid: $scope.projectid

            };
            Documents.getDocuments(document).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.documents.length; i++) {
                        console.log(responce.data.documents[i]);
                        $scope.documentsList.push(responce.data.documents[i]);
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: "Please select project first", //responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    //checkExpiry($scope,responce,$state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.projectid = $stateParams.projectid
        if (angular.isUndefined($scope.projectid)) {
            $scope.projectid = 0;
        }
        //$scope.getDocuments();
        $scope.getDocumentDetail = function ($document_id) {
            $state.go('documentDetail', {
                "documentid": $document_id
            })
        }

    })
    .controller('historOfDocument', function ($location, $scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Documents, $ionicHistory, $ionicViewService) {
        var urlParams = $location.search();
        $scope.data = urlParams.data;
        $scope.goBack = function () {

            //$ionicHistory.goBack(-1);
            $state.go('documentDetail', {
                documentid: urlParams.documentid
            });
        }
    })
    .controller('DocumentDetailCtrl', function ($location, $scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Documents, $ionicHistory, $ionicViewService) {
        $scope.documentDetail = [];
        $scope.documentRow = [];
        $scope.documenthistory = [];
        $scope.userType = localStorage.getItem('type');
        $scope.showDetail = 0;
        $scope.doucment_id = 0;
        $scope.getDocumentDetail = function (documentid) {
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            //alert(documentid);
            var document = {
                token: localStorage.getItem('token'),
                document_id: documentid,
            };
            Documents.getDocumentDetail(document).then(function (responce) {

                $ionicLoading.hide();
                if (responce.data.success) {


                    $scope.showDetail = 1;

                    $scope.documentRow = responce.data;
                    $scope.documentDetail = responce.data.content;
                    $scope.subCategoryModel = decodeURIComponent($scope.documentRow.document.admin_sighn);
                    $scope.picture_admin = picturesUrl + "assets/images/signature/" + $scope.documentRow.document.admin_sighn;
                    $scope.picture_client = picturesUrl + "assets/images/signature/" + $scope.documentRow.document.client_sighn;

                    // alert(JSON.stringify($scope.documentRow));

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.getDocumentDetail($stateParams.documentid);
        $scope.forsignDocument = function () {

            $location.path('/DocumentSighn').search({
                data: $stateParams.documentid
            });;


        }
        $scope.signDocument = function () {
            $ionicLoading.show({
                content: 'loading'
            });
            //alert();
            var document = {
                token: localStorage.getItem('token'),

                document_id: $stateParams.documentid,
            };
            Documents.signDocument(document).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.history = function () {
            $ionicLoading.show({
                content: 'loading'
            })
            //alert();
            var document = {
                token: localStorage.getItem('token'),

                document_id: $stateParams.documentid,
            };
            Documents.history(document).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {

                    for (var i = 0; i < responce.data.history.length; i++) {
                        console.log(responce.data.history[i]);
                        $scope.documenthistory.push(responce.data.history[i]);
                    }

                    $ionicLoading.hide();
                    $location.path('/history').search({
                        data: $scope.documenthistory,
                        documentid: $stateParams.documentid
                    });

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.verify = function () {
            $ionicLoading.show({
                content: 'loading'
            })
            //alert();
            var document = {
                token: localStorage.getItem('token'),

                document_id: $stateParams.documentid,
            };
            Documents.verify(document).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.sendToClient = function (did) {
            $ionicLoading.show({
                content: 'loading'
            })
            var document = {
                token: localStorage.getItem('token'),
                document_id: $stateParams.documentid,
            };
            Documents.sendToClient(document).then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.goBack = function () {
            $state.go('tab.documents');
        }
    })
    .controller('DocumentSighn', function ($location, $state, $scope, Documents, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Documents, $ionicHistory, $ionicViewService) {
        var urlParams = $location.search();
        $scope.userType = localStorage.getItem('type');

        $scope.goBack = function () {

            //$ionicHistory.goBack(-1);
            $state.go('documentDetail', {
                documentid: urlParams.data
            });
        }

        $scope.savepic = function () {
            var canvas = document.getElementById('canvasasdf');
            var ctx = canvas.getContext('2d');


            $scope.mediumQuality = canvas.toDataURL('image/jpeg', 0.5);
            /*return false;	*/
            var image = new Image();
            image.id = "pic"
            image.src = canvas.toDataURL();
            $scope.img = image.src;
            /*document.getElementById('image_for_crop').appendChild(image);*/
            console.log(image.src);


        }
        $scope.clear = function () {
            var context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        $scope.signDocument = function () {
            $scope.savepic();
            //$ionicLoading.show({content:'loading'});
            //$scope.password='test123';
            $scope.data = {};
            $scope.myPopup1 = $ionicPopup.show({
                template: '<input type = "text" ng-model ="data.model" placeholder="Password">',
                title: 'Enter Password',
                subTitle: 'Enter your password',
                cssClass: 'for-check',
                scope: $scope,

                buttons: [{

                    text: 'Cancel',
                    onTap: function (e) {

                        return false;
                    }
                },
                {
                    text: '<b>Submit</b>',
                    type: 'button-positive',
                    onTap: function (e) {


                        $scope.password = $scope.data.model;

                        var document = {
                            token: localStorage.getItem('token'),
                            sighn_image: $scope.img,
                            password: $scope.password,
                            document_id: urlParams.data
                        };
                        Documents.signDocument(document).then(function (responce) {
                            console.log(responce);
                            if (responce.data.success) {
                                $ionicLoading.hide();
                                var myPopup = $ionicPopup.show({
                                    template: responce.data.data.message,
                                    buttons: [{
                                        text: 'OK',
                                        onTap: function (e) {
                                            $scope.goBack();
                                        }
                                    },]
                                });
                            } else {
                                $ionicLoading.hide();
                                var myPopup = $ionicPopup.show({
                                    template: responce.data.data.message,
                                    buttons: [{
                                        text: 'OK'
                                    },]
                                });
                                checkExpiry($scope, responce, $state);
                            }
                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }

                }
                ]
            });



        }



    })
    .controller('InvoiceCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Invoices, $timeout) {

        $scope.addInvoices = [];
        $scope.projectid = 0;
        $scope.projectsList = [];
        $scope.invoice = {};
        $scope.task = {};
        $scope.companies = [];
        $scope.userType = localStorage.getItem('type');
        if (localStorage.getItem('token') == null) {
            $state.go('/login');
        }
        $scope.invoicesList = [];
        $scope.getCompanies = function () {
            // /updatelink($scope,"pending_tab"); // select green color on tab//mn ny ki hai

            $scope.userType = localStorage.getItem('type');

            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token')

            };
            Invoices.getCompanies(project).then(function (responce) {
                if (responce.data.success) {
                    //$scope.projectsList = [];
                    $ionicLoading.hide();
                    console.log(responce);
                    for (var i = 0; i < responce.data.companies.length; i++) {
                        console.log(responce.data.companies[i])
                        $scope.companies.push(responce.data.companies[i]);
                        //$scope.projects = responce.data.projects;
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompanies();

        $scope.allinvoices = function () {
            updatelink($scope, "pending_tab");
           
            var task = {
                token: localStorage.getItem('token')          
            };
            Invoices.allinvoices(task).then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.data.listings.length; i++) {
                        console.log(responce.data.data.listings[i]);
                        $scope.invoicesList.push(responce.data.data.listings[i]);
                    }

                    
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
$scope.allinvoices();
        $scope.getprojectone = function () {
            //return;
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token')
            };

            Invoices.getprojectone(task).then(function (task) {
                //console.log(task);
                //console.log(task);

                if (task.data.success) {
                    //alert('dddd');
                    //alert(JSON.stringify(task.data.projects));
                    $ionicLoading.hide();
                    $scope.project_selected12 = task.data.projects;

                    //$scope.project_selected.push(abc);


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: task.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, task, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getprojectone();
        $scope.allprojects = function () {

             Invoices.allprojects().then(function (responce) {
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                       // console.log(responce.data.data.listings[i]);
                       $scope.projectsList.push(responce.data.projects[i]);
                    }
                    console.log($scope.tasksList);
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }, function (err) {
                $ionicLoading.hide();
            });
         }
         $scope.allprojects();
        $scope.getProjects = function () {

            //updatelink($scope,"pending_tab"); // select green color on tab
             $scope.projectsList = [];
            $scope.userType = localStorage.getItem('type');
            $ionicLoading.show({
                content: 'loading'
            })
            var project = {
                token: localStorage.getItem('token'),
                status: 2,
                company_id: $scope.task.compnay
            };
            Invoices.getProjects(project).then(function (responce) {

                console.log(responce);
                if (responce.data.success) {
                    $scope.invoicesList =[];
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.projects.length; i++) {
                        $scope.projectsList.push(responce.data.projects[i]);
                        //$scope.projects = responce.data.projects;
                    }

                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.getInvoices = function () {

            updatelink($scope, "pending_tab"); // select green color on tab
            $scope.invoicesList = [];
            $ionicLoading.show({
                content: 'loading'
            })

            var invoice = {
                token: localStorage.getItem('token'),
                status: 'pending',
                projectid: $scope.task.projects
            };
            Invoices.getInvoices(invoice).then(function (responce) {
                $ionicLoading.hide();
                if (responce.data.success == false) {
                    var myPopup1 = $ionicPopup.show({
                        template: 'Please Select Project First',
                        buttons: [{
                            text: 'OK',
                            type: 'btn btn-pink'
                        },]
                    });
                };
                if (responce.data.success) {
                    for (var i = 0; i < responce.data.invoices.length; i++) {
                        $scope.invoicesList.push(responce.data.invoices[i]);
                    }
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getCompleteInvoices = function () {
            updatelink($scope, "completed_tab"); // select green color on tab
            $scope.invoicesList = [];
            $ionicLoading.show({
                content: 'loading'
            })

            var invoice = {
                token: localStorage.getItem('token'),
                status: 'completed',
                projectid: $scope.task.projects
            };
            Invoices.getInvoices(invoice).then(function (responce) {
                if (responce.data.success == false) {
                    var myPopup1 = $ionicPopup.show({
                        template: 'Please Select Project First',
                        buttons: [{
                            text: 'OK',
                            type: 'btn btn-pink'
                        },]
                    });
                };
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    for (var i = 0; i < responce.data.invoices.length; i++) {
                        console.log(responce.data.invoices[i]);
                        $scope.invoicesList.push(responce.data.invoices[i]);
                    }
                } else {
                    console.log(responce);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        console.log($stateParams);
        //$scope.getInvoices();
        $scope.projectid = $stateParams.projectid
        if (angular.isUndefined($scope.projectid)) {
            $scope.projectid = 0;
        }
        $scope.getInoviceDetail = function ($invoice_id) {
            $state.go('invoiceDetail', {
                "invoiceid": $invoice_id
            })
        }
        /*$scope.addInvoices = function($invoice_id){
        	$state.go('addInvoices',{"invoiceid":$invoice_id})
        }*/

    })
    .controller('InvoiceDetailCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Invoices, $ionicHistory, $ionicViewService) {
        $scope.invoiceDetail = [];
        $scope.showDetail = 0;
        $scope.getInvoiceDetail = function (invoiceid) {
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var invoice = {
                token: localStorage.getItem('token'),
                invoice_id: invoiceid
            };
            Invoices.getInvoiceDetail(invoice).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $scope.showDetail = 1;
                    $ionicLoading.hide();
                    $scope.invoiceDetail = responce.data.invoice;
                    $scope.invoiceItems = responce.data.invoice_details;
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getInvoiceDetail($stateParams.invoiceid);
        $scope.goBack = function () {
            $ionicViewService.getBackView().go();
        }
        /*$scope.addInvoices = function(projectid){
        	alert('dd');
        	$ionicLoading.show({content:'loading'})
        	var invoice = {
        		token: localStorage.getItem('token'),
        		users: $scope.task.users,
        		title: $scope.task.taskName,
        		due_date: $filter('date')($scope.currentDate, "yyyy-MM-dd"),
        		description: $scope.task.description,
        		notes: $scope.task.notes,
        		company_id: $scope.task.compnay,
        		audience: $scope.task.audience,
        		priority: $scope.task.priority,
        		todo: $scope.toDoListItems,
        		project_id :$scope.task.projects,
        		requirement_id :$scope.task.requirements
        	};
        	
        	//alert(JSON.stringify(task));
        	Tasks.addInvoices(invoice).then(function(responce){
        		//alert(responce);
        		console.log(responce);
        		if(responce.data.success){
        			$ionicLoading.hide();
        			var myPopup = $ionicPopup.show({
        					template: responce.data.data.message,
        					buttons: [
        					{ text: 'OK' },
        					]
        				});
        			$scope.clearFields();
        			$state.go('tab.tasks');
        		}else{
        			$ionicLoading.hide();
        			var myPopup = $ionicPopup.show({
        					template: responce.data.data.message,
        					buttons: [
        					{ text: 'OK' },
        					]
        				});
        			checkExpiry($scope,responce,$state);
        		}
        	},function(err){
        		$ionicLoading.hide();
        	});
        }*/
        $scope.payInvoice = function () {
            $state.go('payInvoice', {
                "invoiceid": $stateParams.invoiceid
            })
        }

    })
    .controller('InvoicePayCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Invoices, $ionicHistory, $ionicViewService) {
        $scope.invoiceDetail = [];
        $scope.invoice = {};
        $scope.years = [];

        var year = new Date().getFullYear();
        var range = [];
        range.push(year);
        for (var i = 1; i < 20; i++) {
            range.push(year + i);
        }
        $scope.years = range;

        $scope.getInvoiceDetail = function (invoiceid) {
            //alert($stateParams.invoiceid);
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }

            $ionicLoading.show({
                content: 'loading'
            })

            var invoice = {
                token: localStorage.getItem('token'),
                invoice_id: invoiceid
            };
            Invoices.getInvoiceDetail(invoice).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    $scope.invoiceDetail = responce.data.invoice;
                    $scope.invoiceItems = responce.data.invoice_details;
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.payInvoice = function (invoiceid) {
            $ionicLoading.show({
                content: 'loading'
            })
            var invoice = {
                token: localStorage.getItem('token'),
                invoice_id: $scope.invoice.id,
                card: $scope.invoice.number,
                month: $scope.invoice.month,
                year: $scope.invoice.year,
                ccv: $scope.invoice.ccv
            };
            Invoices.payInvoice(invoice).then(function (responce) {
                console.log(responce);

                if (responce.data.success) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $state.go('/invoiceDetail');
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });

        }
        console.log($stateParams);
        $scope.getInvoiceDetail($stateParams.invoiceid);
        $scope.invoice.id = $stateParams.invoiceid;
        $scope.goBack = function () {
            //alert('ff');
            $state.go('invoiceDetail', {
                "invoiceid": $stateParams.invoiceid
            });
        }
    })
    .controller('MessagesCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Messages, $timeout) {
        console.log($stateParams);


        if (localStorage.getItem('token') == null) {
            $state.go('/login');
        }



        $scope.noMoreItemsAvailable = false;
        $scope.messageList = [];
        $scope.page = 0;
        $scope.sentMessage = 0;
        $scope.inbox = 1;
        $scope.idofuser = localStorage.getItem('user_id');

        $scope.getInbox = function (index) {
            $scope.idofuser = localStorage.getItem('user_id');
            $scope.inbox = 1;

            if (index == 1) {
                $scope.page = 0;
            }
            updatelink($scope, "pending_tab"); // select green color on tab
            $scope.messageList = [];
            var projectid = $stateParams.projectid
            if (angular.isUndefined(projectid)) {

                projectid = 0;
            }
            searcharr = {
                page: $scope.page,
                token: localStorage.getItem('token'),
                project_id: projectid
            }
            $ionicLoading.show({
                content: 'loading'
            })
            Messages.getInbox(searcharr).then(function (responce) {
                console.log(responce);
                $scope.sentMessage = 0;
                if (responce.data.haveData == true) {
                    $scope.page = $scope.page + 30;
                    $scope.items = responce.data.messages;
                    console.log($scope.items);
                    console.log('test');
                } else {
                    $scope.noMoreItemsAvailable = true;
                }

                console.log($scope.page);
                $ionicLoading.hide();
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function (err) {
                $ionicLoading.hide();
            });

        }
        $scope.getSent = function () {
            $scope.idofuser = localStorage.getItem('user_id');

            $scope.inbox = 0;
            updatelink($scope, "completed_tab"); // select green color on tab

            $scope.messageList = [];
            $ionicLoading.show({
                content: 'loading'
            })
            var projectid = $stateParams.projectid
            if (angular.isUndefined(projectid)) {
                projectid = 0;
            }
            searcharr = {
                page: $scope.page,
                token: localStorage.getItem('token'),
                project_id: projectid
            }
            Messages.getSent(searcharr).then(function (responce) {
                console.log(responce)
                if (responce.data.haveData == true) {
                    $scope.items = responce.data.messages;

                } else {
                    $scope.noMoreItemsAvailable = true;
                }
                $scope.page = $scope.page + 30;
                console.log($scope.page);
                $ionicLoading.hide();
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function (err) {
                $ionicLoading.hide();
            });

        }
        $scope.getSentMessages = function () {
            $scope.inbox = 0;
            $scope.page = 0;
            $scope.sentMessage = 1;
            $scope.noMoreItemsAvailable = false;
            $scope.idofuser = localStorage.getItem('user_id');
            $scope.messageList = [];
            $scope.items = [];
            $scope.getSent();
        }
        $scope.getInbox();
        $scope.messageDetail = function ($message_id) {
            $state.go('messageDetail', {
                "messageid": $message_id
            })
        }
        $scope.addMessage = function () {
            var projectid = $stateParams.projectid
            if (angular.isUndefined(projectid)) {
                projectid = 0;
            }
            $state.go('addMessage', {
                "projectid": projectid
            });
        }
    })
    .controller('MessageDetailCtrl', function ($scope, $ionicPopover, $state, $stateParams, $http, $ionicModal, $ionicLoading, $ionicPopup, Messages, $ionicHistory, $ionicViewService) {

        $scope.messageDetail = [];
        $scope.showDetail = 0;
        $scope.idofuser = localStorage.getItem('user_id');
        $scope.getMessageDetail = function (messageid) {

            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var message = {
                token: localStorage.getItem('token'),
                message_id: messageid
            };
            Messages.getMessageDetail(message).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $scope.showDetail = 1;
                    $ionicLoading.hide();
                    $scope.messageDetail = responce.data.message_detail;
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getMessageDetail($stateParams.messageid);
        $scope.goBack = function () {
            $state.go('messages');
        }
        $scope.messageReply = function ($message_id, $message_subject) {
            $state.go('messageReply', {
                "messageid": $message_id,
                "messageSubject": $message_subject
            })
        }
    })
    .controller('AddMessageCtrl', function ($scope, $ionicPopover, $state, $stateParams, $filter, $http, $ionicModal, $ionicLoading, $ionicPopup, Messages, Tasks, $ionicHistory) {
    
        $scope.addTask = [];
        $scope.items = [];
        $scope.message = {};
        $scope.companies = [];
        $scope.project = [];
        $scope.typeofuser = [];
        $scope.project_selected = [];
        //$scope.projectid =$stateParams.projectid;

        // $scope.list = [
        // {
        // 	index : 1,
        // 	name : "A"
        // },{
        // 	index : 2,
        // 	name : "B"
        // }];
        $scope.userType = localStorage.getItem('type');
        //var userType = localStorage.getItem('type');
        //alert(userType);
        $scope.addMessage = function () {
   

            $ionicLoading.show({
                content: 'loading'
            })

            var message = {
                token: localStorage.getItem('token'),
                users: $scope.message.users,
                subject: $scope.message.subject,
                message: $scope.message.message,
                sendemail: $scope.message.sendemail,
                projectid: $scope.message.project
            };
            console.log(message);
            Messages.addMessage(message).then(function (responce) {
                console.log(responce);

                if (responce.data.success) {
                    //	alert(JSON.stringify(responce.data.messages));
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.messages,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $scope.clearFields();
                    $state.go('messages');
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.clearFields = function () {
            $scope.message.subject = "";
            $scope.message.message = "";
        }
        $scope.checkusers = function () {

            if ($scope.userType == 'admin' || $scope.userType == 'super_admin') {
                if ($scope.items == "" || $scope.items == 'Object') {
                    //	alert('kal');
                    var myPopup = $ionicPopup.show({
                        template: 'First select company',
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                }
            }
            if ($scope.userType == 'client') {
                if ($scope.items == "") {
                    //	alert('kal');
                    var myPopup = $ionicPopup.show({
                        template: 'First select project',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                }
            }
        }

        $scope.getRecipts = function () {
            //return;
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                token: localStorage.getItem('token'),
            };

            Tasks.getRecipts(task).then(function (task) {
                //console.log(task);
                //console.log(task);

                if (task.data.success) {
                    //alert(JSON.stringify(task.data.data.users));
                    $ionicLoading.hide();
                    if ($scope.userType == 'team') {
                        for (var i = 0; i < task.data.data.users.length; i++) {
                            $scope.items.push(task.data.data.users[i]);
                        }
                    }
                    if ($scope.userType == 'admin' || $scope.userType == 'super_admin') {
                        /*for(var i=0;i<task.data.data.users.length;i++){
                        	$scope.items.push(task.data.data.users[i]);
                        }*/

                        for (var i = 0; i < task.data.data.companies.length; i++) {
                            $scope.companies.push(task.data.data.companies[i]);
                        }
                        /*for(var i=0;i<task.data.data.project.length;i++){
                        	$scope.project.push(task.data.data.project[i]);
                        }*/
                    }
                    if ($scope.userType == 'client') {
                        for (var i = 0; i < task.data.data.users.length; i++) {
                            $scope.items.push(task.data.data.users[i]);
                        }

                        /*for(var i=0;i<task.data.data.companies.length;i++){
                        	$scope.companies.push(task.data.data.companies[i]);
                        }*/
                        for (var i = 0; i < task.data.data.project.length; i++) {
                            $scope.project.push(task.data.data.project[i]);
                        }
                    }
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: task.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, task, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getRecipts();

        $scope.message.projectid = $stateParams.projectid
        $scope.onValueChanged = function (val) {
            console.log(val);
            $scope.message.users = val;
        }
        $scope.onValueChangedforproject = function (val) {
            console.log(val);
            $scope.message.projects = val;
        }

        $scope.getCustomText = function (val) {
            console.log(val);

            $scope.message.users = val;

        }

        $scope.getprojectone = function () {
            //return;
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                projectid: $stateParams.projectid,
            };

            Tasks.getprojectone(task).then(function (task) {
                //console.log(task);
                //console.log(task);

                if (task.data.success) {
                    //alert('dddd');
                    //alert(JSON.stringify(task.data.project_one));
                    $ionicLoading.hide();
                    $scope.project_selected12 = task.data.project_one;

                    //$scope.project_selected.push(abc);


                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: task.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, task, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getprojectone();

        $scope.getReciptsByCompnay = function () {
            console.log($scope.message.compnay);
            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                company_id: $scope.message.compnay
            };
            Tasks.getReciptsByCompnay(task).then(function (task) {

                //	alert(JSON.stringify(task));
                //console.log(task.data.users);
                $scope.items = [];
                for (var i = 0; i < task.data.users.length; i++) {
                    $scope.items.push(task.data.users[i]);
                }
                $ionicLoading.hide();
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.getReciptsByproject = function () {

            $ionicLoading.show({
                content: 'loading'
            })
            var task = {
                project_id: $scope.message.project
                //company_id: $scope.task.compnay

            };
            Tasks.getReciptsByproject(task).then(function (responce) {
                console.log(responce.data.users);
                $scope.items = [];
                for (var i = 0; i < responce.data.users.length; i++) {
                    $scope.items.push(responce.data.users[i]);
                }
                $ionicLoading.hide();
                //	alert('asad');
                //$scope.getprojectsByCompnay();
            }, function (err) {
                $ionicLoading.hide();
            });

        }
    })

    .controller('MessageReplyCtrl', function ($scope, $ionicPopover, $state, $stateParams, $filter, $http, $ionicModal, $ionicLoading, $ionicPopup, Messages, Tasks, $ionicHistory) {
        $scope.message = {};
        $scope.addReply = function () {
            $ionicLoading.show({
                content: 'loading'
            })
            var message = {
                token: localStorage.getItem('token'),
                messageid: $scope.message.messageid,
                subject: $scope.message.subject,
                message: $scope.message.message,
                sendemail: $scope.message.sendemail
            };
            Messages.addReply(message).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {

                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $state.go('messages');
                    $scope.clearFields();

                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $state.go('messages');
                    checkExpiry($scope, responce, $state);

                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.clearFields = function () {
            $scope.message.subject = "";
            $scope.message.message = "";
        }
        $scope.setValues = function () {
            $scope.message.messageid = $stateParams.messageid;
            $scope.message.subject = "Re:" + $stateParams.messageSubject;
            return;
        }
        $scope.messageDetail = function ($message_id) {
            $state.go('messageDetail', {
                "messageid": $message_id
            })
        }
        $scope.setValues();
    })
    .controller('MenuCtrl', function ($scope, $ionicPopover, $http, $ionicModal, $ionicLoading, $ionicPopup, Projects, $timeout) {

        $scope.showPopup = function () {
            $scope.data = {};
            $scope.userType = localStorage.getItem('type');
            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: ' <div class="row"> <div class="col align-center margin_10"><button class="btn btn-small pink" ui-sref="profile" href="#/profile" ng-click="confirmClick()">Profile</button></div> </div><div class="row"> <div class="col align-center"><button class="btn btn-small outline cancel-btn" ui-sref="logout" href="#/logout" ng-click="confirmClick()">Logout</button></div> </div>',
                title: '',
                subTitle: '',
                scope: $scope,
                buttons: [{
                    text: 'Cancel',
                    type: 'cancel-btn pink-color'
                }]
            });

            myPopup.then(function (res) {
                //alert('asdsad');
                myPopup.close();
            });
            $scope.confirmClick = function () {
                //alert('ffff');
                myPopup.close();
            }

            /*$timeout(function() {
     		myPopup.close(); //close the popup after 3 seconds for some reason
  		    }, 3000);
            */
        };
    })
    .controller('ChatsCtrl', function ($scope, Chats) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        $scope.chats = Chats.all();
        $scope.remove = function (chat) {
            Chats.remove(chat);
        };
    })
    .controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
        $scope.chat = Chats.get($stateParams.chatId);
    })
    .controller('AccountCtrl', function ($scope) {
        $scope.settings = {
            enableFriends: true
        };
    })
    .controller('SignUpCtrl', function ($scope, $state, $ionicLoading, $ionicPopup, $http, $cordovaImagePicker, AuthService) {

        //varibles declaration
        $scope.signup = {}
        $scope.signup.time_zone = 'ASIA/KOLKATA'

        //mobile number validation	
        $scope.checkMob = function () {
            if ($scope.signup.phone.toString().length > 10) {
                $scope.signup.phone = Number($scope.signup.phone.toString().substr(0, $scope.signup.phone.toString().length - 1))
            }
        }

        $scope.register = function () {

            var fieldsValid = $scope.validateFields();
            $ionicLoading.hide()
            if (fieldsValid) {
                var user = {
                    firstname: $scope.signup.fname,
                    lastname: $scope.signup.lname,
                    phone: $scope.signup.phone,
                    email: $scope.signup.email,
                    password: $scope.signup.password,
                    type: $scope.signup.typeofuserregister,
                };
                $ionicLoading.show({
                    template: 'Registering new user...'
                });

                AuthService.doRegister(user).then(function (user) {
                    if (user.data.success) {
                        var myPopup1 = $ionicPopup.show({
                            template: 'Your registration is complete! One of our team members will contact you shortly.',
                            buttons: [{
                                text: 'OK'
                            },]
                        });

                        console.log(user.data.data.email);
                        localStorage.setItem('user_id', user.data.data.id)
                        localStorage.setItem('token', user.data.data.token)
                        localStorage.setItem('name', user.data.data.name)
                        localStorage.setItem('email', user.data.data.email)
                        $ionicLoading.hide();
                        $state.go('login');

                    } else {

                        console.log(user);
                        $ionicLoading.hide();
                        var myPopup = $ionicPopup.show({
                            template: user.data.data.message,
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                        checkExpiry($scope, responce, $state);
                    }
                }, function (err) {
                    $ionicLoading.hide();
                });
            }
        }

        $scope.validateFields = function () {
            //clear all the messages before validating
            $scope.clearValidationMessages();

            return (
                $scope.validateFirstName() &&
                $scope.validateLastName() &&
                $scope.validateSingupEmail() &&
                $scope.validatePassword()
            )
        }

        $scope.clearValidationMessages = function () {
            $scope.invalidPasswordMessage = "";
            $scope.invalidFirstNameMessage = "";
            $scope.invalidLastNameMessage = "";
            $scope.invalidPasswordConfirmMessage = "";
            $scope.invalidEmailMessage = "";
        }

        $scope.validateFirstName = function () {
            var isFnameValid = true;
            if (angular.isDefined($scope.signup.fname)) {
                var regex = /^[a-zA-Z]{1,50}$/;

                isFnameValid = $scope.signup.fname.match(regex) ? true : false;

                if (!isFnameValid) {
                    $scope.invalidFirstNameMessage = "Invalid last name characters";
                }
            } else {
                isFnameValid = false;
                $scope.invalidFirstNameMessage = "Please enter a first name";
            }

            return isFnameValid;
        }
        $scope.validateLastName = function () {
            var isLnameValid = true;
            if (angular.isDefined($scope.signup.lname)) {
                var regex = /^[a-zA-Z]{1,50}$/;
                isLnameValid = $scope.signup.fname.match(regex) ? true : false;

                if (!isLnameValid) {
                    $scope.invalidLastNameMessage = "Invalid last name characters";
                }
            } else {
                isLnameValid = false;
                $scope.invalidLastNameMessage = "Please enter a last name";
            }
            return isLnameValid;
        }
        $scope.validateSingupEmail = function () {
            if (angular.isUndefined($scope.signup.email)) {
                $scope.invalidEmailMessage = "You must enter an email to proceed.";
                return false;
            }
            var isValidEmail = validateEmail($scope.signup.email);
            if (!isValidEmail) {
                $scope.invalidEmailMessage = "Invalid email address";
            }
            return isValidEmail;
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        $scope.validatePassword = function () {
            if (angular.isUndefined($scope.signup.password)) {
                $scope.invalidPasswordMessage = "You must enter a password to proceed."
                return false;
            }

            if (angular.isUndefined($scope.signup.passwordConfirm)) {
                $scope.invalidPasswordConfirmMessage = "Please enter a confirmation password to proceed."
                return false;
            }

            var isPassValid = true;
            if ($scope.signup.password != '') {
                var pass = $scope.password;
                if ($scope.signup.passwordConfirm != '') {
                    isPassValid = $scope.signup.password == $scope.signup.passwordConfirm ? true : false;
                    if (!isPassValid) {
                        $scope.invalidPasswordConfirmMessage = "Passwords do not match"
                    }
                } else {
                    $scope.invalidPasswordConfirmMessage = "Please enter a confirmation password."
                    isPassValid = false;
                }
            } else {
                $scope.invalidPasswordMessage = "Please enter a password to proceed."
                isPassValid = false;
            }
            return isPassValid;
        }




    })
    .controller('LoginCtrl', function ($ionicPlatform, $scope, $state, $ionicLoading, $http, $ionicPopup, AuthService) {
        $ionicPlatform.registerBackButtonAction(function (event) {
            if (localStorage.getItem('user_id') != '') {
                ionic.Platform.exitApp();
                $state.go('tab.projects');
            }
        }, 401);
        $scope.data = {};


        $scope.logout = function () {
            //alert('ssss');
            $ionicLoading.show({
                content: 'loading'
            })
            AuthService.doLogout(localStorage.getItem('token')).then(function (user) {
                localStorage.clear()
                $ionicLoading.hide()
                localStorage.setItem('token', null)
                $state.go('login')
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.login = function () {
            $scope.error = $scope.validateLogin();
            var isLoginValid = $scope.validateLogin();
            if (isLoginValid) {
                var user = {
                    email: $scope.data.email,
                    password: $scope.data.password
                };
                $ionicLoading.show({
                    template: 'Loading...'
                });
                AuthService.doLogin(user).then(function (user) {
                    if (user.data.success) {

                        console.log(user);
                        localStorage.setItem('user_id', user.data.data.user_id)
                        localStorage.setItem('token', user.data.data.token)
                        localStorage.setItem('name', user.data.data.name)
                        localStorage.setItem('email', user.data.data.email)
                        localStorage.setItem('type', user.data.data.type)
                        localStorage.setItem('phone', user.data.data.phone)
                        $ionicLoading.hide();
                        $state.go('tab.activity');
                    } else {
                        $ionicLoading.hide();
                        var myPopup = $ionicPopup.show({
                            template: user.data.data.message,
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                        checkExpiry($scope, user, $state);
                    }
                }, function (err) {
                    $ionicLoading.hide();
                });
            }
        }

        $scope.validateLogin = function () {
            $scope.clearLoginValidationMessages();
            return (
                $scope.validateLoginEmail() &&
                $scope.validatePassword()
            )
        }

        $scope.clearLoginValidationMessages = function () {
            $scope.invalidEmailMessage = "";
            $scope.invalidPasswordMessage = "";
        }

        $scope.validateLoginEmail = function () {
            if (angular.isUndefined($scope.data.email)) {
                $scope.invalidEmailClass = 'invalid';
                $scope.invalidEmailMessage = "You must enter an email to proceed.";
                return false;
            }
            var isValidEmail = validateEmail($scope.data.email);
            if (!isValidEmail) {
                $scope.invalidEmailClass = 'invalid';
                $scope.invalidEmailMessage = "Invalid email address";
            } else {
                $scope.invalidEmailClass = '';
            }
            return isValidEmail;
        }
        $scope.validatePassword = function () {
            if (angular.isUndefined($scope.data.password)) {
                $scope.invalidPasswordClass = 'invalid';
                $scope.invalidPasswordMessage = "You must enter a password to proceed."
                return false;
            }

            var isPassValid = true;
            if ($scope.data.password != '') {
                $scope.invalidPasswordClass = '';
                var pass = $scope.password;
            } else {
                $scope.invalidPasswordClass = 'invalid';
                $scope.invalidPasswordMessage = "Please enter a password to proceed."
                isPassValid = false;
            }
            return isPassValid;
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        //forgot password
        $scope.forgotPopup = function () {
            $scope.data = {};
            $scope.myPopup1 = "";
            $scope.myPopup1 = $ionicPopup.show({
                template: '<input type = "text" ng-model = "data.model" placeholder="E-mail">',
                title: 'Forgot Password',
                subTitle: 'Enter your e-mail id',
                cssClass: 'for-check',
                scope: $scope,

                buttons: [{

                    text: 'Cancel',
                    onTap: function (e) {
                        return false;
                    }
                },
                {
                    text: '<b>Submit</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.model) {
                            e.preventDefault();
                        } else {
                            res = $scope.data.model;
                            if (validateEmail(res)) {
                                $ionicLoading.show({
                                    content: 'Loading',
                                    maxWidth: 200,
                                    showDelay: 0
                                })
                                var fd = new FormData();
                                //fd.append('email', res);
                                fd = $scope.data.model;
                                //alert(fd);
                                req = {
                                    method: 'POST',
                                    url: baseUrl + 'User/forgotPassword',
                                    headers: {
                                        'Content-Type': undefined
                                    },
                                    data: {
                                        email: $scope.data.model
                                    }
                                }
                                $http(req).then(function (data) {
                                    //alert(data.data.success);
                                    if (data.data.success == false) {
                                        var myPopup = $ionicPopup.alert({
                                            title: 'Email not found',
                                        });
                                        setTimeout(function () {

                                            $scope.forgotPopup();

                                        }, 2000);

                                        $ionicLoading.hide()
                                    };
                                    if (data.data.success == true) {
                                        var myPopup = $ionicPopup.alert({
                                            title: 'Email send successfully',
                                            subTitle: 'Check your Email',
                                        });
                                        $ionicLoading.hide()
                                    }
                                }, function (err) {
                                    $ionicLoading.hide()
                                })
                            } else {
                                var myPopup = $ionicPopup.alert({
                                    title: 'Invalid Email',
                                    subTitle: 'Please enter correct email',
                                });
                                setTimeout(function () {
                                    $scope.forgotPopup();
                                }, 2000);
                            }

                        }
                    }
                }
                ]
            });
        };

    })
    .controller('LogoutCtrl', function ($ionicPlatform, $scope, $state, $ionicLoading, $http, $ionicPopup, AuthService) {
        //alert('d');
        $scope.data = {};
        console.log('logoutctr');
        $scope.logout = function () {
            //alert('ddd');
            console.log('logout function');
            $ionicLoading.show({
                content: 'loading'
            })
            AuthService.doLogout(localStorage.getItem('token')).then(function (user) {
                localStorage.clear();
                $ionicLoading.hide();
                //myPopup.close();
                localStorage.setItem('token', null)
                $state.go('login');
            }, function (err) {
                $ionicLoading.hide();
            });
        }
        $scope.logout();
    })
    .controller('ProfileCtrl', function ($cordovaFileTransfer, $cordovaFile, $state, $scope, $ionicPopover, $http, $ionicModal, $ionicLoading, $ionicPopup, $cordovaImagePicker, Profile, $cordovaCamera, $cordovaDevice, $cordovaActionSheet, $ionicViewService) {
        $scope.profile = {};
      //  localStorage.setItem('')
        $scope.image = null;
        $scope.profileImage = null;
        $scope.getProfile = function () {
            $ionicLoading.show({
                template: 'Geting Profile Data'
            });
            //alert(localStorage.getItem('token'));
            var user = {
                token: localStorage.getItem('token'),
            };
            //alert(JSON.stringify(user))
            Profile.getProfile(user).then(function (user) {
                console.log(user);
                if (user.data.success) {
                    $ionicLoading.hide();
                    $scope.profile.fname = user.data.data.firstname;
                    $scope.profile.lname = user.data.data.lastname;
                    $scope.profile.email = user.data.data.email;
                    $scope.profile.company = user.data.data.company_name.name;
                    $scope.profile.phone = user.data.data.phone;
                } else {
                    console.log(user);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }

        $scope.getProfile();
        $scope.updateProfile = function () {
            $ionicLoading.hide()
            $ionicLoading.show({
                template: 'Updating Profile'
            });
            var user = {
                firstname: $scope.profile.fname,
                lastname: $scope.profile.lname,
                email: $scope.profile.email,
                company: $scope.profile.company,
                phone: $scope.profile.phone,
                token: localStorage.getItem('token')
            };
            Profile.updateProfile(user).then(function (user) {
                if (user.data.success) {

                    console.log(user);
                    $ionicLoading.hide();

                    var myPopup = $ionicPopup.show({

                        template: user.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                } else {
                    console.log(user);
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: user.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });

        }
        $scope.goBack = function () {

            $state.go('tab.activity');
        }
    })
    .controller('GlobalCtrl', function (Messages, $cordovaFileTransfer, $cordovaFile, $state, $scope, $ionicPopover, $http, $ionicModal, $ionicLoading, $ionicPopup, $cordovaImagePicker, Profile, $cordovaCamera, $cordovaDevice, $cordovaActionSheet, $ionicViewService) {
        $scope.userType = localStorage.getItem('type');
        $scope.getMessageCount = function () {
            if (localStorage.getItem('user_id') == null) {
                $state.go('login');
            }
            $ionicLoading.show({
                content: 'loading'
            })
            var message = {
                token: localStorage.getItem('token'),
                //message_id: messageid
            };
            Messages.getUnreadMessage(message).then(function (responce) {
                console.log(responce);
                if (responce.data.success) {
                    $ionicLoading.hide();
                    $scope.messageCount = responce.data.message_count;
                    console.log($scope.messageCount);
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: responce.data.data.message,
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    checkExpiry($scope, responce, $state);
                }
            }, function (err) {
                $ionicLoading.hide();
            });
        }
    })
    .controller('fortype', function (Messages, $cordovaFileTransfer, $cordovaFile, $state, $scope, $ionicPopover, $http, $ionicModal, $ionicLoading, $ionicPopup, $cordovaImagePicker, Profile, $cordovaCamera, $cordovaDevice, $cordovaActionSheet, $ionicViewService) {

        $scope.userType = localStorage.getItem('type');


    })
    .controller('SocialLoginCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, mainSocialService, facebookFactory, twitterFactory, googleFactory, instagramFactory, linkedinFactory) {
        var url = $location.path().split('/');
        $scope.projectid = url[2];

        localStorage.setItem('projects', $scope.projectid);

        console.log($cordovaInAppBrowser)
        $scope.facebookFlag = false;
        $scope.twitterFlag = false;
        $scope.instagramFlag = false;
        $scope.linkedInFlag = false;
        $scope.googleFlag = false;
        $ionicPlatform.registerBackButtonAction(function (event) {
            if (localStorage.getItem('user_id') != '') {
                ionic.Platform.exitApp();
                $state.go('login');
            }
        }, 401);
        $ionicLoading.show({
            template: 'Loading...'
        });
        $scope.apiresult = true;
        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user).then(function (result) {
            console.log(result)
            if (result.data.status == "Success") {
                //set social media status
                $scope.facebookFlag = result.data.socialMediasStatus.facebook
                $scope.twitterFlag = result.data.socialMediasStatus.twitter
                $scope.instagramFlag = result.data.socialMediasStatus.instagram
                $scope.linkedInFlag = result.data.socialMediasStatus.linkedIn
                $scope.googleFlag = result.data.socialMediasStatus.google

                //place data in factory
                facebookFactory.set(result.data.socialMediasStatus.facebook);
                twitterFactory.set(result.data.socialMediasStatus.twitter);
                instagramFactory.set(result.data.socialMediasStatus.instagram);
                linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                googleFactory.set(result.data.socialMediasStatus.google);

                $scope.apiresult = true;
                $ionicLoading.hide();
            } else {
                $scope.apiresult = true;
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                    template: 'Something happened wrong try again after some times',
                    buttons: [{
                        text: 'OK'
                    },]
                });
            }
        }, function (err) {
            $ionicLoading.hide();
            $scope.apiresult = true;
        });




        $scope.fnGoFacebookPage = function () {

            $state.go('facebook', {
                projectid: $scope.projectid
            });
        }
        $scope.fnGoTwitterPage = function () {
            $state.go('twitter', {
                projectid: $scope.projectid
            });
        }
        $scope.fnGoLikedInPage = function () {
            $state.go('linkedin', {
                projectid: $scope.projectid
            });
        }
        $scope.fnGoInstagramPage = function () {

            $state.go('instagram', {
                projectid: $scope.projectid
            });

            //$location.path('/instagram').search({
            //   project_id: $scope.projectid
            // });
        }
        $scope.fnGoYoutubePage = function () {
            $state.go('youtube', {
                projectid: $scope.projectid
            });
        }
        $scope.fnGoGooglePlusPage = function () {
            $state.go('googleplus', {
                projectid: $scope.projectid
            });
        }

        $scope.fnLinkFacebook = function () {

            $scope.login(baseUrl + 'social_oauth/link_facebook?user_id=' + localStorage.getItem('user_id') + '&p_id=' + $scope.projectid, baseUrl + 'social_oauth/facebook_callback', 'facebook')
            console.log('ok')

            // ref.addEventListener('loadstart', function (event) { alert(event.url); });
            // let url = baseUrl + 'social_oauth/link_facebook?user_id=' + localStorage.getItem('user_id');
            // $cordovaInAppBrowser.open(url).then(function (e) {
            //     console.log(e)
            // }, function (e) {
            //     console.log(e)
            // })
            // console.log(baseUrl + 'social_oauth/link_facebook?user_id=' + localStorage.getItem('user_id'))
            // $window.open(baseUrl + 'social_oauth/link_facebook?user_id=' + localStorage.getItem('user_id'), '_blank', options);
            // $cordovaInAppBrowser.open(baseUrl + 'social_oauth/link_facebook?user_id=' + localStorage.getItem('user_id'), '_blank', options)
            //     .then(function (event) {
            //         // success
            //         console.log(2)
            //     })
            //     .catch(function (event) {
            //         // error
            //         console.log(3)
            //     });
            // document.addEventListener("loadstop", function () {
            //     console.log(1);

            //     $cordovaInAppBrowser.close();

            // }, false);



        }
        $scope.fnLinkTwitter = function () {
            $scope.login(baseUrl + 'social_oauth/link_twitter?user_id=' + localStorage.getItem('user_id') + '&p_id=' + $scope.projectid, baseUrl + 'social_oauth/twitter_callback', 'twitter')
        }
        $scope.fnLinkLinkedIn = function () {
            $scope.login(baseUrl + 'social_oauth/link_linkedin?user_id=' + localStorage.getItem('user_id') + '&p_id=' + $scope.projectid, baseUrl + 'social_oauth/linkedin_callback', 'linkedin')
        }
        $scope.fnLinkInstagram = function () {
            $scope.login(baseUrl + 'social_oauth/link_instagram?user_id=' + localStorage.getItem('user_id') + '&p_id=' + $scope.projectid, baseUrl + 'social_oauth/instagram_callback', 'instagram')
        }
        //third parameter of function login is pass a page where to route
        //like facebook/instagram etc.i knw should i try o?kok..i will ping you

        $scope.fnLinkGoogle = function () {
            $scope.login(baseUrl + 'social_oauth/link_google?user_id=' + localStorage.getItem('user_id') + '&p_id=' + $scope.projectid, baseUrl + 'social_oauth/google_callback', 'youtube')
        }

        $scope.login = function (url, setoutputUrl, page) {

            // console.log(page)
            // var options = {
            //     location: 'yes',
            //     clearcache: 'yes',
            //     toolbar: 'no'
            // };
            // $cordovaInAppBrowser.open(url, '_blank', options)
            //     .then(function (event) {
            //         // success
            //         console.log('ok')
            //     })
            //     .catch(function (event) {
            //         // error
            //     });

            var options = {
                location: 'no',
                clearcache: 'yes',
                toolbar: 'no'
            };
            document.addEventListener("deviceready", function () {
                $cordovaInAppBrowser.open(url, '_blank', options)
                    .then(function (event) {
                        // success


                        console.log('s')
                        console.log(event)
                    })
                    .catch(function (event) {
                        console.log(error)
                        console.log(event)
                        // error
                    });
            }, false);
            $rootScope.$on('$cordovaInAppBrowser:loadstop', function (e, event) {

                if (event.url.indexOf(setoutputUrl) == 0) {
                    //  setTimeout(function(){
                    $cordovaInAppBrowser.close();
                    //$state.go(page);

                    if (page == 'twitter') {
                        $scope.fnGoTwitterPage();
                    }
                    if (page == 'linkedin') {
                        alert('through');
                        $scope.fnGoLikedInPage();
                    }
                    if (page == 'instagram') {
                        $scope.fnGoInstagramPage();
                    }
                    if (page == 'youtube') {
                        $scope.fnGoYoutubePage();
                    }
                    if (page == 'facebook') {
                        $scope.fnGoFacebookPage();
                    }
                    if (page == 'googleplus') {
                        $scope.fnGoGooglePlusPage();
                    }

                    // },500)

                }
            });
        }

    })
    .controller('facebookCtrl', function ($location, $ionicPlatform, $ionicLoading, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, mainSocialService, facebookFactory, facebookService, twitterFactory, googleFactory, instagramFactory, linkedinFactory) {
        var url = $location.path().split('/');

        $scope.projectid = url[2];

        $scope.filesToUpload;
        $scope.accName;
        $scope.pages = 'timeline';
        $scope.user;
        $scope.posts;
        $scope.data;
        $scope.showloder = false;
        $scope.showReplayLoder = false;
        $scope.flag = 0;

        $scope.goBack = function () {
            $state.go('tab.socialLogin');
        }
        $ionicLoading.show({
            content: 'loading'
        })
        $ionicLoading.hide();
        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user).then(function (result) {
            if (result.data.status == "Success") {
                //set social media status
                $scope.facebookFlag = result.data.socialMediasStatus.facebook

                //place data in factory
                facebookFactory.set(result.data.socialMediasStatus.facebook);
                twitterFactory.set(result.data.socialMediasStatus.twitter);
                instagramFactory.set(result.data.socialMediasStatus.instagram);
                linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                googleFactory.set(result.data.socialMediasStatus.google);

                $ionicLoading.hide();

            } else {
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                    template: 'Something happened wrong try again after some times',
                    buttons: [{
                        text: 'OK'
                    },]
                });
            }
            $ionicLoading.hide()
        }, function (err) {
            $ionicLoading.hide();
            $state.go('tab.socialLogin')
        });

        facebookService.pagesList().then(function (result) {
            console.log(result)
            $scope.accName = result.data.data
            var user = {
                token: localStorage.getItem('token'),
                project_id: $scope.projectid
            };
            mainSocialService.checkAuth(user).then(function () {
                $scope.getPageData(10);
                $scope.friends(); 
            }, function (error) {
                // let toast = this.toastCtrl.create({
                //     message: 'something happende wrong try again after sometime',
                //     duration: 3000,
                //     position: 'top',
                //     cssClass: 'errorToast'
                // });
                // toast.present();
                var myPopup = $ionicPopup.show({
                    template: "something happende wrong try again after sometime",
                    buttons: [{
                        text: 'OK'
                    },]
                });
            })
        }, function (error) {
            // let toast = this.toastCtrl.create({
            //     message: 'try again with login',
            //     duration: 3000,
            //     position: 'top',
            //     cssClass: 'errorToast'
            // });
            // toast.present();
            var myPopup = $ionicPopup.show({
                template: "try again with login",
                buttons: [{
                    text: 'OK'
                },]
            });
            // this.navCtrl.setRoot('HomePage')

        })

        $scope.getPageData = function (dd = 0) {
            if (dd == 0) {
                if ($scope.flag == 0) {
                    $scope.flag = 1
                } else {
                    $scope.flag = 0
                }
            }
            if ($scope.flag == 0) {
                facebookService.timeline()
                    .then(function (result) {
                        $scope.posts = result.data;
                    }, function (error) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        //   });
                        //   toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            } else {
                facebookService.getPagesData(this.accName[this.pages])
                    .then(function (result) {
                        console.log(result)
                        for (let index = 0; index < result.data.length; index++) {
                            result.data[index].showCmt = false;
                            result.data[index].placeCmt = ''
                            for (let i = 0; i < result.data[index].comments.data.length; i++) {
                                result.data[index].comments.data[i].showReplayTextBox = false;
                                result.data[index].comments.data[i].setReplay = '';
                            }
                        }
                        $scope.posts = result.data;
                    }, function (error) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        //   });
                        //   toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            }

        }
        $scope.friends = function () {
            facebookService.friends()
            .then(function (result) {
                console.log(result)
                $scope.allfriends = result.summary.total_count;
            }, function (error) {
                //   let toast = this.toastCtrl.create({
                //     message: 'something happende wrong try again after sometime',
                //     duration: 3000,
                //     position: 'top',
                //     cssClass: 'errorToast'
                //   });
                //   toast.present();
                var myPopup = $ionicPopup.show({
                    template: "something happende wrong try again after sometime",
                    buttons: [{
                        text: 'OK'
                    },]
                });
            })

        }
        $scope.fnPlaceComment = function (post, postIndex) {
            $scope.showloder = true;
            if (post.placeCmt != "") {
                facebookService.fnPostCmt(post, this.accName[this.pages].access_token)
                    .then(function (result) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'your comment successfuly placed',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'successToast'
                        //   });
                        //   toast.present();
                        $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 2000 });
                        $scope.posts[postIndex].placeCmt = '';
                        facebookService.fnGetSingleComment(result.id, $scope.accName[$scope.pages].access_token)
                            .then(function (res) {
                                res.showReplayTextBox = false;
                                res.setReplay = '';
                                $scope.posts[postIndex].placeCmt = '';
                                $scope.posts[postIndex].showCmt = false;
                                $scope.posts[postIndex].comments.data.push(res)
                                $scope.showloder = false;
                            }, function (err) { })
                    }, function (error) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        //   });
                        //   toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                        $scope.showloder = false;
                    })
            }
        }
        $scope.showlikes= 0;
        $scope.viewLike = function (post, postIndex) {
            $scope.showlikes= 1;
            $scope.listOfLikes =[];
            $scope.listOfLikes2 =[];
            console.log($scope.posts[postIndex].likes.data);
            for (var i = 0; i < $scope.posts[postIndex].likes.data.length; i++) {
                $scope.listOfLikes.push('<i class="icon ion-thumbsup"></i>');
                $scope.listOfLikes.push($scope.posts[postIndex].likes.data[i]['name']); 
                $scope.listOfLikes.push('<br>');
            }
            var str ='<i class="icon ion-thumbsup"></i>';
           for (var i = 1; i < $scope.listOfLikes.length; i++) {
             
                str+=$scope.listOfLikes[i]+" ";
              
              }
            console.log($scope.listOfLikes);
           // var check = $scope.listOfLikes;
           // $scope.listOfLikes = check.replace(",", "<br>");


           // var newchar = '<br>'
           //var mystring = check.split(',').join(newchar);

            var myPopup = $ionicPopup.show({
                template: str,
                buttons: [{
                    text: 'OK'
                },]
            });
            
        }
        $scope.fnPlaceLike = function (post, postIndex) {
            if (this.pages == 'timeline') {
                return false;
            }
            if (!post.likes.summary.has_liked) {
                let fbdata = facebookFactory.get();
                if (this.pages != 'timeline') {
                    facebookService.fnpostLike(post, this.accName[this.pages].access_token)
                        .then(function (result) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'your like successfuly placed',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'successToast'
                            // });
                            // toast.present();
                            $ionicLoading.show({ template: 'your successfuly like the post', noBackdrop: true, duration: 2000 });
                            $scope.getPageData(10);
                        }, function (error) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'something happende wrong try again after sometime',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'errorToast'
                            // });
                            // toast.present();
                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        })
                } else {
                    facebookService.fnpostLike(post, fbdata.fb_access_token)
                        .then(function (result) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'your like successfuly placed',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'successToast'
                            // });
                            // toast.present();
                            $ionicLoading.show({ template: 'Like!', noBackdrop: true, duration: 1000 });

                            this.getPageData();
                        }, function (error) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'something happende wrong try again after sometime',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'errorToast'
                            // });
                            // toast.present();
                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        })
                }
            } else {
                facebookService.fnRemoveLike(post, this.accName[this.pages].access_token).then(function (result) {
                    // let toast = this.toastCtrl.create({
                    //   message: 'Unliked this post like this post ',
                    //   duration: 3000,
                    //   position: 'top',
                    //   cssClass: 'successToast'
                    // });
                    // toast.present();
                    $ionicLoading.show({ template: 'Unliked this post like this post', noBackdrop: true, duration: 2000 });

                    if (this.posts[postIndex].likes.summary.total_count > 0) {
                        this.posts[postIndex].likes.summary.has_liked = false;
                        this.posts[postIndex].likes.summary.total_count--;
                    }
                    this.getPageData();
                })
            }
        }
        $scope.showReplayLoder = false;
        $scope.fnPlaceReplayonComment = function (comment, postIndex, commentIndex) {
            $scope.showReplayLoder = true;
            if (comment.setReplay != "") {
                if (this.pages != 'timeline') {
                    facebookService.fnPlaceReplayonComment(comment, this.accName[this.pages].access_token)
                        .then(function (result) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'your comment successfuly placed',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'successToast'
                            // });
                            // toast.present();
                            $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 2000 });

                            $scope.showReplayLoder = false;

                            this.posts[postIndex].comments.data[commentIndex].showReplayTextBox = false;
                            this.posts[postIndex].comments.data[commentIndex].setReplay = '';
                            $scope.showReplayComments(comment, postIndex, commentIndex)
                        }, function (error) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'something happende wrong try again after sometime',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'errorToast'
                            // });
                            // toast.present();
                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                            $scope.showReplayLoder = false;

                        })
                }
                else {
                    let fbdata = facebookFactory.get()
                    facebookService.fnPlaceReplayonComment(comment, fbdata.fb_access_token)
                        .then(function (result) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'your comment successfuly placed',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'successToast'
                            // });
                            // toast.present();
                            $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 1000 });

                            this.posts[postIndex].comments.data[commentIndex].showReplayTextBox = false;
                            this.posts[postIndex].comments.data[commentIndex].setReplay = '';
                            $scope.showReplayComments(comment, postIndex, commentIndex)
                            $scope.showReplayLoder = false;

                        }, function (error) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'something happende wrong try again after sometime',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'errorToast'
                            // });
                            // toast.present();
                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                            $scope.showReplayLoder = false;
                        })
                }
            }
        }
        $scope.ShowReplayComments = function (comment, postIndex, commentIndex) {
            $scope.posts[postIndex].comments.data[commentIndex].showReplayTextBox = !this.posts[postIndex].comments.data[commentIndex].showReplayTextBox
            if ($scope.flag == 1) {
                facebookService.fnGetCommentReplay(comment, this.accName[this.pages].access_token)
                    .then(function (result) {
                        $scope.posts[postIndex].comments.data[commentIndex].replayComments = result.data.data;
                    }, function (error) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        //   });
                        //   toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            } else {
                facebookService.fnGetCommentReplay(comment, this.CustomeHttpServiceProvider.facebookData.fb_access_token)
                    .then(function (result) {
                        this.posts[postIndex].comments.data[commentIndex].replayComments = result.data.data;
                    }, function (error) {
                        //   let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        //   });
                        //   toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            }
            console.log(this.posts[postIndex].comments)
            console.log(commentIndex)
            console.log(this.posts[postIndex].comments.data[commentIndex])
        }
        $scope.showpostLikes = function (data) {
            // let followersmodal = this.modalCtrl.create(FacebookListPage, { data: data })
            // followersmodal.present();
        }
        $scope.fnDisplayAllComment = function (post, postIndex) {
            // let commentModel = this.modalCtrl.create(CommentListPage, { data: post, page: this.pages, pagesData: this.accName, postIndex: postIndex })
            // commentModel.present();
            // commentModel.onDidDismiss(data {
            //     if (data) {
            //         this.posts[postIndex] = data
            //     }
            // });
        }
        $scope.fnChangeFile = function (event) {
            this.filesToUpload = event.target.files;
        }


    })
    .controller('twitterCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, twitterService, mainSocialService, facebookFactory, twitterFactory, instagramFactory, linkedinFactory, googleFactory) {
        var url = $location.path().split('/');
        $scope.projectid = url[2];
        $scope.twitterPost;
        $scope.tweets;
        $scope.showLoder = false;

        // $scope.closePopup = function () {
        //     console.log('ok')
        //     myPopup.close();
        //     $state.go('tab.socialLogin');
        // };

        $scope.goBack = function () {
            $state.go('tab.socialLogin');
        }
        $ionicLoading.show();
        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user).then(function (result) {
            console.log(result)
            if (result.data.status == "Success") {
                $scope.twitterFlag = result.data.socialMediasStatus.twitter

                facebookFactory.set(result.data.socialMediasStatus.facebook);
                twitterFactory.set(result.data.socialMediasStatus.twitter);
                instagramFactory.set(result.data.socialMediasStatus.instagram);
                linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                googleFactory.set(result.data.socialMediasStatus.google);
                $ionicLoading.hide();
                if (twitterFactory.get()) {
                    $scope.fnGetUserTweets();
                } else {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        scope: $scope,
                        buttons: [{
                            text: 'OK',
                            onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                        },]
                    });
                }


            } else {
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                    template: 'Something happened wrong try again after some times',
                    buttons: [{
                        text: 'OK'
                    },]
                });
            }
        }, function (err) {
            $ionicLoading.hide();
        });

        // $scope.closePopup = function () {
        //     $scope.myPopup.close();
        //     $state.go('tab.socialLogin');
        // };

        $scope.fnGetUserTweets = function () {
            twitterService.getTimeline().then(function (result) {
                //console.log(result)
                if (result.data.status == 'success') {
                    for (let index = 0; index < result.data.length; index++) {
                        result.data.data.tweetsData[index].showRetweet = false;
                        result.data.data.tweetsData[index].placeRetweet = '';
                    }
                    $scope.tweets = result.data.data.tweetsData;
                } else {
                    $scope.myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK',
                            onTap: function () { $scope.myPopup.close(); $state.go('tab.socialLogin') }
                        },]
                    });
                }

            }, function (error) {
                $scope.myPopup = $ionicPopup.show({
                    template: 'Something happened wrong try again after some times',
                    buttons: [{
                        text: 'OK',
                        onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                    },]
                });
            })
        }



        $scope.fnPlaceReplay = function (post, tweetIndex) {
            $scope.showLoder = true;
            console.log(post.placeRetweet)
            twitterService.fnPlaceReplay(post.id, post.placeRetweet)
                .then(function (result) {
                    console.log(result)
                    $scope.showLoder = false;
                    if (result.data.status == 'success') {
                        $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 2000 });
                        result.data.data.result.showRetweet = false;
                        result.data.data.result.placeRetweet = '';
                        $scope.tweets[tweetIndex].placeRetweet = '';
                        $scope.tweets.splice(0, 0, result.data.data.result)
                    } else {
                        var myPopup = $ionicPopup.show({
                            template: 'Something happened wrong try again after some times',
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    }
                    $scope.showLoder = false;
                }, function (error) {
                    $scope.showLoder = false;
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })

        }

        $scope.fnPlaceFavorites = function (post, tweetIndex) {
            twitterService.fnPlaceFavorites(post.id_str)
                .then(function (resul) {
                    $ionicLoading.show({ template: 'your like placed', noBackdrop: true, duration: 2000 });
                    $scope.tweets[tweetIndex].favorited = true;
                    $scope.tweets[tweetIndex].favorite_count++;
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
        }

    })
    .controller('youtubeCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, youtubeService, mainSocialService, facebookFactory, twitterFactory, instagramFactory, linkedinFactory, googleFactory) {
        var url = $location.path().split('/');
        $scope.projectid = url[2];
        $scope.Videos;
        $scope.data;
        $scope.searchKeyword;
        $scope.channelList
        $scope.selectedchannel;
        $scope.videoList = new Array();
        $scope.videoListLikes = new Array();
        $scope.showLoder = false;

        $scope.animationState = 'in';

        $scope.goBack = function () {
            $state.go('tab.socialLogin');
        }
        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user).then(function (result) {
            if (result.data.status == "Success") {

                facebookFactory.set(result.data.socialMediasStatus.facebook);
                twitterFactory.set(result.data.socialMediasStatus.twitter);
                instagramFactory.set(result.data.socialMediasStatus.instagram);
                linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                googleFactory.set(result.data.socialMediasStatus.google);
            }
            youtubeService.getChannelList().then(function (result) {
                console.log(result)
                
                $scope.channelList = result.items[0]
                $scope.selectedchannel = result.items[0].id
                $scope.getVideoByChannel();
                
            }, function (error) {
                mainSocialService.refreshGoogleToken()
                    .then(function () {
                        $scope.fnReloadPage()
                    }, function () {
                        var myPopup = $ionicPopup.show({
                            template: 'Something happened wrong try again after some times',
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            });
        }, function (error) {
            var myPopup = $ionicPopup.show({
                template: 'Something happened wrong try again after some times',
                buttons: [{
                    text: 'OK',
                    onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                },]
            });
        })

        $scope.closePopup = function () {
            $scope.myPopup.close();
            $state.go('tab.socialLogin');
        };

        $scope.fnReloadPage = function () {
            var user = {
                token: localStorage.getItem('token'),
                project_id: $scope.projectid
            };
            mainSocialService.checkAuth(user).then(function (result) {
                if (result.data.status == "Success") {

                    facebookFactory.set(result.data.socialMediasStatus.facebook);
                    twitterFactory.set(result.data.socialMediasStatus.twitter);
                    instagramFactory.set(result.data.socialMediasStatus.instagram);
                    linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                    googleFactory.set(result.data.socialMediasStatus.google);
                }
                youtubeService.getChannelList().then(function (result) {
                    this.channelList = result.items[0];
                    this.selectedchannel = result.items[0].id;
                    this.getVideoByChannel();
                }, function (error) {
                    mainSocialService.refreshGoogleToken()
                        .then(function () {
                            this.ionViewDidLoad();
                        }, function () {
                            var myPopup = $ionicPopup.show({
                                template: 'Something happened wrong try again after some times',
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        })
                });
            }, function (error) {
                var myPopup = $ionicPopup.show({
                    template: 'Something happened wrong try again after some times',
                    buttons: [{
                        text: 'OK',
                        onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                    },]
                });
            })
        }
        //$scope.fnReloadPage(); 
        $scope.toggleShowDiv = function (divName) {
            console.log(divName)
            if (divName === 'divA') {
                console.log(this.animationState);
                this.animationState = this.animationState === 'out' ? 'in' : 'out';
                console.log(this.animationState);
            }
        }
        $scope.fnYoutubeSearch = function () {
        }

        $scope.onCancel = function () {
            this.searchKeyword = null;
        }
        $scope.view_video = function (data) {
        //  $scope.frame=  '<iframe src="'+data+'" frameborder="0" width="560" height="315"></iframe>';
       // $scope.frame= '<video ng-src="'+data+'" class="centerme" controls="controls"></video>';
       // $scope.frame= '<ng-youtube-embed video="'+data+'" autoplay="true" color="white" disablekb="true" end="20"></ng-youtube-embed>';  
       //$scope.frame='<iframe width="420" height="345" src="https://www.youtube.com/embed/1YJ5xP0V9T4"></iframe>'; 
       $scope.frame=  ' <iframe width="420" height="345" ng-src="'+data+'"></iframe>';
       var myPopup = $ionicPopup.show({
            template: $scope.frame,
            buttons: [{
                text: 'Cancel'
            },]
        });  
        }
        $scope.view_details_likes = function (data) {  
        
                    youtubeService.getVideoStatics(data).then(function (res) {
                         $scope.videoListLikes1= res.items;
                         $scope.viewcount= res.items[0].statistics.viewCount;
                         $scope.likecount= res.items[0].statistics.likeCount;
                         $scope.dislike= res.items[0].statistics.dislikeCount;
                         $scope.favorite= res.items[0].statistics.favoriteCount;
                         $scope.comentcount= res.items[0].statistics.commentCount;
                        
                        // $scope.videoList[a]['ffffffffffffffff'].push() = res.items[index];
                           console.log($scope.videoListLikes);
                        $scope.html ='<p><span class="icon ion-thumbsup"></span>Likes Count = '+$scope.likecount+'</p><br><p><span class="icon ion-thumbsdown"></span>DisLikes Count = '+$scope.dislike+'</p><br><p><span class="icon ion-ios-heart"></span>Fovorite Count = '+$scope.favorite+'</p><br><p><span class="icon ion-eye"></span>Views Count = '+$scope.viewcount+'</p><br><p><span class="ion-android-textsms""></span>Comments Count = '+$scope.comentcount+'</p>';
                           var myPopup = $ionicPopup.show({
                            template: $scope.html,
                            buttons: [{
                                text: 'Ok'
                            },]
                        }); 
                      }, function (error){

                     })

        }

        $scope.getVideoByChannel = function () {   
            
            youtubeService.getVideoByChannel(this.selectedchannel).then(function (result) {
                console.log(result)
                $scope.videoList = result
                var add;
                a = 0;
              //  for (var index = 0; index < result.length; index++) {
                   
                //     youtubeService.getVideoStatics(result[a].id.videoId).then(function (res) {
                       
                    //    alert(result[a].id.videoId);
                  //       res.items[0].placeCmt = '';
                    //     $scope.videoListLikes.push(res.items);
                        // $scope.videoList[a]['ffffffffffffffff'].push() = res.items[index];

                      //   console.log($scope.videoListLikes);
                       // a++;
                      //}, function (error){

                    // })
                //}
                //alert(JSON.stringify($scope.videoListLikes ));

            }, function (error) {
                if (error.error.code) {
                    mainSocialService.refreshGoogleToken()
                        .then(function (result) {
                            this.fnReloadPage();
                        }, function (error) {
                            var myPopup = $ionicPopup.show({
                                template: 'Something happened wrong try again after some times',
                                buttons: [{
                                    text: 'OK',
                                    onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                                },]
                            });
                        })
                } else {
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK',
                            onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                        },]
                    });
                }
            })
        }

        $scope.getAllvideos = function () {
            youtubeService.getUsersAllVideo(this.selectedchannel).then(function (result) {
            }, function (error) {
                if (error.error.code) {
                    mainSocialService.refreshGoogleToken()
                        .then(function (result) {
                            this.fnReloadPage();
                        }, function (error) {
                            var myPopup = $ionicPopup.show({
                                template: 'Something happened wrong try again after some times',
                                buttons: [{
                                    text: 'OK',
                                    onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                                },]
                            });
                        })
                } else {
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK',
                            onTap: function () { myPopup.close(); $state.go('tab.socialLogin') }
                        },]
                    });
                }
            })
        }

        $scope.fnGetVideoComments = function (data, index) {
            // console.log(data)
            console.log(data)
            youtubeService.getVideoComment(data.id.videoId)
                .then(function (result) {
                    console.log(result)
                    if (result.items && result.items.length > 0) {
                        for (let i = 0; i < result.items.length; i++) {
                            result.items[i].showReplayComment = false;
                            result.items[i].replayComment = '';
                            result.items[i].replayCommentList;
                        }
                        $scope.videoList[index].comments = result.items;
                        console.log($scope.videoList[index].comments[index].showReplayComment)
                    }
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
        }

        $scope.fnPlaceComment = function (Video, videoIndex) {
            $scope.showLoder = true;
            console.log(Video)
            youtubeService.fnSetVideoComment(Video.id.videoId, $scope.channelList.id, Video.placeCmt)
                .then(function (result) {
                    $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 2000 });

                    $scope.showLoder = false;
                    $scope.videoList[$scope.videoList.indexOf(Video)].placeCmt = '';
                    // $scope.fnGetVideoComments(Video.id.videoId, videoIndex)
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                    $scope.showLoder = false;
                })
        }
        $scope.fnShowCommentReplay = function (comment, commentIndex, videoIndex) {
            // console.log($scope.videoList[videoIndex].comments.indexOf(comment))
            commentIndex = $scope.videoList[videoIndex].comments.indexOf(comment)
            // console.log(videoIndex)
            if ($scope.videoList[videoIndex].comments[commentIndex].showReplayComment) {
                $scope.videoList[videoIndex].comments[commentIndex].showReplayComment = false
            } else {
                $scope.videoList[videoIndex].comments[commentIndex].showReplayComment = true
                youtubeService.fnShowCommentReplay(comment)
                    .then(function (result) {
                        $scope.videoList[videoIndex].comments[commentIndex].replayCommentList = result.items
                    }, function (error) {
                        var myPopup = $ionicPopup.show({
                            template: 'Something happened wrong try again after some times',
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            }
        }

        $scope.fnPlaceReplayComment = function (comment, commentIndex, videoIndex) {
            $scope.showLoder = true;
            commentIndex = $scope.videoList[videoIndex].comments.indexOf(comment);
            youtubeService.fnSetCommentReplay(comment)
                .then(function (result) {
                    $scope.videoList[videoIndex].comments[commentIndex].showReplayComment = false;
                    $scope.videoList[videoIndex].comments[commentIndex].replayComment = '';
                    $scope.fnShowCommentReplay(comment, commentIndex, videoIndex)
                    $scope.showLoder = false;
                    var myPopup = $ionicPopup.show({
                        template: 'your comment successfuly placed',
                        buttons: [{
                            text: 'OK'
                        },]
                    });



                }, function (error) {

                    var myPopup = $ionicPopup.show({
                        template: 'Something happened wrong try again after some times',
                        buttons: [{
                            text: 'OK',
                        },]
                    });
                    $scope.showLoder = false;
                })
        }
        $scope.fnPlaceLike = function (VideoId) {
            youtubeService.fnSetVideoLike(VideoId).then(function (result) {
                var myPopup = $ionicPopup.show({
                    template: 'your Like successfuly placed',
                    buttons: [{
                        text: 'OK'
                    },]
                });
            }, function (error) {
                var myPopup = $ionicPopup.show({
                    template: 'something happende wrong try again after sometime',
                    buttons: [{
                        text: 'OK'
                    },]
                });
            })
        }

    })
    .controller('googlePlusCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, twitterService, mainSocialService, facebookFactory, twitterFactory, instagramFactory, linkedinFactory, googleFactory) {
        var url = $location.path().split('/');

        $scope.projectid = url[2];
        $scope.activities;
        $scope.showReplayLoder = false;
        $scope.showCommentLoder = false;
        $scope.showReplaylist = false;
        $scope.showComment = false;
        $scope.fnPageLoad = function () {
            var user = {
                token: localStorage.getItem('token'),
                project_id: $scope.projectid
            };
            mainSocialService.checkAuth(user).then(function () {
                if (result.data.status == "Success") {

                    facebookFactory.set(result.data.socialMediasStatus.facebook);
                    twitterFactory.set(result.data.socialMediasStatus.twitter);
                    instagramFactory.set(result.data.socialMediasStatus.instagram);
                    linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                    googleFactory.set(result.data.socialMediasStatus.google);
                }
                googlePlusService.fnGetUserActivity()
                    .then(function (result) {
                        for (let index = 0; index < result.items.length; index++) {
                            result.items[index].showComment = false;
                            result.items[index].placeComment = '';
                            googlePlusService.fnGetActivityComments(result.items[index].id)
                                .then(function (res) {
                                    result.items[index].comments = res.items
                                }, function (error) { })
                        }
                        this.activities = result.items
                    }, function (error) {
                        mainSocialService.refreshGoogleToken()
                            .then(function (result) {
                                this.fnPageLoad();
                            }, function (error) {
                                // let toast = this.toastCtrl.create({
                                //   message: 'something happende wrong try again after sometime',
                                //   duration: 3000,
                                //   position: 'top',
                                //   cssClass: 'errorToast'
                                // });
                                // toast.present();
                                var myPopup = $ionicPopup.show({
                                    template: 'something happende wrong try again after sometime',
                                    buttons: [{
                                        text: 'OK'
                                    },]
                                });
                            })
                    });
            }, function (error) {
                // this.navCtrl.setRoot('HomePage')
                $state.go('tab.socialLogin')
            })
        }
        this.fnPageLoad();

        $scope.goBack = function () {
            $state.go('tab.socialLogin');
        }
        $scope.fnPlaceComment = function (activity, activityIndex) {
            googlePlusService.fnSetActivityComment(activity.id, activity.placeComment)
                .then(function (result) {
                    googlePlusService.fnGetActivityComments(activity.id)
                        .then(function (res) {
                            this.activities[activityIndex].comments = res.items;
                            this.activities[activityIndex].placeComment = '';
                            // let toast = this.toastCtrl.create({
                            //   message: 'your comment successfuly placed',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'successToast'
                            // });
                            // toast.present();
                            var myPopup = $ionicPopup.show({
                                template: 'something happende wrong try again after sometime',
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        }, function (error) { })
                    this.activities[activityIndex].placeComment = '';
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'something happende wrong try again after sometime',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
        }
    })
    .controller('linkedinCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, twitterService, mainSocialService, facebookFactory, linkedinService, twitterFactory, instagramFactory, linkedinFactory, googleFactory) {

        var url = $location.path().split('/');
        $scope.projectid = url[2];
        $scope.userDatastr;
        $scope.userData;
        $scope.posts;
        $scope.accName;
        $scope.pages = '';
        $scope.data;
        $scope.showLoder = false;
        $scope.flag = 0;
        $scope.checkss ='';
        $scope.timeline = 'timeline';
        $scope.goBack = function () {
            $state.go('tab.socialLogin')
        }

        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user)
            .then(function (result) {
                if (result.data.status == "Success") {

                    facebookFactory.set(result.data.socialMediasStatus.facebook);
                    twitterFactory.set(result.data.socialMediasStatus.twitter);
                    instagramFactory.set(result.data.socialMediasStatus.instagram);
                    linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                    googleFactory.set(result.data.socialMediasStatus.google);
                }
                linkedinService.companyList().then(function (result) {
                    console.log(result)
                    $scope.accName = result.data.values;
                    console.log($scope.accName)
                    // if (this.accName.lenght > -1) {
                    //   this.accName[0]
                    // }
                    linkedinService.timeline()
                        .then(function (result) {

                            $scope.userData = result.data;
                            $scope.userDatastr = JSON.stringify(result.data);
                            $scope.connections = JSON.stringify(result.data.numConnections);
                            console.log($scope.userDatastr)
                        }, function (error) {
                            // let toast = this.toastCtrl.create({
                            //   message: 'something happende wrong try again after sometime',
                            //   duration: 3000,
                            //   position: 'top',
                            //   cssClass: 'errorToast'
                            // });
                            // toast.present();

                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        })
                }, function (error) {
                    // let toast = this.toastCtrl.create({
                    //   message: 'something happende wrong try again after sometime',
                    //   duration: 3000,
                    //   position: 'top',
                    //   cssClass: 'errorToast'
                    // });
                    // toast.present();

                    var myPopup = $ionicPopup.show({
                        template: "something happende wrong try again after sometime",
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
            }, function (error) {
                // let toast = this.toastCtrl.create({
                //   message: 'something happende wrong try again after sometime',
                //   duration: 3000,
                //   position: 'top',
                //   cssClass: 'errorToast'
                // });
                // toast.present();

                var myPopup = $ionicPopup.show({
                    template: "something happende wrong try again after sometime",
                    buttons: [{
                        text: 'OK'
                    },]
                });
            })


        $scope.getPageData = function () {
            $scope.checkss ='asad';
            var value = document.getElementById("dopdown").value;
            $scope.pages = value
            // console.log(chaking)
            // alert(chaking)
            // $scope.flag = !$scope.flag;
            console.log($scope.pages)
            if ($scope.pages == 'timeline') {
                $scope.flag = 0
            } else {
                $scope.flag = 1
            }
            if ($scope.flag == 0) {

                $scope.flag = 0;
                linkedinService.timeline()
                    .then(function (result) {
                        $scope.posts = result
                        $scope.timeline = result
                        console.log(this.posts)
                    }, function (error) {
                        // let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        // });
                        // toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            } else {
            // alert(JSON.stringify(this.accName[this.pages]));
                console.log(this.accName);
                console.log(this.pages)
                console.log(this.accName[this.pages])
                linkedinService.getPagesData(this.accName[this.pages])
                    .then(function (result) {
                        console.log(result.data)


                        for (let index = 0; index < result.data.values.length; index++) {
                            result.data.values[index].showCmt = true;
                            result.data.values[index].placeCmt = '';
                        }
                        // $scope.flag = 1;
                        $scope.posts = result.data.values;

                    }, function (error) {
                        // let toast = this.toastCtrl.create({
                        //     message: 'something happende wrong try again after sometime',
                        //     duration: 3000,
                        //     position: 'top',
                        //     cssClass: 'errorToast'
                        // });
                        // toast.present();
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            }
        }

        $scope.fnPlaceComment = function (post) {
            $scope.showLoder = true;
            console.log(post)
            linkedinService.fnPlaceComment(post).then(function (result) {
                $ionicLoading.show({ template: 'your comment successfuly placed', noBackdrop: true, duration: 2000 });
                var timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
                $scope.posts[$scope.posts.indexOf(post)].placeCmt = '';

                if ($scope.posts[$scope.posts.indexOf(post)].updateComments) {

                    if ($scope.posts[$scope.posts.indexOf(post)].updateComments.values) {
                        var length = $scope.posts[$scope.posts.indexOf(post)].updateComments.values
                        console.log(length)
                        console.log($scope.posts[$scope.posts.indexOf(post)].updateComments.values)
                        if (length) {
                            if (length.length > 0) {
                                length[length.length] = {
                                    comment: post.placeCmt, timestamp: Date.now(), company: { name: length[0].company.name }
                                }
                                $scope.showLoder = false;
                                $scope.getPageData();

                            } else {
                                $scope.showLoder = false;

                                $scope.getPageData();
                            }
                        } else {
                            $scope.showLoder = false;

                            $scope.getPageData();
                        }
                    }else{
                        $scope.showLoder = false;

                        $scope.getPageData();
                    }

                } else {
                    $scope.showLoder = false;
                    $scope.getPageData();


                }


            }, function (error) {
                var myPopup = $ionicPopup.show({
                    template: "something happende wrong try again after sometime",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                $scope.showLoder = false;
            })
        }
        $scope.likes= function(post){
            return false;
            linkedinService.fnPlaceLikes(post).then(function (result) {
                alert('return');
                 $ionicLoading.show({ template: 'your comment successfuly Like', noBackdrop: true, duration: 2000 });
                 
                 //var timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
                //$scope.posts[$scope.posts.indexOf(post)].placeCmt = '';

            }, function (error) {
                var myPopup = $ionicPopup.show({
                    template: "something happende wrong try again after sometime",
                    buttons: [{
                        text: 'OK'
                    },]
                });
                
            })
        }
        $scope.fnPlaceLikes = function (post) {
            console.log(post)
           


        }
    })
    .controller('instagramCtrl', function ($location, $ionicPlatform, $scope, $rootScope, $cordovaInAppBrowser, $state, $ionicLoading, $http, $ionicPopup, instagramService, mainSocialService, facebookFactory, linkedinService, twitterFactory, instagramFactory, linkedinFactory, googleFactory) {
        var url = $location.path().split('/');

        $scope.projectid = url[2];
        //var urlParams = $location.search();
        //$scope.projectid = urlParams.project_id;
        $scope.InstagramPost;
        $scope.userData;
        $scope.data;
        $scope.showLoder = false;
        $scope.today = Date.now();
        $scope.animationState = 'in';

        $scope.goBack = function () {
            $state.go('tab.socialLogin')
        }
        var user = {
            token: localStorage.getItem('token'),
            project_id: $scope.projectid
        };
        mainSocialService.checkAuth(user).then(function (result) {
            console.log(user)
            console.log(result)
            if (result.data.status == "Success") {
                console.log(result.data.socialMediasStatus.instagram)
                facebookFactory.set(result.data.socialMediasStatus.facebook);
                twitterFactory.set(result.data.socialMediasStatus.twitter);
                instagramFactory.set(result.data.socialMediasStatus.instagram);
                linkedinFactory.set(result.data.socialMediasStatus.linkedIn);
                googleFactory.set(result.data.socialMediasStatus.google);
                instagramService.fnGetUserInfo().then(function (result) {
                    $scope.userData = result.data;
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'something happende wrong try again after sometime',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
            } else {
                $state.go('sociallogin')
            }
            instagramService.timeline()
                .then(function (result) {

                    console.log(result)
                    for (let index = 0; index < result.data.data.length; index++) {
                        result.data.data[index].showComment = 'in'
                        result.data.data[index].placeCmt = '';
                    }
                    console.log(result.data.data)
                    $scope.InstagramPost = result.data.data
                    console.log($scope.InstagramPost)
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'something happende wrong try again after sometime',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
        }, function (error) {
            var myPopup = $ionicPopup.show({
                template: 'something happende wrong try again after sometime',
                buttons: [{
                    text: 'OK'
                },]
            });
        })


        $scope.fnGetCommentList = function (data) {
            alert(JSON.stringify(data));
            instagramService.fnGetMediaComment(data.id)
                .then(function (result) {
                    alert(JSON.stringify(result));
                    $scope.ccc =JSON.stringify(result);
                    console.log(result)
                    $scope.InstagramPost[$scope.InstagramPost.indexOf(data)].comment = result.data.data;
                    console.log($scope.InstagramPost)
                }, function (error) {
                    var myPopup = $ionicPopup.show({
                        template: 'something happende wrong try again after sometime',
                        buttons: [{
                            text: 'OK'
                        },]
                    });
                })
        }
        $scope.fnPlaceComment = function (post) {
            $scope.showLoder = true;
            if (post.placeCmt != "") {
                instagramService.fnSetMediaComment(post.id, post.placeCmt).then(function (result) {

                    if (result.data && result.data.data && result.data.data.meta != undefined && result.data.meta.code == 200) {
                        var myPopup = $ionicPopup.show({
                            template: 'comment placed succesfully',
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                        //loading controller code for show msg
                        $scope.fnGetCommentList(post);
                    } else {
                        var myPopup = $ionicPopup.show({
                            template: 'something happende wrong try again after sometime',
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                        $scope.showLoder = false;
                    }
                    $scope.showLoder = false;
                }, function (error) {
                    $scope.showLoder = false;
                    var myPopup = $ionicPopup.show({
                        template: 'something happende wrong try again after sometime',
                        buttons: [{
                            text: 'OK'
                        },]
                    });

                })
            } else {
                var myPopup = $ionicPopup.show({
                    template: 'something happende wrong try again after sometime',
                    buttons: [{
                        text: 'OK'
                    },]
                });
                $scope.showLoder = false;
            }
        }

        $scope.fnSetMediaLike = function (post, postId) {
            if (!post.user_has_liked) {
                instagramService.fnSetMediaLike(post.id)
                    .then(function (result) {

                        if (result.data.meta && result.data.meta.code == 200) {

                            var myPopup = $ionicPopup.show({
                                template: 'your like successfuly placed',
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                            console.log($scope.InstagramPost[postId])
                            $scope.InstagramPost[postId].user_has_liked = true
                            $scope.InstagramPost[postId].count++;

                        } else {
                            var myPopup = $ionicPopup.show({
                                template: 'something happende wrong try again after sometime',
                                buttons: [{
                                    text: 'OK'
                                },]
                            });;
                        }
                    }, function () {

                        var myPopup = $ionicPopup.show({
                            template: 'something happende wrong try again after sometime',
                            buttons: [{
                                text: 'OK'
                            },]
                        });;
                    })
            } else if (post.user_has_liked) {
                instagramService.fnRemoveMediaLike(post.id)
                    .then(function (result) {
                        if (result && result.data && result.data.data && result.data.data.meta.code == 200) {
                            var myPopup = $ionicPopup.show({
                                template: 'your like successfuly placed',
                                buttons: [{
                                    text: 'OK'
                                },]
                            });;
                            $scope.InstagramPost[postId].user_has_liked = false;
                            $scope.InstagramPost[postId].count++;
                        } else {
                            var myPopup = $ionicPopup.show({
                                template: "something happende wrong try again after sometime",
                                buttons: [{
                                    text: 'OK'
                                },]
                            });
                        }
                    }, function () {
                        var myPopup = $ionicPopup.show({
                            template: "something happende wrong try again after sometime",
                            buttons: [{
                                text: 'OK'
                            },]
                        });
                    })
            }
        }
        $scope.fnShowFollowers = function (pagename, data) {
            let followersmodal = this.modalCtrl.create(InstagramlistPage, { pagename: pagename, data: data })
            followersmodal.present();
        }

    })
function updatelink($scope, id1, id2) {
    if (id1 == 'pending_tab') {
        $scope.completed = '';
        $scope.pending = 'active';
        $scope.rejected = '';
    } if (id1 == 'completed_tab'){
        $scope.completed = 'active';
        $scope.pending = '';
        $scope.rejected = '';
    }
    if (id1 == 'rejected_tab'){
        $scope.rejected = 'active';
        $scope.pending = '';
        $scope.completed = '';
    }
}

function checkExpiry($scope, user, $state) {
    if (user.data.data.userlogin == true) {
        $state.go('login');
    }
}