// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
//https://bleedingbulbapp.com/mobileapi/
//https://bleedingbulbapp.com/
var baseUrl = 'https://www.bleedingbulbapp.com/webservices/';
var picturesUrl = 'https://www.bleedingbulbapp.com/';
var urlArray    = [];
// var console = {};
// console.log = function(){};

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','starter.factory', 'ngCordova', 'ionic-modal-searchable-select', 'ionic-datepicker', 'chart.js'])

    .run(function ($ionicPlatform, $cordovaPush, $rootScope, $state) {
        $ionicPlatform.ready(function () {
            $rootScope.$state = $state;

            // $rootScope.$on('$viewContentLoaded',function(){
            //     console.log("View loaded..");
            // });
            // $rootScope.$on('$viewContentLoading',function(){
            //     console.log("View loading..");
            // });
            // $rootScope.$on('$stateChangeStart',function(){
            //     console.log("State change started....");
            // });
            // $rootScope.$on('$viewContentLoaded',function(){
            //     console.log("View loaded..");
            // });

            $rootScope.$on('$stateChangeSuccess', function () {
                setTimeout(function () {
                    console.log('test');
                    $('select').uniform();
                    //$('select').SumoSelect({okCancelInMulti:true, selectAll:true });                
                }, 200);

            });
        });
    })


    .directive('phoneInput', function ($filter, $browser) {
        return {
            require: 'ngModel',
            link: function ($scope, $element, $attrs, ngModelCtrl) {
                var listener = function () {
                    var value = $element.val().replace(/[^0-9]/g, '');
                    $element.val($filter('tel')(value, false));
                };

                // This runs when we update the text field
                ngModelCtrl.$parsers.push(function (viewValue) {
                    return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
                });

                // This runs when the model gets updated on the scope directly and keeps our view in sync
                ngModelCtrl.$render = function () {
                    $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
                };

                $element.bind('change', listener);
                $element.bind('keydown', function (event) {
                    var key = event.keyCode;
                    // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                    // This lets us support copy and paste too
                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
                        return;
                    }
                    $browser.defer(listener); // Have to do this or changes don't get picked up properly
                });

                $element.bind('paste cut', function () {
                    $browser.defer(listener);
                });
            }

        };
    })
 .directive("allowNumbersOnly", function() {
      return {
        restrict: "A",
        link: function(scope, element, attrs) {
          element.bind("keydown", function(event) {
            if (event.keyCode == 8) {
              return false;
            } else if (!(event.keyCode > 47 && event.keyCode < 58) || event.shiftKey) {
              event.preventDefault();
              return false;
            }
          });



        }
      }
    })
    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    })
    .directive('drawing', function ($filter, $browser) {
        return {
            restrict: "A",
            link: function (scope, element) {
                var ctx = element[0].getContext('2d');

                // variable that decides if something should be drawn on mousemove
                var drawing = false;

                // the last coordinates before the current move
                var lastX;
                var lastY;

                element.bind('mousedown', function (event) {
                  
                    if (event.offsetX !== undefined) {
                        lastX = event.offsetX;
                        lastY = event.offsetY;
                    } else { // Firefox compatibility
                        lastX = event.layerX - event.currentTarget.offsetLeft;
                        lastY = event.layerY - event.currentTarget.offsetTop;
                    }

                    // begins new line
                    ctx.beginPath();
                    drawing = true;

                });
                element.bind('mousemove', function (event) {
                  //  alert('mousemove');
                  drawing = true;
                 //   if (drawing) {
                        // get current mouse position
                        if (event.offsetX !== undefined) {
                           
                            currentX = event.offsetX;
                            currentY = event.offsetY;
                        } else {
                            
                            currentX = event.layerX - event.currentTarget.offsetLeft;
                            currentY = event.layerY - event.currentTarget.offsetTop;
                        }

                        draw(lastX, lastY, currentX, currentY);

                        // set current coordinates to last one
                        lastX = currentX;
                        lastY = currentY;
                //    }

                });
                element.bind('mouseup', function (event) {
                  
                    // stop drawing
                    drawing = false;
                });

                // canvas reset
                function reset() {
                    element[0].width = element[0].width;
                }

                function draw(lX, lY, cX, cY) {

                    // line from
                    ctx.moveTo(lX, lY);
                    // to
                    ctx.lineTo(cX, cY);
                    // color
                    ctx.strokeStyle = "#4bf";
                    // draw it
                    ctx.stroke();
                }
            }
        };
    })

    .filter('tel', function () {
        return function (tel) {
            console.log(tel);
            if (!tel) { return ''; }

            var value = tel.toString().trim().replace(/^\+/, '');

            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var country, city, number;

            switch (value.length) {
                case 1:
                case 2:
                case 3:
                    city = value;
                    break;

                default:
                    city = value.slice(0, 3);
                    number = value.slice(3);
            }

            if (number) {
                if (number.length > 3) {
                    number = number.slice(0, 3) + '-' + number.slice(3, 7);
                }
                else {
                    number = number;
                }

                return ("(" + city + ") " + number).trim();
            }
            else {
                return "(" + city;
            }

        };
    })
.filter('customSplitString', function() {
  return function(input) {
    var arr = input.split(',');
    return arr;
  };
 })
    .config(function (ionicDatePickerProvider, $stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        var datePickerObj = {
            inputDate: new Date(),
            titleLabel: 'Select a Date',
            setLabel: 'Set',
            todayLabel: 'Today',
            closeLabel: 'Close',
            mondayFirst: false,
            weeksList: ["S", "M", "T", "W", "T", "F", "S"],
            monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
            templateType: 'popup',
            from: new Date(2012, 8, 1),
            to: new Date(2018, 8, 1),
            showTodayButton: true,
            dateFormat: 'dd MMMM yyyy',
            closeOnSelect: false,
            disableWeekdays: []
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);

        $stateProvider
            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html',
                controller: 'fortype'
            })
            .state('tab.dash', {
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('tab.activity', {
                url: '/activity',
                views: {
                    'tab-activity': {
                        templateUrl: 'templates/tab-activity.html',
                        controller: 'ActivityCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('showgraph', {
                url: '/showgraph',
                templateUrl: 'templates/showgraph.html',
                controller: 'showgraphCtrl',
                reload: true,
                cache: false
            })
            .state('showgoogle', {
                url: '/showgoogle',
                templateUrl: 'templates/showgoogle.html',
                controller: 'showgoogle',
                reload: true,
                cache: false
            })
            .state('webPerformance', {
                url: '/webPerformance',
                templateUrl: 'templates/webPerformance.html',
                controller: 'webPerformance',
                reload: true,
                cache: false
            })
            .state('heatmap', {
                url: '/heatmap',
                templateUrl: 'templates/heatmapview.html',
                controller: 'heatmap',
                reload: true,
                cache: false
            })
            .state('history', {
                url: '/history',
                templateUrl: 'templates/historyView.html',
                controller: 'historOfDocument',
                reload: true,
                cache: false
            })
            .state('forpage/chart', {
                url: '/forpage/chart',
                templateUrl: 'templates/showgraph.html',
                controller: 'forpage',
                reload: true,
                cache: false
            })
            .state('forpage', {
                url: '/forpage/:projectid',
                templateUrl: 'templates/pageDetail.html',
                controller: 'forpage',
                reload: true,
                cache: false
            })
            .state('tab.projects', {
                url: '/projects',
                views: {
                    'tab-projects': {
                        templateUrl: 'templates/tab-projects.html',
                        controller: 'ProjectsCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('projectDetail', {
                url: '/projectDetail/:projectid',
                templateUrl: 'templates/projectDetail.html',
                controller: 'ProjectsDetailCtrl',
                reload: true,
                cache: false
            })
            .state('tab.tasks', {
                url: '/tasks',
                views: {
                    'tab-tasks': {
                        templateUrl: 'templates/tab-tasks.html',
                        controller: 'TasksCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('taskDetail', {
                url: '/taskDetail/:taskid',
                templateUrl: 'templates/taskDetail.html',
                controller: 'TasksDetailCtrl',
                reload: true,
                cache: false
            })
            .state('addTask', {
                url: '/addTask/:projectid',
                templateUrl: 'templates/addTask.html',
                controller: 'AddTaskCtrl',
                reload: true,
                cache: false
            })
            /* .state('addInvoices', {
                 url: '/addInvoices/',
                 templateUrl: 'templates/addInvoices.html',
                 controller: 'InvoiceDetailCtrl',
                 reload: true,
                 cache: false
             })
            */
            .state('editTask', {
                url: '/editTask/:id',
                templateUrl: 'templates/editTask.html',
                controller: 'EditTaskCtrl',
                reload: true,
                cache: false
            })
            .state('tab.documents', {
                url: '/documents',
                views: {
                    'tab-documents': {
                        templateUrl: 'templates/tab-documents.html',
                        controller: 'DocumentCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('tab.quotes', {
                url: '/quotes',
                views: {
                    'tab-quotes': {
                        templateUrl: 'templates/tab-quotes.html',
                        controller: 'quotes'
                    }
                },
                reload: true,
                cache: false
            })
            .state('quotesDetail', {
                url: '/quotesDetail',
                templateUrl: 'templates/quotesDetail.html',
                controller: 'quotesDetail',
                reload: true,
                cache: false
            })
             .state('ndaDetails', {
                url: '/ndaDetails',
                templateUrl: 'templates/nda_view.html',
                controller: 'ndaDetails',
                reload: true,
                cache: false
            })
              .state('saleAgreementDetail', {
                url: '/saleAgreementDetail',
                templateUrl: 'templates/sale_view.html',
                controller: 'saleAgreementDetail',
                reload: true,
                cache: false
            })
            .state('documentDetail', {
                url: '/documentDetail/:documentid',
                templateUrl: 'templates/documentDetail.html',
                controller: 'DocumentDetailCtrl',
                reload: true,
                cache: false
            })
            .state('DocumentSighn', {
                url: '/DocumentSighn',
                templateUrl: 'templates/DocumentSighn.html',
                controller: 'DocumentSighn',
                reload: true,
                cache: false
            })
            .state('projectInvoices', {
                url: '/projectInvoices/:projectid',
                templateUrl: 'templates/projectInvoices.html',
                controller: 'InvoiceCtrl',
                reload: true,
                cache: false
            })

            .state('projectTasks', {
                url: '/projectTasks/:projectid',
                templateUrl: 'templates/projectTasks.html',
                controller: 'TasksCtrl',
                reload: true,
                cache: false
            })
            .state('projectDocuments', {
                url: '/projectDocuments/:projectid',
                templateUrl: 'templates/projectDocuments.html',
                controller: 'DocumentCtrl',
                reload: true,
                cache: false
            })
            .state('tab.invoices', {
                url: '/invoices',
                views: {
                    'tab-invoices': {
                        templateUrl: 'templates/tab-invoices.html',
                        controller: 'InvoiceCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('invoiceDetail', {
                url: '/invoiceDetail/:invoiceid',
                templateUrl: 'templates/invoiceDetail.html',
                controller: 'InvoiceDetailCtrl',
                reload: true,
                cache: false
            })
            .state('payInvoice', {
                url: '/payInvoice/:invoiceid',
                templateUrl: 'templates/payInvoice.html',
                controller: 'InvoicePayCtrl',
                reload: true,
                cache: false
            })
            .state('projectMessages', {
                url: '/projectMessages/:projectid',
                templateUrl: 'templates/messages.html',
                controller: 'MessagesCtrl',
                reload: true,
                cache: false
            })
            .state('messages', {
                url: '/messages',
                templateUrl: 'templates/messages.html',
                controller: 'MessagesCtrl',
                reload: true,
                cache: false
            })
            .state('messageDetail', {
                url: '/messageDetail/:messageid',
                templateUrl: 'templates/messageDetail.html',
                controller: 'MessageDetailCtrl',
                reload: true,
                cache: false
            })
            .state('addMessage', {
                url: '/addMessage/:projectid',
                templateUrl: 'templates/addMessage.html',
                controller: 'AddMessageCtrl',
                reload: true,
                cache: false
            })
            .state('messageReply', {
                url: '/messageReply/:messageid/:messageSubject',
                templateUrl: 'templates/messageReply.html',
                controller: 'MessageReplyCtrl',
                reload: true,
                cache: false
            })
            .state('tab.chats', {
                url: '/chats',
                views: {
                    'tab-chats': {
                        templateUrl: 'templates/tab-chats.html',
                        controller: 'ChatsCtrl'
                    }
                },
                reload: true,
                cache: false
            })
            .state('tab.chat-detail', {
                url: '/chats/:chatId',
                views: {
                    'tab-chats': {
                        templateUrl: 'templates/chat-detail.html',
                        controller: 'ChatDetailCtrl'
                    }
                },
                reload: true,
                cache: false
            })

            .state('tab.account', {
                url: '/account',
                views: {
                    'tab-account': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                },
                reload: true,
                cache: false
            })

            .state('signup', {
                url: '/signup',
                templateUrl: 'templates/signup.html',
                controller: 'SignUpCtrl',
                reload: true,
                cache: false
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl',
                reload: true,
                cache: false
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutCtrl',
                reload: true,
                cache: false
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl',
                reload: true,
                cache: false
            })
            .state('socialLogin', {
                url: '/sociallogin/:projectid',
                templateUrl: 'templates/sociallogin.html',
                controller: 'SocialLoginCtrl',
                reload: true,
                cache: false
               /* views: {
                    'tab-socialLogin': {
                        templateUrl: 'templates/sociallogin.html',
                        controller: 'SocialLoginCtrl'
                    }
                },*/
               // reload: true,
                //cache: false
            })
            .state('facebook', {
                url: '/facebook/:projectid',
                templateUrl: 'templates/facebook.html',
                controller: 'facebookCtrl',
                reload: true,
                cache: false
            })
            .state('twitter', {
                url: '/twitter/:projectid',
                templateUrl: 'templates/twitter.html',
                controller: 'twitterCtrl',
                reload: true,
                cache: false
            })
            .state('youtube', {
                url: '/youtube/:projectid',
                templateUrl: 'templates/youtube.html',
                controller: 'youtubeCtrl',
                reload: true,
                cache: false
            })
            .state('googleplus', {
                url: '/googleplus/:projectid',
                templateUrl: 'templates/googleplus.html',
                controller: 'googlePlusCtrl',
                reload: true,
                cache: false
            })
            .state('linkedin', {
                url: '/linkedin/:projectid',
                templateUrl: 'templates/linkedin.html',
                controller: 'linkedinCtrl',
                reload: true,
                cache: false
            })
            .state('instagram', {
                url: '/instagram/:projectid',
                templateUrl: 'templates/instagram.html',
                controller: 'instagramCtrl',
                reload: true,
                cache: false
            })
            
        // if none of the above states are matched, use this as the fallback
        if (localStorage.getItem('token') == null) {
            $urlRouterProvider.otherwise('/login');
        } else {
            $urlRouterProvider.otherwise('tab/activity');
        }
        // if none of the above states are matched, use this as the fallback
        //$urlRouterProvider.otherwise('/tab/dash');
    });
