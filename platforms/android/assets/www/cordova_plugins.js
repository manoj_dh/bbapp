cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-fcm.FCMPlugin",
    "file": "plugins/cordova-plugin-fcm/www/FCMPlugin.js",
    "pluginId": "cordova-plugin-fcm",
    "clobbers": [
      "FCMPlugin"
    ]
  },
  {
    "id": "cordova-plugin-inappbrowser.inappbrowser",
    "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
    "pluginId": "cordova-plugin-inappbrowser",
    "clobbers": [
      "cordova.InAppBrowser.open",
      "window.open"
    ]
  },
  {
    "id": "im.ltdev.cordova.UserAgent.UserAgent",
    "file": "plugins/im.ltdev.cordova.UserAgent/www/UserAgent.js",
    "pluginId": "im.ltdev.cordova.UserAgent",
    "clobbers": [
      "UserAgent"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-fcm": "2.1.2",
  "cordova-plugin-inappbrowser": "2.0.2",
  "cordova-plugin-whitelist": "1.3.3",
  "im.ltdev.cordova.UserAgent": "0.1.0"
};
// BOTTOM OF METADATA
});