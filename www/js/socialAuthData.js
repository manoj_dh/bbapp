angular.module('starter.factory', [])
        .factory('facebookFactory', function () {
                facebookData = false;
                return {
                        get: function () {
                                if (facebookData) {
                                        return facebookData;
                                } else {
                                        return false;
                                }
                        },
                        set: function (data) {
                                facebookData = data;
                                return facebookData;
                        }
                };
        })

        .factory('twitterFactory', function () {
                twitterData = false;
                return {
                        get: function () {
                                if (twitterData) {
                                        return twitterData;
                                } else {
                                        return false;
                                }
                        },
                        set: function (data) {
                                twitterData = data;
                                return twitterData;
                        }
                };
        })

        .factory('googleFactory', function () {
                googleData = false;
                return {
                        get: function () {
                                if (googleData) {
                                        return googleData;
                                } else {
                                        return false;
                                }
                        },
                        set: function (data) {
                                googleData = data;
                                return googleData;
                        }
                }
        })

        .factory('instagramFactory', function () {
                instagramData = false;
                return {
                        get: function () {
                                if (instagramData) {
                                        return instagramData;
                                } else {
                                        return false;
                                }
                        },
                        set: function (data) {
                                instagramData = data;
                                return instagramData;
                        }
                }
        })

        .factory('linkedinFactory', function () {
                likedinData = false;
                return {
                        get: function () {
                                if (likedinData) {
                                        return likedinData;
                                } else {
                                        return false;
                                }
                        },
                        set: function (data) {
                                likedinData = data;
                                return likedinData
                        }
                }
        })