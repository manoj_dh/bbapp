angular.module('starter.services', [])

    .service('AuthService', function ($rootScope, $http, $q, $ionicLoading) {
        this.doLogin = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/login',
                headers: {
                    'Content-Type': undefined
                },
                data: { email: user.email, password: user.password }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.doLogout = function (token) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/logout',
                headers: {
                    'Content-Type': undefined
                },
                data: { token: token }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });

        };



        this.doRegister = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/register',
                headers: {
                    'Content-Type': undefined
                },
                data: { firstname: user.firstname, lastname: user.lastname, phone: user.phone, email: user.email, password: user.password, type: user.type }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });

        };
    })
    .service('mainSocialService', function ($rootScope, $http, $q, $ionicLoading) {        
        this.checkAuth = function (user) {
           
            req = {
                method: 'get',
                url: baseUrl + 'social_oauth/social_medias_status?user_id=' + localStorage.getItem('user_id')+'&p_id='+user.project_id,
                headers: {
                    'Content-Type': undefined
                },

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };


        this.refreshGoogleToken = function () {
            req = {
                method: 'get',
                url: baseUrl + 'social_oauth/refresh_google_token?user_id=' + localStorage.getItem('user_id'),
                headers: {
                    'Content-Type': undefined
                },
            }

            $http(req).then(function (result) {
                googleFactory.set(result.data.oAuthData)
            }, function (error) {
                return false;
            })
            // return new Promise((resolve, reject) => {
            //     let url = this.baseURl + 'refresh_google_token?user_id=1'
            //     this.http.get(url)
            //         .subscribe((result: any) => {
            //             this.googleData = result.data.oAuthData;
            //             resolve(result.data.oAuthData)
            //         }, (error: any) => {
            //             reject();
            //         })
            // })
        }

    })
    .service('facebookService', function ($rootScope, $http, $q, $ionicLoading, facebookFactory) {
        this.pagesList = function () {
            req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/me/accounts?access_token=" + facebookFactory.get().fb_access_token,
                headers: {
                    'Content-Type': undefined
                },
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.friends = function () {
            var fbdata = facebookFactory.get();
            // let url = "https://graph.facebook.com/v2.11/me/posts?fields=story,from,description,message,story_tags,likes.summary(true),comments.summary(true)&access_token=" + fbdata.fb_access_token;
            req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/me/friends?&access_token=" + fbdata.fb_access_token,

            }
            return $http(req).then(function (result) {
                return result.data;

            }, function (error) {
                return error;
            })
        }








        this.timeline = function () {
            var fbdata = facebookFactory.get();
            // let url = "https://graph.facebook.com/v2.11/me/posts?fields=story,from,description,message,story_tags,likes.summary(true),comments.summary(true)&access_token=" + fbdata.fb_access_token;
            req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/me/posts?fields=story,from,description,message,story_tags,likes.summary(true),comments.summary(true)&access_token=" + fbdata.fb_access_token,

            }
            return $http(req).then(function (result) {
                //console.log(result.data)
                //result = result.data;
                for (let index = 0; index < result.data.length; index++) {
                    result.data[index].showCmt = false;
                    result.data[index].placeCmt = '';
                    /*
                    this.fnGetPostAttachment(result.data[index].id, fbdata.fb_access_token).then(function (res) {
                        result.data[index].data = res.data
                    }, function (error) { })
                    */
                }
                return result.data;

            }, function (error) {
                return error;
            })
            // return $http.get(url).subscribe((result: any) => {
            //     this.getUSerFriend()
            //     for (let index = 0; index < result.data.length; index++) {
            //         result.data[index].showCmt = false
            //         result.data[index].placeCmt = '';

            //         this.fnGetPostAttachment(result.data[index].id, fbdata.fb_access_token)
            //             .then((res: any) => {
            //                 result.data[index].data = res.data
            //             }, (error: any) => {
            //             })
            //         if (index == (result.data.length - 1)) {
            //             resolve(result)
            //         }
            //     }
            // }, (error: any) => {
            //     reject(error);
            // })
        }
        this.fnGetPostAttachment = function (postId, token) {
            let fbdata = facebookFactory.get();
            let url = "https://graph.facebook.com/v2.11/" + postId + "/attachments?access_token=" + token;
            req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/" + postId + "/attachments?access_token=" + token
            }
            return $http(req).then(function (result) {
                console.log(result)
                return (result)
            }, function (error) {
                return (error)
            })
        }
        this.getPagesData = function (data) {
            req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/me/posts?fields=story,attachments,from,description,message,story_tags,likes.summary(true),comments.summary(true)&access_token=" + data.access_token
            }
            let fbdata = facebookFactory.get();
            return $http(req).then(function (result) {
                console.log(result.data)
                for (let index = 0; index < result.data.length; index++) {
                    result.data.data[index].showCmt = false;
                    result.data.data[index].placeCmt = '';
                }
                return result.data;
            }, function (error) {
                return error;
            })

        }
        this.fnPostCmt = function (post, token) {
            let fbdata = facebookFactory.get();
            var da = {
                message: post.placeCmt
            }
            var req = {
                method: 'POST',
                url: "https://graph.facebook.com/v2.11/" + post.id + "/comments?access_token=" + token,
                data: da
            }
            return $http(req).then(function (result) {
                return result
            }, function (error) {
                return error
            })
        }
        this.fnpostLike = function (post, token) {

            let fbdata = facebookFactory.get();
            var da = {
                message: post.placeCmt
            }
            var req = {
                method: 'POST',
                url: "https://graph.facebook.com/v2.11/" + post.id + "/likes?access_token=" + token,
                data: da
            }
            return $http(req).then(function (result) {
                return (result)
            }, function (error) {
                return (error)
            })
            // return new Promise((resolve, reject) => {
            //     this.http.post(url, { message: post.placeCmt }).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnRemoveLike = function (post, token) {
            let fbdata = facebookFactory.get();
            let req = {
                method: 'POST',
                url: "https://graph.facebook.com/v2.11/" + post.id + "/likes?access_token=" + token
            }
            return $http(req).then(function (result) {
                return result;
            }, function (error) {
                return error;
            })
            // return new Promise((resolve, reject) => {
            //     this.http.delete(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnPlaceReplayonComment = function (comment, token) {
            let req = {
                method: 'POST',
                url: "https://graph.facebook.com/v2.11/" + comment.id + "/comments/?access_token=" + token,
                data: { message: comment.setReplay }
            }
            return $http(req).then(function (result) {
                return result
            }, function (error) {
                return error;
            })
            // return new Promise((resolve, reject) => {
            //     this.http.post(url, { "message": comment.setReplay }).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnGetCommentReplay = function (comment, token) {
            let req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/" + comment.id + "/comments/?access_token=" + token
            }
            return $http(req)
                .then(function (result) {
                    return result
                }, function (error) {
                    return error
                })
            // return new Promise<any>((resolve, reject) => {
            //     this.http.get(url)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error: any) => {
            //             reject(error)
            //         })
            // })
        }
        this.fnGetSingleComment = function (id, token) {
            let req = {
                method: 'get',
                url: "https://graph.facebook.com/v2.11/" + id + "/?access_token=" + token
            }
            return $http(req).then(function (result) {
                return result;
            }, function (error) { return error; })

        }

    })
    .service('twitterService', function ($rootScope, $http, $q, $ionicLoading, twitterFactory) {
        twitterData = twitterFactory.get();
        this.getTimeline = function () {
            var da = {
                twitter_oauth_token: twitterFactory.get().twitter_oauth_token,
                twitter_oauth_token_secret: twitterFactory.get().twitter_oauth_token_secret
            }
            var form = new FormData();
            form.append("twitter_oauth_token", twitterFactory.get().twitter_oauth_token);
            form.append("twitter_oauth_token_secret", twitterFactory.get().twitter_oauth_token_secret);

            var settings = {
                url: "https://www.bleedingbulbapp.com/webservices/twitter/get_tweets",
                method: "POST",
                data: {
                    twitter_oauth_token: twitterFactory.get().twitter_oauth_token,
                    twitter_oauth_token_secret: twitterFactory.get().twitter_oauth_token_secret
                },
            }

            req = {
                method: 'POST',
                url: baseUrl + 'twitter/get_tweets',
                data: da,
            }
            return $http(settings).then(function (result) {
                return result;
            }, function (error) {
                return error;
            })
            // $http.post("https://www.bleedingbulbapp.com/webservices/twitter/get_tweets", da).success(function (result) {
            //     console.log(result)
            // }).error(function () {
            //     console.log(error)
            // })

            // return $http(settings).then(function (data) {
            //     return data;
            // }, function (data) {
            //     return data;
            // })
        }
        this.getUserTweets = function () {
            let twitterdata = twitterFactory.get()
            console.log(twitterdata)
            let form = new FormData();
            form.append('twitter_oauth_token', twitterdata.twitter_oauth_token)
            form.append('twitter_oauth_token_secret', twitterdata.twitter_oauth_token_secret)
            let req = {
                method: 'POST',
                url: baseUrl + 'twitter/get_user_tweets',
                data: form
            }
            return $http(req).then(function (result) {
                return (result)
            }, function (error) {
                return error;
            })
            // return new Promise((resolve, reject) => {
            //     this.http.post(this.baseURL + 'get_user_tweets', form)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }
        this.fnPlaceReplay = function (id, status) {
            let twitterdata = twitterFactory.get()
            let form = new FormData();
            form.append('twitter_oauth_token', twitterdata.twitter_oauth_token)
            form.append('twitter_oauth_token_secret', twitterdata.twitter_oauth_token_secret)
            form.append('tweet_id', id)
            form.append('status', status)
            console.log(status)
            var data = {
                'twitter_oauth_token': twitterdata.twitter_oauth_token,
                'twitter_oauth_token_secret': twitterdata.twitter_oauth_token_secret,
                'tweet_id': id,
                'status': status
            }
            let req = {
                method: 'POST',
                url: baseUrl + 'twitter/reply_tweet',
                data: data
            }
            return $http(req).then(function (result) { return result; }, function (error) { return error; })
            // return new Promise((resolve, reject) => {
            //     this.http.post(this.baseURL + 'reply_tweet', form)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }
        this.fnPlaceFavorites = function (id) {
            var twitterdata = twitterFactory.get()
            let form = new FormData();
            form.append('twitter_oauth_token', twitterdata.twitter_oauth_token)
            form.append('twitter_oauth_token_secret', twitterdata.twitter_oauth_token_secret)
            form.append('tweet_id', id)
            let req = {
                method: 'POST',
                url: baseUrl + 'twitter/like_tweet',
                data: form
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })
            // return new Promise((resolve, reject) => {
            //     this.http.post(this.baseURL + 'like_tweet', form)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }
    })
    .service('youtubeService', function ($rootScope, $http, $q, $ionicLoading, googleFactory) {
        var videoList;
        this.getChannelList = function () {
            req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails,statistics&mine=true&maxResults=20&access_token=" + googleFactory.get().google_access_token,
                headers: {
                    "authorization": "Bearer " + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                var data = result.data
                return data;
            }, function (error) {
                return error;
            })

            // let header = new HttpHeaders();
            // header.append('authorization', 'Bearer ' + this.CustomeHttpServiceProvider.googleData.google_access_token)
            // let url = "https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails,statistics&mine=true&maxResults=20&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.get(url, { headers: header })
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }
        
        this.getVideoByChannel = function (id) {
            
            req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/search?key=" + googleFactory.get().google_client_secret + "&channelId=" + id + "&part=snippet,id&order=date&access_token=" + googleFactory.get().google_access_token,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                var arr =[];
                for (let i = 0; i < result.data.items.length; i++) {
                   arr.push(result.data.items[i]);
                }
                console.log(result)
                return arr;
              // return result.data.items;
                // console.log()
                // var videos;
                // j = 0;
                // for (let index = 0; index < videoList.length; index++) {
                //     var q = {
                //         method: 'get',
                //         url: "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + videoList[index].id.videoId + "&key= " + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token,
                //         headers: {
                //             "authorization": "Bearer" + googleFactory.get().google_access_token
                //         }
                //     }

                //     return $http(q).then(function (result) {
                //         console.log(result.data)
                //         videoList[index] = result.data
                        
                //     }, function (erro) {

                //     })
                //     if (index == (videoList.length - 1)) {

                //         return true;
                //     }
                    // this.getVideoStatics(this.videoList[index].id.videoId)
                    //     .then(function (result) {
                    //         this.videoList[index].VideoData = result
                    //         if (index == (this.videoList.length - 1)) {
                    //             return result.data;
                    //         }
                    //     }, function (error) { })

                // }
            }, function (error) {
                return error;
            })

            // let header = new HttpHeaders();
            // header.append('authorization', 'Bearer ' + this.CustomeHttpServiceProvider.googleData.google_access_token)
            // let url = "https://www.googleapis.com/youtube/v3/search?key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&channelId=" + id + "&part=snippet,id&order=date&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.get(url, { headers: header })
            //         .subscribe((result: any) => {
            //             this.videoList = result.items;
            //             for (let index = 0; index < this.videoList.length; index++) {
            //                 this.getVideoStatics(this.videoList[index].id.videoId)
            //                     .then((result: any) => {
            //                         this.videoList[index].VideoData = result
            //                         if (index == (this.videoList.length - 1)) {
            //                             resolve(this.videoList)
            //                         }
            //                     }, (error: any) => {

            //                     })
            //             }
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.getVideoStatics = function (id) {
            req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + id + "&key= " + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })
            // let header = new HttpHeaders();
            // header.append('authorization', 'Bearer ' + this.CustomeHttpServiceProvider.googleData.google_access_token)
            // let url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + id + "&key= " + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.get(url, { headers: header })
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.getUsersAllVideo = function (id) {
            req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&type=rabata&id=Ks-_Mh1QhMc&access_token=" + googleFactory.get().google_access_token,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })

            // let header = new HttpHeaders();
            // header.append('authorization', 'Bearer ' + this.CustomeHttpServiceProvider.googleData.google_access_token)
            // let url = "https://www.googleapis.com/youtube/v3/videos?key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&type=rabata&part=snippet"
            // url = "https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&type=rabata&id=Ks-_Mh1QhMc&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.get(url, { headers: header })
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.getVideoComment = function (data) {
            var req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/commentThreads?key=" + googleFactory.get().google_client_secret + "&textFormat=plainText&part=snippet&videoId=" + data + "&maxResults=50&access_token=" + googleFactory.get().google_access_token,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })

            // let header = new HttpHeaders();
            // header.append('authorization', 'Bearer ' + this.CustomeHttpServiceProvider.googleData.google_access_token)
            // let url = "https://www.googleapis.com/youtube/v3/commentThreads?key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&textFormat=plainText&part=snippet&videoId=" + data + "&maxResults=50&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.get(url, { headers: header })
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.fnSetVideoComment = function (videoId, channelId, cm) {
            let param = {
                snippet: {
                    channelId: channelId,
                    topLevelComment: {
                        snippet: {
                            textOriginal: cm
                        }
                    },
                    videoId: videoId
                }
            }


            var req = {
                method: 'post',
                url: "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&alt=json&key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token,
                data: param,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })


            // let url = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&alt=json&key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.post(url, param)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.fnShowCommentReplay = function (comment) {
            var req = {
                method: 'get',
                url: "https://www.googleapis.com/youtube/v3/comments?part=snippet,id&parentId=" + comment.id + "&key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token + "&order=time&maxResults=100",
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })


            // let url = "https://www.googleapis.com/youtube/v3/comments?part=snippet,id&parentId=" + comment.id + "&key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token + "&order=time&maxResults=100";
            // return new Promise<any>((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result.items)
            //     }, (error: any) => {
            //         reject(error)
            //     })
            // })
        }

        this.fnSetCommentReplay = function (comment) {
            let param = {
                snippet: {
                    parentId: comment.id,
                    channelId: comment.snippet.topLevelComment.snippet.authorChannelId.value,
                    videoId: comment.snippet.videoId,
                    textOriginal: comment.replayComment,
                    topLevelComment: {
                        snippet: {
                            textOriginal: comment.replayComment
                        }
                    },
                }
            }

            var req = {
                method: 'post',
                url: "https://www.googleapis.com/youtube/v3/comments?part=snippet&alt=json&key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token,
                data: param,
                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })

            // let url = "https://www.googleapis.com/youtube/v3/comments?part=snippet&alt=json&key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token
            // return new Promise((resolve, reject) => {
            //     this.http.post(url, param)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.fnSetVideoLike = function (VideoId) {
            var req = {
                method: 'get',
                url: "https://content.googleapis.com/youtube/v3/videos/rate?id=" + VideoId + "&rating=like&&key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token + "&order=time&maxResults=100",

                headers: {
                    "authorization": "Bearer" + googleFactory.get().google_access_token
                }
            }
            return $http(req).then(function (result) {
                return result.data;
            }, function (error) {
                return error;
            })

        }
    })

    .service('googlePlusService', function ($rootScope, $http, $q, $ionicLoading, googleFactory) {

        this.fnGetUserActivity = function () {
            req = {
                method: 'get',
                url: "https://content.googleapis.com/plus/v1/people/" + googleFactory.get().google_user_id + "/activities/public?key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,selfLink,title,updated"
            }
            $http(req).then(function (result) { return result }, function (error) { return error })
            // let url = "https://content.googleapis.com/plus/v1/people/" + this.CustomeHttpServiceProvider.googleData.google_user_id + "/activities/public?key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,selfLink,title,updated"
            // return new Promise<any>((resolve, reject) => {
            //     this.http.get(url)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error) => {
            //             reject(error)
            //         })
            // })
        }

        this.fnGetActivityComments = function (activitiId) {
            req = {
                method: 'get',
                url: "https://content.googleapis.com/plus/v1/activities/" + activitiId + "/comments?key=" + googleFactory.get().google_client_secret + "&access_token=" + googleFactory.get().google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,title,updated"
            }
            $http(req).then(function (result) { return result }, function (error) { return error })

            // let url = "https://content.googleapis.com/plus/v1/activities/" + activitiId + "/comments?key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,title,updated"
            // return new Promise<any>((resolve, reject) => {
            //     this.http.get(url)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error: any) => {
            //             reject(error)
            //         })
            // })
        }

        this.fnSetActivityComment = function (activitiId, comment) {
            let param = {
                object: {
                    originalContent: comment
                }
            }

            req = {
                method: 'POST',
                url: "https://www.googleapis.com/plusDomains/v1/activities/" + activitiId + "/comments?alt=json&key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,selfLink,title,updated",
                data: param
            }
            $http(req).then(function (result) { return result }, function (error) { return error })

            // let url = "https://www.googleapis.com/plusDomains/v1/activities/" + activitiId + "/comments?alt=json&key=" + this.CustomeHttpServiceProvider.googleData.google_client_secret + "&access_token=" + this.CustomeHttpServiceProvider.googleData.google_access_token + "&fields=etag,id,items,kind,nextLink,nextPageToken,selfLink,title,updated"
            // this.http.post(url, param)
            // return new Promise((resolve, reject) => {
            //     this.http.post(url, param)
            //         .subscribe((result: any) => {
            //             resolve(result)
            //         }, (error: any) => {
            //             reject(error)
            //         })
            // })
        }
    })
    .service('instagramService', function ($rootScope, $http, $q, $ionicLoading, instagramFactory) {
        this.timeline = function () {
            console.log('/n/n/n/n/n/n/n/');
            console.log(instagramFactory.get())
            var req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/users/' + instagramFactory.get().instagram_user_id + '/media/recent/?access_token=' + instagramFactory.get().instagram_access_token
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })
            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }

        this.fnGetUserInfo = function () {
            var a = instagramFactory.get().instagram_user_id;
            console.log(a)
            let req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/users/' + instagramFactory.get().instagram_user_id + '/?access_token=' + instagramFactory.get().instagram_access_token
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })

        }
        this.fnGetUserFollowers = function () {
            let req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/users/' + instagramFactory.get().instagram_user_id + '/followed-by?access_token=' + instagramFactory.get().instagram_access_token
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnGetUserFollows = function () {
            let req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/users/' + instagramFactory.get().instagram_user_id + '/follows?access_token=' + instagramFactory.get().instagram_access_token

            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }


        this.fnGetMediaLike = function (mediaId) {
            var req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/media/' + mediaId + '/likes?access_token=' + instagramFactory.get().instagram_access_token

            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise<any>((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }

        this.fnGetMediaComment = function (mediaId) {
            let req = {
                method: 'get',
                url: 'https://api.instagram.com/v1/media/' + mediaId + '/comments?access_token=' + instagramFactory.get().instagram_access_token
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnSetMediaComment = function (mediaId, comment) {
            let formData = new FormData()
            formData.append('text', comment)
            let req = {
                method: 'POST',
                url: 'https://api.instagram.com/v1/media/' + mediaId + '/comments?access_token=' + instagramFactory.get().instagram_access_token + '&text=' + comment,
                data: formData
            }
            return $http(req).then(function (result) {
                alert(JSON.stringify(result));
                 return result
                 }, function (error) { 
                     return error })

            // return new Promise((resolve, reject) => {
            //     this.http.post(url, formData).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
        this.fnDeleteMediaComment = function (mediaId, commentId) {
            let req = {
                method: 'delete',
                url: 'https://api.instagram.com/v1/media/' + mediaId + '/comments/' + commentId + '/?access_token=' + instagramFactory.get().instagram_access_token,

            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.delete(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }

        this.fnSetMediaLike = function (mediaId) {
            let formData = new FormData()
            let req = {
                method: 'POST',
                url: ' https://api.instagram.com/v1/media/' + mediaId + '/likes?access_token=' + instagramFactory.get().instagram_access_token,
                data: formData
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.post(url, formData).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }

        this.fnRemoveMediaLike = function (mediaId) {
            let req = {
                method: 'delete',
                url: ' https://api.instagram.com/v1/media/' + mediaId + '/likes?access_token=' + instagramFactory.get().instagram_access_token,
            }
            return $http(req).then(function (result) { return result }, function (error) { return error })

            // return new Promise((resolve, reject) => {
            //     this.http.delete(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error);
            //     })
            // })
        }
    })
    .service('linkedinService', function ($rootScope, $http, $q, $ionicLoading, linkedinFactory) {
        this.timeline = function () {
            var req = {
                method: 'get',
                url: 'https://api.linkedin.com/v1/people/~:(id,location,picture-url,specialties,public-profile-url,email-address,formatted-name,num-connections)?format=json&oauth2_access_token=' + linkedinFactory.get().linkedin_access_token
            }
            return $http(req).then(function (result) {
                return result;
            }, function (error) {
                return error;
            })
        }
        this.companyList = function () {
            var req = {
                method: 'get',
                url: 'https://api.linkedin.com/v1/companies?oauth2_access_token=' + linkedinFactory.get().linkedin_access_token + '&format=json&is-company-admin=true'
            }
            return $http(req).then(function (result) {
                console.log(result);
                return result;
            }, function (error) {
                return error;
            })
            // return new Promise((resolve, reject) => {
            //     let url = 'https://api.linkedin.com/v1/companies?oauth2_access_token=' + this.CustomeHttpServiceProvider.linkedinData.linkedin_access_token + '&format=json&is-company-admin=true'
            //     this._http.get(url).subscribe((result: any) => {
            //         result = result.json();
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error)
            //     })
            // })
        }

        this.getPagesData = function (data) {
            console.log(data)
                console.log(linkedinFactory.get())
            var req = {
                method: 'get',
                url: "https://api.linkedin.com/v1/companies/" + data.id + "/updates?oauth2_access_token=" + linkedinFactory.get().linkedin_access_token + "&format=json&count=250"
            }
            return $http(req).then(function (result) {
                return result;
            }, function (error) {
                return error;
            })
            // let url = "https://api.linkedin.com/v1/companies/" + data.id + "/updates?oauth2_access_token=" + this.CustomeHttpServiceProvider.linkedinData.linkedin_access_token + "&format=json";
            // return new Promise((resolve, reject) => {
            //     this.http.get(url).subscribe((result: any) => {
            //         resolve(result)
            //     }, (error: any) => {
            //         reject();
            //     })
            // })
        }

        this.fnPlaceComment = function (data) {
            console.log(data)
            var req = {
                method: 'POST',
                url: 'https://api.linkedin.com/v1/companies/' + data.updateContent.company.id + '/updates/key=' + data.updateKey + '/update-comments-as-company?format=json&oauth2_access_token=' + linkedinFactory.get().linkedin_access_token,
                data: { 'comment': data.placeCmt },
                headers: {
                    'Content-Type': 'application/json',
                    'x-li-format': 'json'
                }
            }
            return $http(req).then(function (result) {
                console.log(result)
                console.log(1)
                return result;
            }, function (error) {
                console.log(error)
                console.log(2)

                return error;
            })
            // let url = 'https://api.linkedin.com/v1/companies/' + data.updateContent.company.id + '/updates/key=' + data.updateKey + '/update-comments-as-company?format=json&oauth2_access_token=' + this.CustomeHttpServiceProvider.linkedinData.linkedin_access_token
            // return new Promise<any>((resolve, reject) => {
            //     this._http.post(url, { 'comment': data.placeCmt }).subscribe((result: any) => {
            //         // result = result.json()
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error)
            //     })
            // })
        }
        this.fnPlaceLikes = function (data) {
      /*      alert(JSON.stringify(data.updateContent.company.id));
            alert(JSON.stringify(data.updateKey));*/

            var req = {
                method: 'POST',
                url: 'https://api.linkedin.com/v1/companies/' + data.updateContent.company.id + '/updates/key=' + data.updateKey + '/update-likes-as-company?format=json&oauth2_access_token=' + linkedinFactory.get().linkedin_access_token,
              //  data: { 'comment': data.placeCmt }
            }
            //alert(req.url);
            $http(req).then(function (result) {
                return result;
            }, function (error) {
                return error;
            })
            // let url = 'https://api.linkedin.com/v1/companies/' + data.updateContent.company.id + '/updates/key=' + data.updateKey + '/update-likes-as-company?format=json&oauth2_access_token=' + this.CustomeHttpServiceProvider.linkedinData.linkedin_access_token
            // return new Promise<any>((resolve, reject) => {
            //     this._http.post(url, { 'comment': data.placeCmt }).subscribe((result: any) => {
            //         // result = result.json()
            //         resolve(result)
            //     }, (error: any) => {
            //         reject(error)
            //     })
            // })
        }
    })
    .service('Invitation', function ($rootScope, $http, $q, $ionicLoading) {
        this.sendInvitation = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/sendInvitation',
                headers: {
                    'Content-Type': undefined
                },
                data: { name: user.name, email: user.email, token: user.token }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getInvitations = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/getInvitationByUserID/' + user.page,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    id: user.id,
                    search: user.search
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

    })
    .service('Profile', function ($rootScope, $http, $q, $ionicLoading) {
        this.updateProfile = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/editProfile',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email,
                    company: user.company,
                    phone: user.phone,
                    token: user.token
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getProfile = function (user) {
            //alert(JSON.stringify(user));

            req = {
                method: 'POST',
                url: baseUrl + 'user/getUserProfileData',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token
                }
            }
            return $http(req).then(function (data) {
                // alert(JSON.stringify(data));
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Events', function ($rootScope, $http, $q, $ionicLoading) {

        this.getEvent = function (event) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/getEventByToken',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: event.token
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.updateEvent = function (event) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/updateEvent',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    name: event.name,
                    address: event.address,
                    city: event.city,
                    state: event.state,
                    zip: event.zip,
                    token: event.token
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getAddress = function (address) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/getAddressBylatlong',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    lat: address.lat,
                    long: address.long,
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Sent', function ($rootScope, $http, $q, $ionicLoading) {

        this.getEvent = function (event) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/getEventByToken',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: event.token
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.updateEvent = function (event) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/updateEvent',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    name: event.name,
                    address: event.address,
                    city: event.city,
                    state: event.state,
                    zip: event.zip,
                    token: event.token
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Projects', function ($rootScope, $http, $q, $ionicLoading) {
        this.getusers = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_users_of_project',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token,
                    project_id: project.project_id

                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

this.add_bug = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/add_bug',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token,
                    project_id: project.project_id,
                    files:project.files,
                    description:project.description
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
 

        this.getproject_bugs = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_listing_bugs',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token,
                    pid: project.id

                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

  this.getcontent_txfile = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_contentOftxt',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    name: project.name

                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getCompanies = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_companies',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getProjects = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/getProjectsByStatus',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token,
                    status: project.status,
                    company_id: project.company_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getProjectDetail = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/projectDetail',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token,
                    project_id: project.project_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Activity', function ($rootScope, $http, $q, $ionicLoading) {
        this.registerDevice = function (user) {

            req = {
                method: 'POST',
                url: baseUrl + 'user/registerUserDevice',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token,
                    device_id: user.device_id,
                    type: user.type,
                    user_id: user.user_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });

        };
        this.getActivity = function (activity) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_user_activity',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: activity.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Quotes', function ($rootScope, $http, $q, $ionicLoading) {
        this.getquotes = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/servicesAllData_web',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: quotes.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.saverequest = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/addinfo_web',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    name: quotes.name,
                    phone: quotes.phone,
                    email: quotes.email,
                    detail: quotes.detail,
                    totalprice: quotes.totalprice
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getdetails = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/listing',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    status: quotes.status,
                    token: quotes.token

                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.sendback = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/sentback',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id,
                    email: quotes.email,
                    price: quotes.price

                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.accept = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/accepted',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id,
                    email: quotes.token


                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.Reject = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/rejection_comment',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id,
                    email: quotes.token,
                    rejection_comment: quotes.coment

                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.NDA = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/generateNdaDocument',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.NDA_view = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/NDA_view',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.saleagreement = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/generateSaleAgreementDocument',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.saleagreement_view = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/saleagreement_view',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.get_services = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/get_services',
                headers: {
                    'Content-Type': undefined
                },

            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.get_all_packages = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/servicesAllPackages_web',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }

            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.get_against_services = function (quotes) {
            req = {
                method: 'POST',
                url: baseUrl + 'quotes/get_against_services_web',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    id: quotes.id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

    })
    .service('Chart', function ($rootScope, $http, $q, $ionicLoading) {

        this.getpages = function (chart) {
            req = {
                method: 'POST',
                url: baseUrl + 'Chart/chartview/',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: chart.token,
                    projectid: chart.projectid
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getviews = function (chart) {
            req = {
                method: 'POST',
                url: baseUrl + 'Chart/load_role_metrics_sub_view',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: chart.token,
                    projectid: chart.projectid,
                    view: chart.view,
                    page: chart.page
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.showchart = function (chart) {
            req = {
                method: 'POST',
                url: baseUrl + 'Chart/chartview/',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: chart.token,
                    projectid: chart.projectid,
                    startdate: chart.start_date,
                    page: chart.page,
                    title: chart.title
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getchart = function (chart) {
            req = {
                method: 'POST',
                url: baseUrl + 'Chart/get_chartview/',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: chart.token,
                    startdate: chart.start_date,
                    page: chart.page,
                    title: chart.title
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

    })
    .service('Tasks', function ($rootScope, $http, $q, $ionicLoading) {
        this.getusers = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_users_of_project',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    project_id: task.project_id

                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.allTasks = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/getAllTasks',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.allprojects = function () {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/getAllprojects',
                headers: {
                    'Content-Type': undefined
                },
               
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getProjects = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_projects_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    company_id: task.company_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getallProjectsfor = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_projects_for_user',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getCompanies = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_companies',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

        this.getTasks = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/tasks',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    status: task.status,
                    project_id: task.projectid
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.gettasks_user = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/get_tasks_for_users',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    statusoftasks: task.statusoftasks,
                    category: task.status,
                    project_id: task.projectid
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.deleteTask = function (id, task) {
            req = {
                method: 'POST',
                url: baseUrl + 'projects/deleteTask/' + id,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.deleteToDoTask = function (id, task) {
            req = {
                method: 'POST',
                url: baseUrl + 'projects/deleteToDoTask/' + id,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.taskComplete = function (id) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/taskComplete/' + id,
                headers: {
                    'Content-Type': undefined
                },
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getTaskDetail = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/tasksDetail',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    task_id: task.task_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.addTask = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/add',
                headers: {
                    'Content-Type': undefined
                },

                data: {
                    token: task.token,
                    users: task.users,
                    title: task.title,
                    company_id: task.company_id,
                    due_date: task.due_date,
                    description: task.description,
                    notes: task.notes,
                    priority: task.priority,
                    todo: task.todo,
                    audience: task.audience,
                    project_id: task.project_id,
                    requirement_id: task.requirement_id,
                    description_checklist: task.description_checklist
                }
            }
            // alert(JSON.stringify(req));
            return $http(req).then(function (data) {
                // alert(data);
                return data;
            }, function (data) {
                alert(data);
                return data;
            });
        };
        this.getRecipts = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_receipts',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getProjects = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_projects_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    company_id: task.company_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getprojectone = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_only_one_project',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    projectid: task.projectid
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };
        this.getReciptsByCompnay = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_receipts_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    company_id: task.company_id
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };
        this.gettype = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/gettype',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };


        this.getReciptsByproject = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_receipts_by_project',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    project_id: task.project_id
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };
        this.getprojectsByCompnay = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_projects_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    company_id: task.company_id
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };
        this.getRequirements = function (task12) {
            req = {
                method: 'POST',
                url: baseUrl + 'user/get_requirements',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    project_id: task12.project_id
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };

        this.updateStatus = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'Tasks/updateTaskStatus',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    id: task.todo_id,
                    task_id: task.task_id,
                    type: task.type
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getTaskById = function (id) {
            console.log(id);
            req = {
                method: 'POST',
                url: baseUrl + 'projects/getTaskById/' + id,
                headers: {
                    'Content-Type': undefined
                },
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.editTask = function (id, task) {
            req = {
                method: 'POST',
                url: baseUrl + 'projects/editTask/' + id,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token,
                    title: task.title,
                    due_date: task.due_date,
                    description: task.description,
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };

    })
    .service('Documents', function ($rootScope, $http, $q, $ionicLoading) {
        this.getallProjectsfor = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_projects_for_user',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.alldocs = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'documents/getAlldocs',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
         this.allprojects = function () {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/getAllprojects',
                headers: {
                    'Content-Type': undefined
                },
               
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getProjects = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_projects_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    company_id: task.company_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getCompanies = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_companies',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getDocuments = function (document) {
            req = {
                method: 'POST',
                url: baseUrl + 'documents/getAlldocumentsByUser',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    status: document.status,
                    project_id: document.projectid
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getDocumentDetail = function (document) {
            console.log(document);
            req = {
                method: 'POST',
                url: baseUrl + 'documents/documentDetail',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    document_id: document.document_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.signDocument = function (document) {
            req = {
                method: 'POST',
                url: baseUrl + 'Project/sign_document_web',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    sighn_image: document.sighn_image,
                    document_id: document.document_id,
                    password: document.password,
                    ids_check: document.ids_check
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.verify = function (document) {
            req = {
                method: 'POST',
                url: baseUrl + 'Project/verify_document',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    document_id: document.document_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.history = function (document) {
            req = {
                method: 'POST',
                url: baseUrl + 'Project/history_of_document',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    document_id: document.document_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.sendToClient = function (document) {
            req = {
                method: 'POST',
                url: baseUrl + 'Documents/send_to_client',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: document.token,
                    document_id: document.document_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Invoices', function ($rootScope, $http, $q, $ionicLoading) {
        this.getCompanies = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_companies',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
         this.gethistory = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/gethistory',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.cashout_request = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/Cashout_request',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.cashoutHistory = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/get_cashout_history',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
         this.cashaccepted = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/updateStatus',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    id: user.id


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getcredits = function (user) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/remainingCredits',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: user.token


                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.allprojects = function () {
            req = {
                method: 'POST',
                url: baseUrl + 'tasks/getAllprojects',
                headers: {
                    'Content-Type': undefined
                },
               
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.allinvoices = function (project) {
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/getAllInvoices',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: project.token
                }

            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getProjects = function (project) {

            req = {
                method: 'POST',
                url: baseUrl + 'project/get_projects_by_company',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    company_id: project.company_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getprojectone = function (task) {
            req = {
                method: 'POST',
                url: baseUrl + 'project/get_all_projects_for_user',
                headers: {
                    'Content-Type': undefined
                },
                data: {

                    token: task.token
                }
            }
            return $http(req).then(function (data) {
                //alert(JSON.stringify(data));
                return data;
            }, function (data) {
                //alert(JSON.stringify(data));
                return data;
            });
        };
        this.getInvoices = function (invoice) {
            console.log(invoice);
            req = {
                method: 'POST',
                url: baseUrl + 'invoices/invoices',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: invoice.token,
                    status: invoice.status,
                    project_id: invoice.projectid
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        /*   this.addInvoices = function(invoice) {
               console.log(invoice);
               req = {
                   method: 'POST',
                   url: baseUrl+'invoices/add_invoice',
                   headers: {
                       'Content-Type': undefined
                   },
                   data: { 
                       token:invoice.token,
                       project_id:invoice.projectid,
                       status:invoice.status,
                   }
               }
               return $http(req).then(function(data){
                   return data;
               }, function(data){
                   return data;
               });
           };*/
        this.getInvoiceDetail = function (invoice) {
            console.log(invoice);
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/invoiceDetail',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: invoice.token,
                    invoice_id: invoice.invoice_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.payInvoice = function (invoice) {
            console.log(invoice);
            req = {
                method: 'POST',
                url: baseUrl + 'Invoices/payInvoiceNew',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: invoice.token,
                    invoice_id: invoice.invoice_id,
                    card: invoice.card,
                    month: invoice.month,
                    year: invoice.year,
                    ccv: invoice.ccv,
                    usecredit: invoice.usecredit
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .service('Messages', function ($rootScope, $http, $q, $ionicLoading) {
        this.getInbox = function (message) {
            req = {
                method: 'POST',
                url: baseUrl + 'messages/getInboxMessages/' + message.page,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    project_id: message.project_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getSent = function (message) {
            req = {
                method: 'POST',
                url: baseUrl + 'messages/getSentMessages/' + message.page,
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    project_id: message.project_id
                }
            }

            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getMessageDetail = function (message) {
            console.log(document);
            req = {
                method: 'POST',
                url: baseUrl + 'messages/getMessageDetail',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    message_id: message.message_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.getUnreadMessage = function (message) {
            console.log(document);
            req = {
                method: 'POST',
                url: baseUrl + 'messages/getUnreadMessage',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    //message_id:message.message_id
                }
            }
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.addMessage = function (message) {
            req = {
                method: 'POST',
                url: baseUrl + 'messages/add',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    users: message.users,
                    subject: message.subject,
                    message: message.message,
                    sendemail: message.sendemail,
                    projectid: message.projectid
                }
            }
            console.log(req);
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
        this.addReply = function (message) {
            req = {
                method: 'POST',
                url: baseUrl + 'messages/reply',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    token: message.token,
                    message_id: message.messageid,
                    subject: message.subject,
                    text: message.message,
                    send_email: message.sendemail,
                    projectid: message.projectid
                }
            }
            console.log(req);
            return $http(req).then(function (data) {
                return data;
            }, function (data) {
                return data;
            });
        };
    })
    .factory('Chats', function () {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var chats = [{
            id: 0,
            name: 'Ben Sparrow',
            lastText: 'You on your way?',
            face: 'img/ben.png'
        }, {
            id: 1,
            name: 'Max Lynx',
            lastText: 'Hey, it\'s me',
            face: 'img/max.png'
        }, {
            id: 2,
            name: 'Adam Bradleyson',
            lastText: 'I should buy a boat',
            face: 'img/adam.jpg'
        }, {
            id: 3,
            name: 'Perry Governor',
            lastText: 'Look at my mukluks!',
            face: 'img/perry.png'
        }, {
            id: 4,
            name: 'Mike Harrington',
            lastText: 'This is wicked good ice cream.',
            face: 'img/mike.png'
        }];

        return {
            all: function () {
                return chats;
            },
            remove: function (chat) {
                chats.splice(chats.indexOf(chat), 1);
            },
            get: function (chatId) {
                for (var i = 0; i < chats.length; i++) {
                    if (chats[i].id === parseInt(chatId)) {
                        return chats[i];
                    }
                }
                return null;
            },


        };
    });